/**
 * Export code providing a virtio-blk device over a vhost-user transport.
 */
use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, NetListener, NetStream, SendOnDrop, SocketAddr};
use crate::node::exports::{self, BackgroundOpReq};
use crate::node::{NodePerm, NodeUser, NodeUserBuilder};
use crate::server::ServerNode;
use async_trait::async_trait;
use futures::FutureExt;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use tokio::sync::mpsc;

mod backend;
mod guest_memory;
mod packed_vq;
mod protocol;
mod split_vq;
mod traits;
mod virtio_blk;
mod virtq;

use backend::VhostUserBackend;
use virtio_blk::VirtioBlk;
use virtq::CallBatching;

pub type Config = exports::Config<VhostUserBlkConfig>;
pub type Data = exports::Data<VhostUserBlkConfig>;

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct VhostUserBlkConfig {
    /// Server address (only unix sockets supported)
    addr: SocketAddr,

    /// Whether it should be allowed to write to the exported device or not
    #[serde(default)]
    writable: bool,

    /// Logical block size in bytes (defaults to 512)
    logical_block_size: Option<usize>,

    /// Number of virtqueues to create (defaults to 1)
    num_queues: Option<usize>,

    /// Where to put the I/O queues (defaults to ephemeral threads)
    iothreads: Option<Vec<Option<String>>>,

    /// Parameters for batching notifications to the guest (default: no batching)
    call_batching: Option<ConfigCallBatching>,

    /// How long to continue busy-polling after receiving a request from the guest (default: no
    /// continual polling)
    poll_duration: Option<f32>,
}

pub struct VhostUserBlkBackgroundOpData {
    srv: VhostUserBackend<VirtioBlk>,
    listener: NetListener,
    quit: mpsc::Receiver<()>,
    drop_watchdog: Arc<SendOnDrop<()>>,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct ConfigCallBatching {
    /// Number of buffers to complete before sending a notification to the guest
    pub batch_size: usize,

    /// Number of seconds to wait before sending a notification to the guest, regardless of whether
    /// the batch size has been reached or not
    pub max_latency: f32,
}

#[async_trait(?Send)]
impl exports::ImmutableExportConfig for VhostUserBlkConfig {
    type BackgroundOpData = VhostUserBlkBackgroundOpData;

    fn construct_node_user(
        &self,
        init: NodeUserBuilder,
        read_only: bool,
    ) -> BlockResult<NodeUserBuilder> {
        if self.writable && read_only {
            return Err("Cannot set both writable and read-only".into());
        }

        let mut node_user = init
            .require(NodePerm::ConsistentRead)
            .block(NodePerm::Write)
            .block(NodePerm::Resize);
        if self.writable {
            node_user = node_user.require(NodePerm::Write);
        }
        Ok(node_user)
    }

    async fn create_background_op_data(
        &self,
        _node_name: &str,
        exported: &Arc<NodeUser>,
        channel_r: mpsc::UnboundedReceiver<BackgroundOpReq>,
        read_only: bool,
    ) -> BlockResult<Self::BackgroundOpData> {
        if self.writable && read_only {
            return Err("Cannot set both writable and read-only".into());
        }

        let (num_queues, threads) = match (self.num_queues, self.iothreads.clone()) {
            (Some(num), Some(thr)) => (num, thr),
            (Some(num), None) => (num, vec![None; num]),
            (None, Some(thr)) => (thr.len(), thr),
            (None, None) => (1, vec![None]),
        };
        if threads.len() != num_queues {
            return Err(format!(
                "num-queues ({}) differs from the number of threads specified ({})",
                num_queues,
                threads.len()
            )
            .into());
        }
        if num_queues < 1 {
            return Err("num-queues must be positive".into());
        }

        let call_batching = self.call_batching.as_ref().unwrap_or(&ConfigCallBatching {
            batch_size: 1,
            max_latency: 0.0,
        });
        if call_batching.batch_size < 1 {
            return Err("call-batching.batch-size must be at least 1".into());
        }
        if call_batching.max_latency < 0.0 {
            return Err("call-batching.max-latency must not be negative".into());
        }
        if call_batching.batch_size > 1 && call_batching.max_latency == 0.0 {
            return Err(
                "call-batching.batch-size > 1 has no effect with a zero max-latency".into(),
            );
        }
        if call_batching.max_latency > 0.0 && call_batching.batch_size == 1 {
            return Err(
                "Setting call-batching.max-latency has no effect with a batch-size of 1".into(),
            );
        }

        let call_batching = CallBatching {
            batch_size: call_batching.batch_size,
            max_latency: call_batching.max_latency,
        };

        let poll_duration = self.poll_duration.unwrap_or(0.0);
        if poll_duration < 0.0 {
            return Err("poll-duration must not be negative".into());
        }

        let physical_block_size = exported.node().request_align();
        let logical_block_size = self.logical_block_size.unwrap_or(512);
        if logical_block_size < 512 {
            return Err("logical-block-size must be at least 512".into());
        }
        if !logical_block_size.is_power_of_two() {
            return Err("logical-block-size must be a power of two".into());
        }
        if logical_block_size < physical_block_size {
            return Err(format!(
                "logical-block-size must be at least as large as the physical block size ({})",
                physical_block_size
            )
            .into());
        }

        // When this function returns, all references to the node must be gone.  Accomplish that by
        // handing out a drop watchdog that accompanies all instances of the node and I/O queues.
        let drop_watchdog = Arc::new(SendOnDrop::new(()));

        let device = VirtioBlk::new(
            Arc::clone(exported),
            Arc::clone(&drop_watchdog),
            !self.writable,
            num_queues,
            logical_block_size,
        )?;

        if !matches!(self.addr, SocketAddr::Unix(_)) {
            return Err(
                "Invalid address type for vhost-user; only unix sockets are supported".into(),
            );
        }
        let listener = self.addr.clone().into_listener().await?;

        let (srv, quit) =
            VhostUserBackend::new(device, channel_r, threads, call_batching, poll_duration).await?;

        Ok(VhostUserBlkBackgroundOpData {
            srv,
            listener,
            quit,
            drop_watchdog,
        })
    }
}

#[async_trait(?Send)]
impl exports::BackgroundOpData for VhostUserBlkBackgroundOpData {
    fn background_operation(self) -> (BlockFutureResult<'static, ()>, Option<ServerNode>) {
        (
            Box::pin(worker(
                self.srv,
                self.listener,
                self.quit,
                self.drop_watchdog,
            )),
            None,
        )
    }

    async fn async_drop(self) {
        drop(self.srv);
        drop(self.listener);
        drop(self.quit);

        let _: Result<(), _> = SendOnDrop::into_receiver(self.drop_watchdog).unwrap().await;
    }
}

async fn worker(
    mut srv: VhostUserBackend<VirtioBlk>,
    mut listener: NetListener,
    mut quit: mpsc::Receiver<()>,
    drop_watchdog: Arc<SendOnDrop<()>>,
) -> BlockResult<()> {
    // Must be put in a `Box`, because using `futures::pin_mut!()` means that explicitly dropping
    // it will no longer free the associated resources (i.e. the device and backend).  Putting it
    // in a box will have the resources be freed.
    let mut main_loop_fut = Box::pin(
        async move {
            loop {
                let con = match (&mut listener).await? {
                    NetStream::Unix(u) => u,
                    _ => unreachable!(),
                };
                srv.connect(con);

                loop {
                    if let Err(e) = srv.process_message().await {
                        if e.get_inner().kind() == std::io::ErrorKind::ConnectionAborted {
                            srv.disconnect().await;
                            break;
                        }
                        return Err(e);
                    }
                }
            }
        }
        .fuse(),
    );

    let quit_fut = quit.recv().fuse();
    futures::pin_mut!(quit_fut);

    let result = futures::select! {
        result = main_loop_fut => result,
        _ = quit_fut => Ok(()),
    };

    drop(main_loop_fut);

    let _: Result<(), _> = SendOnDrop::into_receiver(drop_watchdog).unwrap().await;

    result
}
