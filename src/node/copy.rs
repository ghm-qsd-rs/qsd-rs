use crate::error::{BlockError, BlockResult};
use crate::helpers::{
    BlockFutureResult, BoxedFuture, InfallibleFuture, IoBuffer, IoBufferMut, IoBufferRef,
    ThreadBound,
};
use crate::monitor::qmp::{self, BlockErrorAction, IoOperationType};
use crate::monitor::{self, broadcast_event, ThreadHandle};
use crate::node::{
    BackgroundOpResult, ChangeGraphStep, DirtyBitmap, DirtyBitmapArea, DirtyBitmapIterator,
    IoQueue, IoQueueDriverData, JobInfo, Node, NodeBasicInfo, NodeCacheConfig, NodeConfig,
    NodeConfigOrReference, NodeDriverConfig, NodeDriverData, NodeLimits, NodePerm, NodePermPair,
    NodeUser, NodeUserBuilder, PollableNodeStopMode, ReopenQueue,
};
use crate::server::ServerNode;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::future::Future;
use std::pin::Pin;
use std::sync::atomic::{AtomicBool, AtomicU64, AtomicUsize, Ordering};
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};
use tokio::sync::{mpsc, oneshot};

struct MutData {
    file: Arc<NodeUser>,
    target: Arc<NodeUser>,

    switch_to_active: bool,

    pre_reopen_file: Option<Arc<NodeUser>>,
    pre_reopen_target: Option<Arc<NodeUser>>,
}

struct SharedData {
    job_id: String,
    job_type: qmp::JobType,
    mode: CopyMode,
    sync: SyncMode,

    granularity: u64,
    simultaneous: usize,
    dirty_bitmap: Arc<Mutex<DirtyBitmap>>,
    in_flight: InFlight,

    auto_finalize: bool,
    auto_dismiss: bool,

    active: AtomicBool,
    status: Mutex<qmp::JobStatus>,

    /// If there was an error, it is noted here.  (qemu design kind of requires this, instead of
    /// just returning some `Result<(), E>` and printing the error once.)
    error: Mutex<Option<String>>,

    /// Copying is complete: Once the dirty bitmap is empty, do all I/O to the final node instead
    /// of continuing the copy operation.
    /// When this is set, the dirty bitmap is guaranteed to be and stay clean (empty).
    /// To ensure this, setting of this field is guarded in two ways:
    /// (1) The bitmap lock is held (and the bitmap is checked to be empty under the same lock),
    /// (2) All in-flight requests are drained.
    /// Whenever dirtying the bitmap, one of these conditions must be blocked; either this field
    /// must be checked under the same bitmap lock that is used to dirty the bitmap, or it must be
    /// checked while an in-flight request is alive, which must say alive until the bitmap is
    /// dirtied.
    completed: AtomicBool,
    /// Relevant only once `completed` is set: Whether to forward I/O to the target (`true`) or the
    /// source (`false`).
    switched_over: AtomicBool,
}

pub struct Data {
    mut_data: Arc<Mutex<MutData>>,
    shared_data: Arc<SharedData>,
    immutable_config: ImmutableConfig,

    background_operation: ThreadBound<MirrorBackgroundOpData>,
    background_channel: mpsc::UnboundedSender<BackgroundOpReq>,
}

struct MirrorBackgroundOpData {
    thread: ThreadHandle,
    mirror_background: ThreadBound<MirrorBackground>,
}

pub struct Queue {
    file: IoQueue,
    target: IoQueue,

    mut_data: Arc<Mutex<MutData>>,
    shared_data: Arc<SharedData>,

    pre_reopen_file: Option<IoQueue>,
    pre_reopen_target: Option<IoQueue>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    file: NodeConfigOrReference,
    target: NodeConfigOrReference,

    /// When this is `false`, the node will not perform copying, and just pass through everything
    /// to the source node.  When reopening, this can only be switched from `false` to `true`, not
    /// the other way.
    /// The intention is to add the node with `blockdev-add` and `active=false`, and then insert it
    /// between an existing parent and a child with `blockdev-reopen`, while simultaneously setting
    /// `active=true`.
    active: bool,

    #[serde(flatten)]
    immutable: ImmutableConfig,
}

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
struct ImmutableConfig {
    job_id: String,
    mode: CopyMode,
    #[serde(default)]
    sync: SyncMode,
    auto_finalize: bool,
    auto_dismiss: Option<bool>,

    granularity: Option<usize>,
    /// Maximum size of background requests (must be a multiple of the granularity)
    buf_size: Option<usize>,
    /// Maximum number of simultaneous background requests
    simultaneous: Option<usize>,

    iothread: Option<String>,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub enum CopyMode {
    Lazy,
    CopyBeforeWrite,
    CopyAfterWrite,
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub enum SyncMode {
    None,
    #[default]
    Full,
}

struct MirrorBackground {
    empty_bufs: Vec<IoBuffer>,

    buf_size: usize,
    shared_data: Arc<SharedData>,
    channel: mpsc::UnboundedReceiver<BackgroundOpReq>,

    source_queue: IoQueue,
    target_queue: IoQueue,

    should_complete: bool,
    should_switch_over: bool,
    emitted_ready: bool,
}

struct MirrorBackgroundRef<'a> {
    empty_bufs: &'a mut Vec<IoBuffer>,

    buf_size: usize,
    shared_data: &'a SharedData,
    channel: &'a mut mpsc::UnboundedReceiver<BackgroundOpReq>,

    workers: MirrorWorkers<'a>,

    should_complete: bool,
    should_switch_over: bool,
    emitted_ready: &'a mut bool,
}

struct MirrorWorkers<'a> {
    workers: Vec<BoxedFuture<'a, IoBuffer>>,
    source_queue: &'a IoQueue,
    target_queue: &'a IoQueue,
    shared_data: &'a SharedData,
}

struct MirrorWorkersFut<'a, 'b> {
    workers: &'a mut MirrorWorkers<'b>,
    channel: &'a mut mpsc::UnboundedReceiver<BackgroundOpReq>,
    additional_notifier: Option<oneshot::Receiver<()>>,
}

struct MirrorWorkersFutAll<'a> {
    workers: MirrorWorkers<'a>,
    collected_buffers: Vec<IoBuffer>,
}

enum MirrorWorkersResult {
    WorkerDone(IoBuffer),
    ChannelMessage(BackgroundOpReq),
    AdditionalNotifier,
}

enum BackgroundOpReq {
    Quiesce(oneshot::Sender<()>),
    Unquiesce,
    Stop(PollableNodeStopMode, oneshot::Sender<BlockResult<()>>),

    GoActive,

    ChangeChildrenDo(
        Arc<NodeUser>,
        Arc<NodeUser>,
        oneshot::Sender<BlockResult<()>>,
    ),
    ChangeChildrenClean(oneshot::Sender<()>),
    ChangeChildrenRollBack(oneshot::Sender<()>),
}

struct InFlight {
    inner: Arc<Mutex<InFlightMut>>,
    granularity: u64,
}

struct InFlightMut {
    drained: bool,
    drain_waiters: Vec<oneshot::Sender<()>>,

    bitmap: DirtyBitmap,
    requests: Vec<InFlightRequest>,
    progress_in_flight: u64,
    progress_done: u64,

    blocker_id_counter: u64,
}

struct InFlightRequest {
    id: u64,
    start: u64,
    end: u64,
    waiters: Vec<oneshot::Sender<()>>,
}

struct InFlightBlocker<'a> {
    id: u64,
    progress: u64,
    in_flight: &'a Mutex<InFlightMut>,
}

struct InFlightDrain<'a> {
    in_flight: &'a Mutex<InFlightMut>,
}

impl Config {
    pub fn split_tree(&mut self, vec: &mut Vec<NodeConfig>) -> BlockResult<()> {
        self.file.split_tree(vec)?;
        self.target.split_tree(vec)?;
        Ok(())
    }
}

fn source_node_perms(init: NodeUserBuilder) -> NodeUserBuilder {
    init.require(NodePerm::ConsistentRead)
        .block(NodePerm::Write)
        .block(NodePerm::Resize)
}

fn target_node_perms(init: NodeUserBuilder) -> NodeUserBuilder {
    init.require(NodePerm::Write)
        .block(NodePerm::ConsistentRead)
        .block(NodePerm::Resize)
}

fn job_status_change(shared_data: &SharedData, new_status: qmp::JobStatus) {
    *shared_data.status.lock().unwrap() = new_status;
    broadcast_event(qmp::Event::new(qmp::Events::JobStatusChange {
        id: shared_data.job_id.clone(),
        status: new_status,
    }))
}

impl Data {
    pub async fn new(
        node_name: &str,
        opts: Config,
        read_only: bool,
        _cache: &NodeCacheConfig,
    ) -> BlockResult<Box<Self>> {
        if read_only {
            return Err("The copy node cannot be used in read-only mode".into());
        }

        let simultaneous = opts.immutable.simultaneous.unwrap_or(16);
        if simultaneous == 0 {
            return Err("Number of simultaneous requests must be positive".into());
        }

        let granularity = opts.immutable.granularity.unwrap_or(65536);
        if granularity < 512 {
            return Err("Granularity must at least be 512".into());
        }

        let buf_size = opts
            .immutable
            .buf_size
            .unwrap_or_else(|| std::cmp::max(1048576, granularity));
        if buf_size % granularity != 0 {
            return Err(format!(
                "buf-size ({}) must be a multiple of the granularity ({}), but it is not",
                buf_size, granularity
            )
            .into());
        }

        // When adding multiple nodes simultaneously, it is possible that the copy job (when
        // started in `active` mode) can finish before all of its parents are added, resulting in a
        // potential race (or at least an unclear post-finalize graph state).  Disallow
        // auto-finalize in this case.
        if opts.active && opts.immutable.auto_finalize {
            return Err("Cannot use auto-finalize when active is immediately set".into());
        }

        let mut file_user = NodeUser::builder(node_name, "file");
        if opts.active {
            file_user = source_node_perms(file_user);
        }
        let file = opts.file.lookup()?.add_user(file_user).await?;

        let mut target_user = NodeUser::builder(node_name, "target");
        if opts.active {
            target_user = target_node_perms(target_user);
        }
        let target = opts.target.lookup()?.add_user(target_user).await?;

        let source_size = file.node().size();
        let target_size = target.node().size();
        if source_size > target_size {
            return Err(format!(
                "Size of the source exceeds that of the target ({} vs. {})",
                source_size, target_size
            )
            .into());
        }

        let job_type = if opts.immutable.mode == CopyMode::CopyBeforeWrite {
            qmp::JobType::Backup
        } else {
            qmp::JobType::Mirror
        };

        let mut dirty_bitmap = DirtyBitmap::new(source_size, granularity as u64, true)?;
        match opts.immutable.sync {
            // `None` means a cleared bitmap (usually)
            SyncMode::None => {
                if opts.immutable.mode == CopyMode::CopyBeforeWrite {
                    // A literal `None` does not make much sense in CBW mode, because then we'd be
                    // done immediately.  qemu therefore interprets this as "full, but without
                    // copying anything in the background".  Let's do the same.
                    dirty_bitmap.dirty(0, source_size);
                }
            }

            // `Full` means a fully dirty bitmap
            SyncMode::Full => dirty_bitmap.dirty(0, source_size),
        }

        let in_flight = InFlight::new(source_size, granularity as u64)?;

        let shared_data = Arc::new(SharedData {
            job_id: opts.immutable.job_id.clone(),
            job_type,
            mode: opts.immutable.mode,
            sync: opts.immutable.sync,
            granularity: granularity as u64,
            simultaneous,
            dirty_bitmap: Arc::new(Mutex::new(dirty_bitmap)),
            in_flight,
            auto_finalize: opts.immutable.auto_finalize,
            auto_dismiss: opts.immutable.auto_dismiss.unwrap_or(true),
            active: AtomicBool::new(opts.active),
            status: Mutex::new(qmp::JobStatus::Undefined),
            error: Mutex::new(None),
            completed: AtomicBool::new(false),
            switched_over: AtomicBool::new(false),
        });

        let thread = monitor::monitor().get_thread_from_opt(&opts.immutable.iothread)?;

        let (channel_w, channel_r) = mpsc::unbounded_channel::<BackgroundOpReq>();

        let mirror_background = thread
            .run({
                let source = Arc::clone(&file);
                let target = Arc::clone(&target);
                let shared_data = Arc::clone(&shared_data);

                move || -> BlockFutureResult<_> {
                    Box::pin(async move {
                        let source_queue = source.new_queue()?;
                        let target_queue = target.new_queue()?;

                        // In CBW mode, complete as soon as we have reached synchonization; in
                        // other modes, wait for an explicit command from the user.
                        let should_complete = shared_data.mode == CopyMode::CopyBeforeWrite;

                        // If we are to complete even without user input, there is no need to send
                        // READY, so set this bool from the beginning.
                        let emitted_ready = should_complete;

                        let mut mb = MirrorBackground {
                            empty_bufs: Vec::new(),
                            buf_size,
                            shared_data,
                            channel: channel_r,
                            source_queue,
                            target_queue,
                            should_complete,
                            should_switch_over: false,
                            emitted_ready,
                        };
                        mb.set_up_bufs()?;

                        // Safe because we will either start the background operation, thus
                        // consuming this; or we will correctly drop this in drain_caches().
                        Ok(unsafe { ThreadBound::new_unsafe(mb) })
                    })
                }
            })
            .await?;

        let mut_data = MutData {
            file,
            target,
            switch_to_active: false,
            pre_reopen_file: None,
            pre_reopen_target: None,
        };

        let background_operation = MirrorBackgroundOpData {
            thread,
            mirror_background,
        };

        // Safe because all operations that could drop this (`background_operation()` and
        // `drain_caches()`) are run in the main thread
        let background_operation = unsafe { ThreadBound::new_unsafe(background_operation) };

        Ok(Box::new(Data {
            mut_data: Arc::new(Mutex::new(mut_data)),
            shared_data,
            immutable_config: opts.immutable,
            background_operation,
            background_channel: channel_w,
        }))
    }
}

#[async_trait]
impl NodeDriverData for Data {
    fn background_operation(
        &self,
    ) -> Option<(BoxedFuture<'static, BackgroundOpResult>, Option<ServerNode>)> {
        job_status_change(&self.shared_data, qmp::JobStatus::Created);

        let op = self.background_operation.take().unwrap();
        let shared_data = Arc::clone(&self.shared_data);

        Some((
            Box::pin(async move {
                if shared_data.active.load(Ordering::Relaxed) {
                    job_status_change(&shared_data, qmp::JobStatus::Running);
                }

                op.thread
                    .owned_run(move || Box::pin(op.mirror_background.unwrap().run()))
                    .await;

                if shared_data.error.lock().unwrap().is_none() {
                    job_status_change(&shared_data, qmp::JobStatus::Pending);
                }

                BackgroundOpResult {
                    result: Ok(()),
                    auto_fade: shared_data.auto_finalize,
                }
            }),
            None,
        ))
    }

    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        let data = self.mut_data.lock().unwrap();
        let (source, target) = (data.file.node(), data.target.node());

        let (req_align, mem_align) = if self.shared_data.mode == CopyMode::CopyBeforeWrite {
            // No switch-over in CBW mode, alignment is determined solely by the source
            (source.request_align(), source.mem_align())
        } else {
            // Be ready for potential switch-over: Require alignment to source and target
            (
                std::cmp::max(source.request_align(), target.request_align()),
                std::cmp::max(source.mem_align(), target.mem_align()),
            )
        };

        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(source.size()),
                request_alignment: AtomicUsize::new(req_align),
                memory_alignment: AtomicUsize::new(mem_align),
            },
        })
    }

    fn block_job_info(&self) -> Option<JobInfo> {
        Some(self.shared_data.block_job_info())
    }

    fn get_successor(&self) -> Option<Arc<Node>> {
        let mut_data = self.mut_data.lock().unwrap();

        let successor = match self.shared_data.switched_over.load(Ordering::Relaxed) {
            true => &mut_data.target,
            false => &mut_data.file,
        };

        Some(Arc::clone(successor.node()))
    }

    fn drain_caches(&mut self) -> InfallibleFuture {
        Box::pin(async move {
            if let Some(bg_op) = self.background_operation.take() {
                // Background operation was never started, drop it in its thread
                bg_op
                    .thread
                    .owned_run(move || {
                        Box::pin(async move {
                            let _: MirrorBackground = bg_op.mirror_background.unwrap();
                        })
                    })
                    .await;
            }

            job_status_change(&self.shared_data, qmp::JobStatus::Concluded);
            let mon = monitor::monitor();
            mon.job_concluded(self.shared_data.block_job_info());
            if self.shared_data.auto_dismiss {
                let _: BlockResult<()> = mon.job_dismiss(&self.shared_data.job_id);
            }
        })
    }

    fn new_queue(&self) -> BlockResult<Box<dyn IoQueueDriverData>> {
        let data = self.mut_data.lock().unwrap();
        Ok(Box::new(Queue {
            file: data.file.new_queue()?,
            target: data.target.new_queue()?,
            mut_data: Arc::clone(&self.mut_data),
            shared_data: Arc::clone(&self.shared_data),
            pre_reopen_file: None,
            pre_reopen_target: None,
        }))
    }

    fn get_children(&self) -> Vec<Arc<Node>> {
        let data = self.mut_data.lock().unwrap();
        let file = Arc::clone(data.file.node());
        let target = Arc::clone(data.target.node());
        vec![file, target]
    }

    fn get_children_after_reopen(&self, opts: &NodeConfig) -> BlockResult<Vec<Arc<Node>>> {
        let opts: &Config = (&opts.driver).try_into()?;
        let file = opts.file.lookup()?;
        let target = opts.target.lookup()?;
        Ok(vec![file, target])
    }

    fn quiesce(&self) -> InfallibleFuture {
        let (ack_w, ack) = oneshot::channel();
        // Ignore finished background operation; if it is done, we are stopped
        let _: Result<(), _> = self
            .background_channel
            .send(BackgroundOpReq::Quiesce(ack_w));
        Box::pin(async move { ack.await.unwrap_or(()) })
    }

    fn unquiesce(&self) {
        let _: Result<(), _> = self.background_channel.send(BackgroundOpReq::Unquiesce);
    }

    fn stop<'a>(&self, mode: PollableNodeStopMode) -> BlockFutureResult<'a, ()> {
        let (result_w, result) = oneshot::channel();
        // Ignore finished background operation; if it is done, we are stopped
        let _: Result<(), _> = self
            .background_channel
            .send(BackgroundOpReq::Stop(mode, result_w));
        Box::pin(async move { result.await.unwrap_or(Ok(())) })
    }

    fn reopen_change_graph<'a>(
        &'a self,
        opts: &'a NodeConfig,
        perms: NodePermPair,
        read_only: bool,
        step: ChangeGraphStep,
    ) -> BlockFutureResult<'a, ()> {
        Box::pin(async move {
            let opts: &Config = (&opts.driver).try_into()?;
            let new_file = opts.file.lookup()?;
            let new_target = opts.target.lookup()?;
            let mut data = self.mut_data.lock().unwrap();

            if read_only {
                return Err("The copy node cannot be used in read-only mode".into());
            }

            match step {
                ChangeGraphStep::Release => {
                    data.file
                        .set_perms_in_reopen_change_graph(NodePermPair::default())?;

                    data.target
                        .set_perms_in_reopen_change_graph(NodePermPair::default())?;
                }

                ChangeGraphStep::Acquire => {
                    let file_user = NodeUserBuilder::from(data.file.as_ref())
                        .set_perms(NodePermPair::default());
                    let target_user = NodeUserBuilder::from(data.target.as_ref())
                        .set_perms(NodePermPair::default());

                    let (file_user, target_user) = {
                        if !opts.active && !self.shared_data.active.load(Ordering::Relaxed) {
                            // Inactive: Pass everything to the source
                            (file_user.set_perms(perms), target_user)
                        } else if self.shared_data.completed.load(Ordering::Relaxed) {
                            if self.shared_data.switched_over.load(Ordering::Relaxed) {
                                // After switch-over: File is unused, do not take permissions; target
                                // needs the same permissions as our parent takes on us
                                (file_user, target_user.set_perms(perms))
                            } else {
                                // Completed without switch-over: File needs the same permissions as
                                // our parent takes on use; target is unused, do not take permissions
                                (file_user.set_perms(perms), target_user)
                            }
                        } else {
                            // Not yet completed: Normal in-operation permissions
                            (
                                source_node_perms(file_user.set_perms(perms)),
                                target_node_perms(target_user),
                            )
                        }
                    };

                    let new_file = new_file.add_user_in_reopen_change_graph(file_user)?;
                    let new_target = new_target.add_user_in_reopen_change_graph(target_user)?;

                    let old_file = std::mem::replace(&mut data.file, new_file);
                    data.pre_reopen_file.replace(old_file);

                    let old_target = std::mem::replace(&mut data.target, new_target);
                    data.pre_reopen_target.replace(old_target);
                }
            }

            Ok(())
        })
    }

    fn reopen_do(
        &self,
        opts: NodeConfig,
        _perms: NodePermPair,
        read_only: bool,
    ) -> BlockFutureResult<()> {
        Box::pin(async move {
            let opts: Config = opts.driver.try_into()?;

            if read_only {
                return Err("The copy node cannot be used in read-only mode".into());
            }

            if opts.immutable != self.immutable_config {
                return Err("Attempting to change an option that cannot be changed".into());
            }

            let is_active = self.shared_data.active.load(Ordering::Relaxed);
            if opts.active && !is_active {
                self.mut_data.lock().unwrap().switch_to_active = true;
            } else if !opts.active && is_active {
                return Err("Cannot go back to inactive mode".into());
            }

            let (file, target) = {
                let mut_data = self.mut_data.lock().unwrap();
                (Arc::clone(&mut_data.file), Arc::clone(&mut_data.target))
            };
            let (result_w, result) = oneshot::channel();
            // Ignore finished background operation
            let _: Result<(), _> = self
                .background_channel
                .send(BackgroundOpReq::ChangeChildrenDo(file, target, result_w));
            result.await.unwrap_or(Ok(()))
        })
    }

    fn reopen_clean_async(&self) -> InfallibleFuture {
        let switch_to_active = {
            let mut data = self.mut_data.lock().unwrap();
            data.pre_reopen_file.take();
            data.pre_reopen_target.take();

            if data.switch_to_active {
                data.switch_to_active = false;
                true
            } else {
                false
            }
        };
        if switch_to_active {
            self.shared_data.active.store(true, Ordering::Relaxed);
            let _: Result<(), _> = self.background_channel.send(BackgroundOpReq::GoActive);
        }

        let (ack_w, ack) = oneshot::channel();
        // Ignore finished background operation
        let _: Result<(), _> = self
            .background_channel
            .send(BackgroundOpReq::ChangeChildrenClean(ack_w));
        Box::pin(async move { ack.await.unwrap_or(()) })
    }

    fn reopen_roll_back_async(&self) -> InfallibleFuture {
        {
            let mut data = self.mut_data.lock().unwrap();
            if let Some(old_file) = data.pre_reopen_file.take() {
                data.file = old_file;
            }
            if let Some(old_target) = data.pre_reopen_target.take() {
                data.target = old_target;
            }

            data.switch_to_active = false;
        }

        let (ack_w, ack) = oneshot::channel();
        // Ignore finished background operation
        let _: Result<(), _> = self
            .background_channel
            .send(BackgroundOpReq::ChangeChildrenRollBack(ack_w));
        Box::pin(async move { ack.await.unwrap_or(()) })
    }
}

impl Queue {
    fn job_io(
        &self,
        result: BlockResult<()>,
        io_type: IoOperationType,
        action: BlockErrorAction,
    ) -> BlockResult<()> {
        if result.is_ok() {
            return Ok(());
        }

        broadcast_event(qmp::Event::new(qmp::Events::BlockJobError {
            device: self.shared_data.job_id.clone(),
            operation: io_type,
            action,
        }));

        if action == BlockErrorAction::Report {
            result
        } else {
            Ok(())
        }
    }

    async fn lazy_write(&self, buf: IoBufferRef<'_>, offset: u64) -> BlockResult<()> {
        let (write_target, in_flight) = {
            // Create an in-flight request before checking `completed`, as its contract requires.
            // This does not block anything except delay completion.
            let in_flight = self.shared_data.in_flight.pseudo_blocker();
            if self.shared_data.completed.load(Ordering::Relaxed) {
                // Post-completion: Simple write to source or target.  Drop the in-flight request
                // immediately.
                if self.shared_data.switched_over.load(Ordering::Relaxed) {
                    (&self.target, None)
                } else {
                    (&self.file, None)
                }
            } else {
                // Pre-completion: Keep the in-flight request until the write is done, *then* dirty
                // the bitmap.  Dirtying the bitmap after the write is important so we do not start
                // copying that data off until the data is present on the source node.  (If we
                // dirtied the bitmap before, we would need to create an actual in-flight blocker
                // for the affected area, which means we would e.g. need to wait for ongoing
                // operations on that area instead of just ignoring them.  We can ignore them,
                // because they would be in the procecss of copying outdated data anyway, so we
                // might as well completely corrupt it with a concurrent write.)
                (&self.file, Some(in_flight))
            }
        };

        let length = buf.len();
        let result = write_target.write(buf, offset).await;
        if in_flight.is_some() {
            let mut bitmap = self.shared_data.dirty_bitmap.lock().unwrap();
            // We cannot go to completed mode until all in-flight requests have settled, so
            // this cannot have been set while `_in_flight` lives
            assert!(!self.shared_data.completed.load(Ordering::Relaxed));
            bitmap.dirty(offset, length as u64);
        }
        result
    }

    async fn do_single_cbw(&self, offset: u64, length: usize, alignment: usize) -> BlockResult<()> {
        let mut copy_buf = IoBuffer::new(length, alignment)?;

        self.job_io(
            self.file.read(copy_buf.as_mut(), offset).await,
            IoOperationType::Read,
            BlockErrorAction::Report,
        )?;

        self.job_io(
            self.target.write(copy_buf.as_ref(), offset).await,
            IoOperationType::Write,
            BlockErrorAction::Report,
        )?;

        Ok(())
    }

    async fn cbw_write(&self, buf: IoBufferRef<'_>, offset: u64) -> BlockResult<()> {
        // Round area up to granularity
        let cbw_offset = offset & !(self.shared_data.granularity - 1);
        let cbw_end = (offset + buf.len() as u64 + self.shared_data.granularity - 1)
            & !(self.shared_data.granularity - 1);

        let mut in_flight = self
            .shared_data
            .in_flight
            .block_conflicting(cbw_offset, (cbw_end - cbw_offset) as usize)
            .await;

        if self.shared_data.completed.load(Ordering::Relaxed) {
            // Post-completion: Skip copying, just write through to the source.
            return self.file.write(buf, offset).await;
        }

        // In all dirty areas (and only there!), read current data from the source, write it to the
        // target.  Then, on success (only on success), write the new data to the target.

        let cbw_areas: Vec<(u64, usize)> = {
            let mut bitmap = self.shared_data.dirty_bitmap.lock().unwrap();
            let mut areas: Vec<(u64, usize)> = Vec::new();
            let cbw_end = std::cmp::min(cbw_end, bitmap.len());

            let mut offset = cbw_offset;
            while offset < cbw_end {
                let dirty_len = bitmap.get_dirty_area(offset, cbw_end - offset);
                if dirty_len > 0 {
                    let len: usize = dirty_len.try_into().unwrap();
                    bitmap.clear(offset, dirty_len);
                    areas.push((offset, len));
                    in_flight.add_progress(len);
                    offset += dirty_len;
                }
                if offset < cbw_end {
                    offset += bitmap.get_clean_area(offset, cbw_end - offset);
                }
            }

            areas
        };

        let alignment = std::cmp::max(self.file.mem_align(), self.target.mem_align());
        let mut cbw_error: Option<BlockError> = None;
        for (offset, length) in cbw_areas {
            let result = self.do_single_cbw(offset, length, alignment).await;

            if let Err(err) = result {
                let mut bitmap = self.shared_data.dirty_bitmap.lock().unwrap();
                // We cannot get here in completed mode.  That flag will only be set once the dirty
                // bitmap is empty and no requests are in flight anymore.  Since we operate only on
                // previously dirty areas here, and the request is still in flight, the flag cannot
                // be set, and we are allowed to mark the bitmap as dirty.
                assert!(!self.shared_data.completed.load(Ordering::Relaxed));
                bitmap.dirty(offset, length as u64);
                cbw_error.replace(err);
            }
        }

        // Do not overwrite existing data when copying it off failed, instead return an error to
        // the guest
        if let Some(err) = cbw_error {
            return Err(err);
        }

        self.file.write(buf, offset).await
    }

    async fn caw_write(&self, buf: IoBufferRef<'_>, offset: u64) -> BlockResult<()> {
        let mut in_flight = self
            .shared_data
            .in_flight
            .block_conflicting(offset, buf.len())
            .await;
        in_flight.add_progress(buf.len());

        if self.shared_data.completed.load(Ordering::Relaxed) {
            // Post-completion: Skip copying, just write to either source or target.
            let write_target = if self.shared_data.switched_over.load(Ordering::Relaxed) {
                &self.target
            } else {
                &self.file
            };
            return write_target.write(buf, offset).await;
        }

        // Pre-completion: Write the data both to the source and the target, returning only once
        // both are done.  The guest will only sees an error if the write to the source failed.

        // You cannot copy/clone a reference to an IoBuffer, because the guest might modify the
        // buffer while the request is in flight, so both references might eventually see different
        // data in the buffer.  We must create a bounce buffer to create multiple references.
        let alignment = std::cmp::max(self.file.mem_align(), self.target.mem_align());
        let tmp_buf = buf.try_into_owned(alignment)?;

        // Cannot write simultaneously to source and target, because we do not want the target to
        // be more up-to-date than the source.
        self.file.write(tmp_buf.as_ref(), offset).await?;

        let target_result = self.target.write(tmp_buf.as_ref(), offset).await;
        self.job_io(
            target_result.clone(),
            IoOperationType::Write,
            BlockErrorAction::Ignore,
        )?;
        if target_result.is_err() {
            let mut bitmap = self.shared_data.dirty_bitmap.lock().unwrap();
            // We cannot go to completed mode until all in-flight requests have settled, so this
            // cannot have been set while `_in_flight` lives
            assert!(!self.shared_data.completed.load(Ordering::Relaxed));
            bitmap.dirty(offset, tmp_buf.len() as u64);
        }

        Ok(())
    }
}

impl IoQueueDriverData for Queue {
    fn read<'a>(&'a self, buf: IoBufferMut<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        let read_source = if self.shared_data.switched_over.load(Ordering::Relaxed) {
            &self.target
        } else {
            &self.file
        };
        read_source.read(buf, offset)
    }

    fn write<'a>(&'a self, buf: IoBufferRef<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        // Skip any copying while inactive
        if !self.shared_data.active.load(Ordering::Relaxed) {
            return self.file.write(buf, offset);
        }

        match self.shared_data.mode {
            CopyMode::Lazy => Box::pin(self.lazy_write(buf, offset)),

            CopyMode::CopyBeforeWrite => Box::pin(self.cbw_write(buf, offset)),

            CopyMode::CopyAfterWrite => Box::pin(self.caw_write(buf, offset)),
        }
    }

    fn flush(&self) -> BlockFutureResult<'_, ()> {
        Box::pin(async move {
            let result = futures::join!(self.file.flush(), self.target.flush());
            self.job_io(result.1, IoOperationType::Flush, BlockErrorAction::Ignore)?;
            result.0
        })
    }

    fn reopen_do(&mut self) -> BlockResult<()> {
        let data = self.mut_data.lock().unwrap();
        let new_file = data.file.new_queue()?;
        let new_target = data.target.new_queue()?;

        let old_file = std::mem::replace(&mut self.file, new_file);
        self.pre_reopen_file.replace(old_file);

        let old_target = std::mem::replace(&mut self.target, new_target);
        self.pre_reopen_target.replace(old_target);

        Ok(())
    }

    fn reopen_clean(&mut self) {
        self.pre_reopen_file.take();
        self.pre_reopen_target.take();
    }

    fn reopen_roll_back(&mut self) {
        let old_file = self.pre_reopen_file.take().unwrap();
        self.file = old_file;

        let old_target = self.pre_reopen_target.take().unwrap();
        self.target = old_target;
    }
}

impl MirrorBackground {
    fn set_up_bufs(&mut self) -> BlockResult<()> {
        let mem_align = self.source_queue.mem_align();

        self.empty_bufs.clear();
        for _ in 0..self.shared_data.simultaneous {
            self.empty_bufs
                .push(IoBuffer::new(self.buf_size, mem_align)?);
        }
        Ok(())
    }

    async fn run(mut self) {
        let mut active = self.shared_data.active.load(Ordering::Relaxed);
        let mut quiesced = false;
        let mut pre_reopen_source_queue: Option<IoQueue> = None;
        let mut pre_reopen_target_queue: Option<IoQueue> = None;
        let mut pre_reopen_empty_bufs: Option<Vec<IoBuffer>> = None;
        // `SyncMode::None` in CBW mode means to have an initially fully dirty bitmap, but to skip
        // all background copying
        let skip_background = self.shared_data.sync == SyncMode::None
            && self.shared_data.mode == CopyMode::CopyBeforeWrite;

        loop {
            let msg = if quiesced || !active || skip_background {
                self.channel.recv().await.unwrap()
            } else {
                match self.get_ref().run().await {
                    Some(msg) => msg,
                    None => return, // done
                }
            };

            match msg {
                BackgroundOpReq::Quiesce(ack) => {
                    // Quiescing has already been done by virtue of `run_with_queues()` having returned

                    let status = *self.shared_data.status.lock().unwrap();
                    if let Some(new_status) = match status {
                        qmp::JobStatus::Running => Some(qmp::JobStatus::Paused),
                        qmp::JobStatus::Ready => Some(qmp::JobStatus::Standby),
                        _ => None, // no defined paused status otherwise
                    } {
                        job_status_change(&self.shared_data, new_status);
                    }

                    ack.send(()).unwrap();

                    quiesced = true;
                }

                BackgroundOpReq::Unquiesce => {
                    let status = *self.shared_data.status.lock().unwrap();
                    if let Some(new_status) = match status {
                        qmp::JobStatus::Paused => Some(qmp::JobStatus::Running),
                        qmp::JobStatus::Standby => Some(qmp::JobStatus::Ready),
                        _ => None, // maybe the status changed while quiesced, keep it as-is
                    } {
                        job_status_change(&self.shared_data, new_status);
                    }

                    quiesced = false;
                }

                BackgroundOpReq::Stop(PollableNodeStopMode::Safe, ack) => {
                    ack.send(Err("'safe' stop mode not supported by copy node".into()))
                        .unwrap();
                }

                BackgroundOpReq::Stop(PollableNodeStopMode::Hard, ack) => {
                    ack.send(Ok(())).unwrap();
                    self.shared_data
                        .error
                        .lock()
                        .unwrap()
                        .get_or_insert(String::from("Job aborted"));
                    return;
                }

                BackgroundOpReq::Stop(PollableNodeStopMode::CopyComplete { switch_over }, ack) => {
                    let result = if self.should_complete {
                        Err("Already completing".into())
                    } else if switch_over && self.shared_data.mode == CopyMode::CopyBeforeWrite {
                        Err("Cannot switch over to the target in copy-before-write mode".into())
                    } else {
                        self.should_complete = true;
                        self.should_switch_over = switch_over;
                        Ok(())
                    };
                    ack.send(result).unwrap();
                }

                BackgroundOpReq::GoActive => {
                    active = true;
                    job_status_change(&self.shared_data, qmp::JobStatus::Running);
                }

                BackgroundOpReq::ChangeChildrenDo(source, target, ack) => {
                    let result = match (source.new_queue(), target.new_queue()) {
                        (Ok(source), Ok(target)) => {
                            let old_source = std::mem::replace(&mut self.source_queue, source);
                            let old_target = std::mem::replace(&mut self.target_queue, target);
                            pre_reopen_source_queue = Some(old_source);
                            pre_reopen_target_queue = Some(old_target);

                            let empty_bufs = std::mem::take(&mut self.empty_bufs);
                            match self.set_up_bufs() {
                                Ok(()) => {
                                    pre_reopen_empty_bufs = Some(empty_bufs);
                                    Ok(())
                                }
                                Err(err) => {
                                    self.source_queue = pre_reopen_source_queue.take().unwrap();
                                    self.target_queue = pre_reopen_target_queue.take().unwrap();
                                    self.empty_bufs = empty_bufs;
                                    Err(err)
                                }
                            }
                        }
                        (Err(err), Ok(_)) => Err(err),
                        (Ok(_), Err(err)) => Err(err),
                        (Err(e1), Err(e2)) => Err(format!("{}; {}", e1, e2).into()),
                    };
                    ack.send(result).unwrap();
                }

                BackgroundOpReq::ChangeChildrenClean(ack) => {
                    pre_reopen_source_queue.take();
                    pre_reopen_target_queue.take();
                    ack.send(()).unwrap();
                }

                BackgroundOpReq::ChangeChildrenRollBack(ack) => {
                    if let Some(queue) = pre_reopen_source_queue.take() {
                        self.source_queue = queue;
                    }
                    if let Some(queue) = pre_reopen_target_queue.take() {
                        self.target_queue = queue;
                    }
                    if let Some(empty_bufs) = pre_reopen_empty_bufs.take() {
                        self.empty_bufs = empty_bufs;
                    }
                    ack.send(()).unwrap();
                }
            }
        }
    }

    fn get_ref(&mut self) -> MirrorBackgroundRef<'_> {
        let shared_data = self.shared_data.as_ref();
        MirrorBackgroundRef {
            empty_bufs: &mut self.empty_bufs,
            buf_size: self.buf_size,
            shared_data,
            channel: &mut self.channel,
            workers: MirrorWorkers::new(shared_data, &self.source_queue, &self.target_queue),
            should_complete: self.should_complete,
            should_switch_over: self.should_switch_over,
            emitted_ready: &mut self.emitted_ready,
        }
    }
}

impl<'a> MirrorBackgroundRef<'a> {
    fn new_bitmap_iter(&self) -> DirtyBitmapIterator {
        DirtyBitmapIterator::new(
            Arc::clone(&self.shared_data.dirty_bitmap),
            self.buf_size as u64,
            false,
        )
    }

    async fn run(mut self) -> Option<BackgroundOpReq> {
        let mut iter = self.new_bitmap_iter();
        loop {
            let bitmap_dirty_notifier = loop {
                while let Some(buf) = self.empty_bufs.pop() {
                    // This only finds dirty areas, but will not clear them.  We must not clear the
                    // dirty bitmap until a respective in-flight request has been created.  That is
                    // very important in CBW mode, because active I/O will modify the target if the
                    // dirty bitmap is clean and there is no in-flight request; such a modification
                    // would corrupt the data we want to back up.
                    let area = match iter.next() {
                        Some(area) => area,
                        None => {
                            iter = self.new_bitmap_iter();
                            match iter.next() {
                                Some(area) => area,
                                None => {
                                    self.empty_bufs.push(buf);
                                    break;
                                }
                            }
                        }
                    };

                    // This will create a future that creates an in-flight request (awaiting
                    // conflicting ones), then checks the dirty bitmap whether it is still dirty,
                    // and if so, clears it and begins the actual I/O.  If the bitmap is no longer
                    // dirty, it will just return without doing anything.
                    // That means the dirty bitmap is not cleared here still, and so at the end of
                    // the copy operation, we will create multiple requests here for the same area.
                    // Only one of them will actually do something, so that is not harmful.
                    // (We could create the in-flight request here and clear the dirty bitmap right
                    // here in this loop, but that might cause deadlocks if the in-flight request
                    // is in conflict with an existing background request.)
                    self.workers.add(area, buf);
                }

                if !self.workers.is_empty() {
                    // We found work to do, await it
                    break None;
                } else if self.shared_data.dirty_bitmap.lock().unwrap().is_empty() {
                    // No work to do, and the bitmap does seem empty, so emit READY if necessary.
                    // Then, if we are supposed to complete, drain all in-flight active requests,
                    // check the dirty bitmap again, and if it is still empty, complete.
                    if !*self.emitted_ready {
                        *self.emitted_ready = true;

                        let (done, remaining) = {
                            let in_flight = self.shared_data.in_flight.inner.lock().unwrap();
                            (in_flight.progress_done, in_flight.progress_in_flight)
                        };

                        broadcast_event(qmp::Event::new(qmp::Events::BlockJobReady {
                            job_type: self.shared_data.job_type,
                            device: self.shared_data.job_id.clone(),
                            len: done + remaining,
                            offset: done,
                            speed: 0,
                        }));

                        job_status_change(self.shared_data, qmp::JobStatus::Ready);
                    }

                    if self.should_complete {
                        let _in_flight_drain = self.shared_data.in_flight.drain().await;

                        let bitmap = self.shared_data.dirty_bitmap.lock().unwrap();
                        if bitmap.is_empty() {
                            // Bitmap is empty, and we have no in-flight requests -- we are done
                            if self.should_switch_over {
                                self.shared_data
                                    .switched_over
                                    .store(true, Ordering::Relaxed);
                            }
                            self.shared_data.completed.store(true, Ordering::Relaxed);
                            return None;
                        }
                        // Bitmap is not empty anymore?  Loop once more, creating a worker to
                        // operate on a dirty area.
                    } else {
                        let mut bitmap = self.shared_data.dirty_bitmap.lock().unwrap();
                        if bitmap.is_empty() {
                            // The bitmap is empty, but we are not supposed to complete yet; create a
                            // notifier to fire when the bitmap becomes dirty again
                            break Some(bitmap.add_dirty_notifier());
                        }
                        // Bitmap is not empty anymore?  Loop once more, creating a worker to
                        // operate on a dirty area.
                    }
                }
            };

            match self
                .workers
                .await_one(self.channel, bitmap_dirty_notifier)
                .await
            {
                MirrorWorkersResult::WorkerDone(buffer) => {
                    self.empty_bufs.push(buffer);
                }

                MirrorWorkersResult::ChannelMessage(msg) => {
                    let mut buffers_done = self.workers.await_all().await;
                    self.empty_bufs.append(&mut buffers_done);
                    return Some(msg);
                }

                // Bitmap got a new dirty area: Loop without doing anything
                MirrorWorkersResult::AdditionalNotifier => (),
            }
        }
    }
}

impl<'a> MirrorWorkers<'a> {
    fn new(
        shared_data: &'a SharedData,
        source_queue: &'a IoQueue,
        target_queue: &'a IoQueue,
    ) -> Self {
        MirrorWorkers {
            workers: Vec::new(),
            source_queue,
            target_queue,
            shared_data,
        }
    }

    fn is_empty(&self) -> bool {
        self.workers.is_empty()
    }

    fn add(&mut self, area: DirtyBitmapArea, mut buffer: IoBuffer) {
        let offset = area.offset;
        let length: usize = area.length.try_into().unwrap();
        let source = self.source_queue;
        let target = self.target_queue;
        let job_id = &self.shared_data.job_id;
        let dirty_bitmap = &self.shared_data.dirty_bitmap;
        let in_flight = &self.shared_data.in_flight;

        assert!(length <= buffer.len());

        let fut = Box::pin(async move {
            // As explained in `MirrorBackgroundRef::run()`, wait for conflicting requests, then
            // double-check the dirty bitmap.  If it is not fully dirty, skip this range, and have
            // the `MirrorBackgroundRef::run()` loop find another one.
            let mut in_flight = in_flight.block_conflicting(area.offset, length).await;

            {
                let mut bitmap = dirty_bitmap.lock().unwrap();
                if bitmap.clean_in_range(area.offset, area.length) {
                    return buffer;
                }
                bitmap.clear(area.offset, area.length);
            }

            in_flight.add_progress(length);

            if source
                .read(buffer.as_mut_range(0..length), offset)
                .await
                .is_err()
            {
                dirty_bitmap.lock().unwrap().dirty(area.offset, area.length);

                // TODO: Allow setting on-source-error, for the following alternatives:
                // - Report: Abort the job with error
                // - Stop: Pause the job
                // - Enospc: Stop on enospc, report otherwise
                broadcast_event(qmp::Event::new(qmp::Events::BlockJobError {
                    device: job_id.clone(),
                    operation: IoOperationType::Read,
                    action: BlockErrorAction::Ignore,
                }));

                return buffer;
            }

            if target
                .write(buffer.as_ref_range(0..length), offset)
                .await
                .is_err()
            {
                dirty_bitmap.lock().unwrap().dirty(area.offset, area.length);

                // TODO: Allow setting on-target-error, for the following alternatives:
                // - Report: Abort the job with error
                // - Stop: Pause the job
                // - Enospc: Stop on enospc, report otherwise
                broadcast_event(qmp::Event::new(qmp::Events::BlockJobError {
                    device: job_id.clone(),
                    operation: IoOperationType::Write,
                    action: BlockErrorAction::Ignore,
                }));

                return buffer;
            }

            buffer
        });

        self.workers.push(fut);
    }

    fn await_one<'b>(
        &'b mut self,
        channel: &'b mut mpsc::UnboundedReceiver<BackgroundOpReq>,
        additional_notifier: Option<oneshot::Receiver<()>>,
    ) -> MirrorWorkersFut<'b, 'a> {
        MirrorWorkersFut {
            workers: self,
            channel,
            additional_notifier,
        }
    }

    fn await_all(self) -> MirrorWorkersFutAll<'a> {
        MirrorWorkersFutAll {
            workers: self,
            collected_buffers: Vec::new(),
        }
    }
}

impl<'a, 'b> Future for MirrorWorkersFut<'a, 'b> {
    type Output = MirrorWorkersResult;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if let Poll::Ready(res) = self.channel.poll_recv(cx) {
            return Poll::Ready(MirrorWorkersResult::ChannelMessage(res.unwrap()));
        }

        if let Some(notifiee) = self.additional_notifier.as_mut() {
            // Ignore potential errors
            if Pin::new(notifiee).poll(cx).is_ready() {
                return Poll::Ready(MirrorWorkersResult::AdditionalNotifier);
            }
        }

        let workers = &mut self.workers.workers;
        for i in 0..workers.len() {
            if let Poll::Ready(buffer) = Future::poll(workers[i].as_mut(), cx) {
                workers.swap_remove(i);
                return Poll::Ready(MirrorWorkersResult::WorkerDone(buffer));
            }
        }

        Poll::Pending
    }
}

impl<'a> Future for MirrorWorkersFutAll<'a> {
    type Output = Vec<IoBuffer>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        loop {
            let mut any_ready = false;
            let mut i = 0;
            while i < self.workers.workers.len() {
                if let Poll::Ready(buffer) = Future::poll(self.workers.workers[i].as_mut(), cx) {
                    self.workers.workers.swap_remove(i);
                    self.collected_buffers.push(buffer);
                    any_ready = true;
                } else {
                    i += 1;
                }
            }
            if !any_ready {
                break;
            }
        }

        if self.workers.workers.is_empty() {
            let mut ret = Vec::new();
            // Drain `collected_buffers` into a new vector
            ret.append(&mut self.collected_buffers);
            Poll::Ready(ret)
        } else {
            Poll::Pending
        }
    }
}

impl InFlight {
    fn new(length: u64, granularity: u64) -> BlockResult<Self> {
        let bitmap = DirtyBitmap::new(length, granularity, true)?;
        let inner = InFlightMut {
            drained: false,
            drain_waiters: Vec::new(),
            bitmap,
            requests: Vec::new(),
            progress_in_flight: 0,
            progress_done: 0,
            blocker_id_counter: 0,
        };
        Ok(InFlight {
            inner: Arc::new(Mutex::new(inner)),
            granularity,
        })
    }

    fn overlaps(start_a: u64, end_a: u64, start_b: u64, end_b: u64) -> bool {
        start_a < end_b && start_b < end_a
    }

    /// Wait on all conflicting requests, then enter itself as an in-flight requests to block
    /// future conflicting requests (until the returned blocker is dropped).
    async fn block_conflicting(&self, offset: u64, length: usize) -> InFlightBlocker<'_> {
        // Round up to the in-flight bitmap's granularity for the purpose of checking overlaps
        let granularity_mask = !(self.granularity - 1);
        let end = (offset + length as u64 + self.granularity - 1) & granularity_mask;
        let offset = offset & granularity_mask;
        let length = end - offset;

        let id = loop {
            let notifiee = {
                let mut in_flight = self.inner.lock().unwrap();
                if in_flight.drained {
                    let (notifier, notifiee) = oneshot::channel();
                    in_flight.drain_waiters.push(notifier);
                    notifiee
                } else if in_flight.bitmap.dirty_in_range(offset, length) {
                    let conflicting = in_flight
                        .requests
                        .iter_mut()
                        .find(|req| Self::overlaps(offset, end, req.start, req.end))
                        .unwrap();

                    let (notifier, notifiee) = oneshot::channel();
                    conflicting.waiters.push(notifier);
                    notifiee
                } else {
                    let id = in_flight.blocker_id_counter;
                    in_flight.blocker_id_counter = in_flight.blocker_id_counter.wrapping_add(1);
                    let request = InFlightRequest {
                        id,
                        start: offset,
                        end,
                        waiters: Vec::new(),
                    };
                    in_flight.requests.push(request);
                    in_flight.bitmap.dirty(offset, length);
                    break id;
                }
            };

            let _: Result<(), _> = notifiee.await;
        };

        InFlightBlocker {
            id,
            progress: 0,
            in_flight: &self.inner,
        }
    }

    /// Blockers created with this function do not protect any specific area, but just prevent the
    /// job from going into completed/ready state.
    fn pseudo_blocker(&self) -> InFlightBlocker<'_> {
        let mut in_flight = self.inner.lock().unwrap();
        let id = in_flight.blocker_id_counter;
        in_flight.blocker_id_counter = in_flight.blocker_id_counter.wrapping_add(1);

        let request = InFlightRequest {
            id,
            start: 0,
            end: 0,
            waiters: Vec::new(),
        };

        let blocker = InFlightBlocker {
            id,
            progress: 0,
            in_flight: &self.inner,
        };

        in_flight.requests.push(request);
        blocker
    }

    async fn drain(&self) -> InFlightDrain<'_> {
        let waiters: Vec<oneshot::Receiver<()>> = {
            let mut in_flight = self.inner.lock().unwrap();
            // Nested drains are not supported
            assert!(!in_flight.drained);
            in_flight.drained = true;

            in_flight
                .requests
                .iter_mut()
                .map(|req| {
                    let (notifier, notifiee) = oneshot::channel();
                    req.waiters.push(notifier);
                    notifiee
                })
                .collect()
        };

        for waiter in waiters {
            let _: Result<(), _> = waiter.await;
        }

        InFlightDrain {
            in_flight: &self.inner,
        }
    }
}

impl<'a> InFlightBlocker<'a> {
    fn add_progress(&mut self, length: usize) {
        let length = length as u64;
        self.progress += length;
        self.in_flight.lock().unwrap().progress_in_flight += length;
    }
}

impl<'a> Drop for InFlightBlocker<'a> {
    fn drop(&mut self) {
        let req = {
            let mut in_flight = self.in_flight.lock().unwrap();
            let mut found = None;
            for i in 0..in_flight.requests.len() {
                let req = &mut in_flight.requests[i];
                if req.id == self.id {
                    found = Some(in_flight.requests.swap_remove(i));
                    break;
                }
            }
            let req = found.unwrap();
            let length = req.end - req.start;
            in_flight.bitmap.clear(req.start, length);
            in_flight.progress_in_flight -= self.progress;
            in_flight.progress_done += self.progress;
            req
        };
        for waiter in req.waiters {
            let _: Result<(), _> = waiter.send(());
        }
    }
}

impl<'a> Drop for InFlightDrain<'a> {
    fn drop(&mut self) {
        let waiters: Vec<oneshot::Sender<()>> = {
            let mut in_flight = self.in_flight.lock().unwrap();
            in_flight.drained = false;
            in_flight.drain_waiters.drain(..).collect()
        };

        for waiter in waiters {
            let _: Result<(), _> = waiter.send(());
        }
    }
}

impl SharedData {
    fn block_job_info(&self) -> JobInfo {
        let (done, in_flight, remaining) = {
            let in_flight = self.in_flight.inner.lock().unwrap();
            let dirty_bitmap = self.dirty_bitmap.lock().unwrap();

            (
                in_flight.progress_done,
                in_flight.progress_in_flight,
                in_flight.progress_in_flight + dirty_bitmap.dirty_bytes(),
            )
        };

        JobInfo {
            job_type: self.job_type,
            status: *self.status.lock().unwrap(),
            id: self.job_id.clone(),
            done,
            remaining,
            busy: in_flight > 0,
            auto_finalize: self.auto_finalize,
            auto_dismiss: self.auto_dismiss,
            error: self.error.lock().unwrap().clone(),
        }
    }
}

#[allow(clippy::too_many_arguments)]
async fn create_copy_node(
    job_id: String,
    device: String,
    target: String,
    sync: SyncMode,
    granularity: Option<usize>,
    buf_size: Option<usize>,
    filter_node_name: String,
    mode: CopyMode,
    auto_finalize: Option<bool>,
    auto_dismiss: Option<bool>,
) -> BlockResult<NodeConfig> {
    let config = NodeConfig {
        node_name: filter_node_name,
        read_only: Some(false),
        auto_read_only: Some(false),
        cache: NodeCacheConfig::default(),
        driver: NodeDriverConfig::Copy(Config {
            file: NodeConfigOrReference::Reference(device),
            target: NodeConfigOrReference::Reference(target),
            active: false,
            immutable: ImmutableConfig {
                job_id,
                mode,
                sync,
                auto_finalize: auto_finalize.unwrap_or(true),
                auto_dismiss: Some(auto_dismiss.unwrap_or(true)),
                granularity,
                buf_size: buf_size.map(|sz| sz / 16),
                simultaneous: Some(16),
                iothread: None,
            },
        }),
    };

    monitor::monitor().blockdev_add(config.clone()).await?;
    Ok(config)
}

pub async fn insert_copy_node(node_config: NodeConfig) -> BlockResult<()> {
    let copy_node_name = &node_config.node_name;

    let (source, copy) = {
        let copy_config: &Config = (&node_config.driver).try_into().unwrap();
        let mon = monitor::monitor();

        let source_name = match &copy_config.file {
            NodeConfigOrReference::Reference(name) => name,
            _ => unreachable!(),
        };

        (
            mon.lookup_node(source_name)?,
            mon.lookup_node(copy_node_name)?,
        )
    };

    // Simultaneously replace the source node by the copy node and switch the copy node into
    // active mode
    let mut reopen_queue = ReopenQueue::replace_node(&source, copy_node_name)?;

    let node_config = reopen_queue.get_mut_or_push(copy, node_config)?;
    let copy_config: &mut Config = (&mut node_config.driver).try_into().unwrap();
    copy_config.active = true;

    reopen_queue.reopen().await
}

#[allow(clippy::too_many_arguments)]
pub async fn blockdev_mirror(
    job_id: String,
    device: String,
    target: String,
    sync: SyncMode,
    granularity: Option<usize>,
    buf_size: Option<usize>,
    filter_node_name: String,
    copy_mode: Option<qmp::MirrorCopyMode>,
    auto_finalize: Option<bool>,
    auto_dismiss: Option<bool>,
) -> BlockResult<()> {
    let mode = match copy_mode.unwrap_or(qmp::MirrorCopyMode::Background) {
        qmp::MirrorCopyMode::Background => CopyMode::Lazy,
        qmp::MirrorCopyMode::WriteBlocking => CopyMode::CopyAfterWrite,
    };

    let config = create_copy_node(
        job_id,
        device,
        target,
        sync,
        granularity,
        buf_size,
        filter_node_name.clone(),
        mode,
        auto_finalize,
        auto_dismiss,
    )
    .await?;
    if let Err(err) = insert_copy_node(config).await {
        let _: BlockResult<()> = monitor::monitor().blockdev_del(filter_node_name).await;
        return Err(err);
    }
    Ok(())
}

#[allow(clippy::too_many_arguments)]
pub async fn blockdev_backup(
    job_id: String,
    device: String,
    target: String,
    sync: SyncMode,
    granularity: Option<usize>,
    buf_size: Option<usize>,
    filter_node_name: String,
    auto_finalize: Option<bool>,
    auto_dismiss: Option<bool>,
) -> BlockResult<()> {
    let config = create_copy_node(
        job_id,
        device,
        target,
        sync,
        granularity,
        buf_size,
        filter_node_name.clone(),
        CopyMode::CopyBeforeWrite,
        auto_finalize,
        auto_dismiss,
    )
    .await?;
    if let Err(err) = insert_copy_node(config).await {
        let _: BlockResult<()> = monitor::monitor().blockdev_del(filter_node_name).await;
        return Err(err);
    }
    Ok(())
}
