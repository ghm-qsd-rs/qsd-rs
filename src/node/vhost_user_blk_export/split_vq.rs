/**
 * Implementation of split virt queues.
 */
use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, FlatSize};
use crate::node::vhost_user_blk_export::guest_memory::{GuestMemory, MemRegion};
use crate::node::vhost_user_blk_export::protocol::{
    le_u16, VhostUserFeature, VirtqSplitDesc, VirtqUsedElem,
};
use crate::node::vhost_user_blk_export::traits::{DescriptorIterator, VirtioDeviceThreadData};
use crate::node::vhost_user_blk_export::virtq::{InFlight, ReqBuffers, VirtqRingAddresses};
use std::collections::LinkedList;
use std::sync::atomic::Ordering;

/// Everything that belongs to the available ring (written by the guest driver)
struct VirtqAvailRef<'a> {
    /// Flags
    flags: &'a le_u16,
    /// Available index
    idx: &'a le_u16,
    /// The ring itself
    ring: &'a [le_u16],
    /// Only with `RingEventIdx`: The guest driver wants an event when this descriptor index is
    /// marked as used
    used_event: &'a le_u16,
}

/// Everything that belongs to the used ring (written by the device, i.e. us)
struct VirtqUsedRef<'a> {
    /// Flags
    flags: &'a mut le_u16,
    /// Used index
    idx: &'a mut le_u16,
    /// The ring itself
    ring: &'a mut [VirtqUsedElem],
    /// Only with `RingEventIdx`: We want an event when this descriptor index is marked as
    /// available
    avail_event: &'a mut le_u16,
}

/// Represents the technical state of a split virt queue
pub struct VirtqSplitRings<'a> {
    /// Descriptor ring
    descriptor: &'a [VirtqSplitDesc],
    /// Used ring
    used: VirtqUsedRef<'a>,
    /// Available ring
    available: VirtqAvailRef<'a>,

    /// Handle on guest memory
    guest_mem: &'a GuestMemory,

    /// Whether the virt queue is considered running or not; the only implication for us is to
    /// clear `used.flags` once when it begins running
    running: bool,
    /// Queue size minus one, such that `index & queue_mask == index % queue_size`
    queue_mask: u16,
    /// Whether `RingEventIdx` was negotiated or not
    event_idx: bool,

    /// Index in the available ring from which to fetch the next available descriptor
    fetch_index: u16,
    /// Index in the used ring to where to store the next used descriptor
    store_index: u16,
    /// Value of `used.avail_event` (cached so we do not need to read it back to see whether it
    /// needs to be changed)
    cached_avail_index: u16,
}

/// Iterator over a chain of available descriptors
struct SplitDescriptorIterator<'a> {
    /// Handle on guest memory
    guest_mem: &'a GuestMemory,
    /// Cached region for `guest_mem.map_guest_cached()`
    current_region: Option<&'a MemRegion>,
    /// Handle on the descriptor ring; when encountering an indirect descriptor, this will be
    /// replaced with the memory area to which the indirect descriptor points
    descriptor_ring: &'a [VirtqSplitDesc],

    /// The next index from which to fetch a descriptor (`descriptor_ring[current_index]`)
    current_index: Option<u16>,
    /// Whether we have followed an indirect descriptor
    indirect: bool,
    /// Queue mask
    qm: u16,

    /// Collects buffers fetched from the original descriptor ring (with ID and total number of
    /// writable bytes)
    buffers: ReqBuffers,
}

impl<'a> Iterator for SplitDescriptorIterator<'a> {
    type Item = &'a mut [u8];

    fn next(&mut self) -> Option<&'a mut [u8]> {
        let id = self.current_index?;

        let desc = &self.descriptor_ring[(id & self.qm) as usize];

        let addr: u64 = desc.addr.into();
        let size = u32::from(desc.len) as usize;
        let flags: u16 = desc.flags.into();

        let write_size = if flags & 0x6 == 0x2 { size } else { 0 };

        // Store only the head of the descriptor chain
        match &mut self.buffers {
            ReqBuffers::Empty => {
                self.buffers = ReqBuffers::Single((id, write_size));
            }
            ReqBuffers::Single((_buf, len)) => *len += write_size,
            ReqBuffers::List(_) => unreachable!(),
        }

        // Ignore indirect descriptors that do not hold anything
        if flags & 4 != 0 && size >= VirtqSplitDesc::SIZE {
            let count = size / VirtqSplitDesc::SIZE;

            // Follow indirect descriptor
            self.descriptor_ring = self
                .guest_mem
                .map_guest_cached(&mut self.current_region, addr, count)
                .unwrap();

            self.current_index = Some(0);
            self.indirect = true;

            return Iterator::next(self);
        } else if flags & 1 != 0 {
            let next: u16 = desc.next.into();
            self.current_index = Some(next);
        } else {
            self.current_index = None;
        }

        // Skip 0-byte descriptors
        if size == 0 {
            return Iterator::next(self);
        }

        let addr = self
            .guest_mem
            .map_guest_cached(&mut self.current_region, addr, size)
            .unwrap();

        Some(addr)
    }
}

impl<'a> DescriptorIterator<'a> for SplitDescriptorIterator<'a> {
    fn is_empty(&self) -> bool {
        self.current_index.is_none()
    }

    fn get_buffers(self) -> ReqBuffers {
        self.buffers
    }
}

impl<'a> VirtqSplitRings<'a> {
    /// Given the ring addresses, creates a `VirtqSplitRings` object.  Can consume an old one (to
    /// update just the mappings).
    pub fn from_addresses(
        addr: &VirtqRingAddresses,
        guest_mem: &'a GuestMemory,
        queue_size: usize,
        avail_base: u32,
        features: u64,
        existing: Option<&Self>,
    ) -> BlockResult<Self> {
        assert!(features & (VhostUserFeature::RingPacked as u64) == 0);

        let descriptor = guest_mem.map_user::<VirtqSplitDesc>(addr.descriptor_ring, queue_size)?;

        let used = {
            let mut addr = addr.used_ring;
            let flags = guest_mem.map_user::<le_u16>(addr, 1)?;
            addr += le_u16::SIZE as u64;

            let idx = guest_mem.map_user::<le_u16>(addr, 1)?;
            addr += le_u16::SIZE as u64;

            let ring = guest_mem.map_user::<VirtqUsedElem>(addr, queue_size)?;
            addr += (queue_size * VirtqUsedElem::SIZE) as u64;

            let avail_event = guest_mem.map_user::<le_u16>(addr, 1)?;

            VirtqUsedRef {
                flags: &mut flags[0],
                idx: &mut idx[0],
                ring,
                avail_event: &mut avail_event[0],
            }
        };

        let available = {
            let mut addr = addr.available_ring;

            let flags = guest_mem.map_user::<le_u16>(addr, 1)?;
            addr += le_u16::SIZE as u64;

            let idx = guest_mem.map_user::<le_u16>(addr, 1)?;
            addr += le_u16::SIZE as u64;

            let ring = guest_mem.map_user::<le_u16>(addr, queue_size)?;
            addr += (queue_size * le_u16::SIZE) as u64;

            let used_event = guest_mem.map_user::<le_u16>(addr, 1)?;

            VirtqAvailRef {
                flags: &flags[0],
                idx: &idx[0],
                ring,
                used_event: &used_event[0],
            }
        };

        let event_idx = features & (VhostUserFeature::RingEventIdx as u64) != 0;

        let rings = VirtqSplitRings {
            descriptor,
            used,
            available,

            guest_mem,

            running: existing.map(|e| e.running).unwrap_or(false),
            queue_mask: (queue_size - 1).try_into().unwrap(),
            event_idx,

            // For split VQs, we only get a single base index, which is supposed to be the same for
            // both fetching and storing
            fetch_index: existing
                .map(|e| e.fetch_index)
                .unwrap_or_else(|| avail_base.try_into().unwrap()),
            store_index: existing
                .map(|e| e.store_index)
                .unwrap_or_else(|| avail_base.try_into().unwrap()),
            cached_avail_index: existing.map(|e| e.cached_avail_index).unwrap_or(0),
        };

        Ok(rings)
    }

    /// Fetch available descriptors and process them (i.e. create virtio requests based off of
    /// them).  If there are none, and if `set_event` is true, set up notifications for new
    /// descriptors becoming available.
    pub fn poll_avail<T: VirtioDeviceThreadData>(
        &mut self,
        device: &'a T,
        in_flight: &mut LinkedList<InFlight<'a>>,
        set_event: bool,
    ) -> bool {
        if !self.running {
            self.running = true;
            *self.used.flags = 0.into();
        }

        let mut ceiling = u16::from(*self.available.idx);
        if ceiling == self.fetch_index {
            if set_event && self.event_idx && ceiling != self.cached_avail_index {
                loop {
                    *self.used.avail_event = ceiling.into();
                    std::sync::atomic::fence(Ordering::SeqCst);
                    let new_ceiling = u16::from(*self.available.idx);
                    if new_ceiling != ceiling {
                        ceiling = new_ceiling;
                    } else {
                        break;
                    }
                }
                self.cached_avail_index = ceiling;
            }
            if ceiling == self.fetch_index {
                return false;
            }
        }

        let qm = self.queue_mask;
        let mut i = self.fetch_index;
        while i != ceiling {
            let head = self.available.ring[(i & qm) as usize].into();
            if head & !qm == 0 {
                let req = self.new_request(device, head);
                i = i.wrapping_add(req.buffers.len() as u16);
                in_flight.push_back(req);
            } else {
                eprintln!("[vhost-user] Invalid descriptor, skipping");
                i = i.wrapping_add(1);
            }
        }
        self.fetch_index = i;

        true
    }

    /// Create a new request beginning from the given descriptor index
    fn new_request<T: VirtioDeviceThreadData>(
        &mut self,
        device: &'a T,
        first_desc_index: u16,
    ) -> InFlight<'a> {
        let mut it = SplitDescriptorIterator {
            guest_mem: self.guest_mem,
            current_region: None,
            descriptor_ring: self.descriptor,

            current_index: Some(first_desc_index),
            indirect: false,
            qm: self.queue_mask,

            buffers: ReqBuffers::Empty,
        };

        let req: BlockFutureResult<()> = match device.request(&mut it) {
            Ok(x) => x,
            Err(e) => Box::pin(async move { Err(e) }),
        };

        InFlight {
            buffers: it.get_buffers(),
            req,
        }
    }

    /// Settle the given descriptor (i.e. mark as used)
    pub fn settle(&mut self, id: u16, len: usize) {
        let used_entry = &mut self.used.ring[(self.store_index & self.queue_mask) as usize];

        used_entry.id = (id as u32).into();
        used_entry.len = (len as u32).into();

        self.store_index = self.store_index.wrapping_add(1);

        std::sync::atomic::fence(Ordering::SeqCst);
        *self.used.idx = self.store_index.into();
    }

    /// Check whether we need to notify (call) the guest driver about a number of completed
    /// (settled) descriptors; return true if so.
    pub fn notify_guest(&mut self, completed: usize) -> bool {
        if self.event_idx {
            let used_event = u16::from(*self.available.used_event);
            (self.store_index.wrapping_sub(used_event).wrapping_sub(1) as usize) < completed
        } else {
            u16::from(*self.available.flags) & 1 == 0
        }
    }

    pub fn get_base(&self) -> u32 {
        // For split VQs, qemu assumes we only return one index.  Let's hope it's the same for both
        // fetching and storing.
        self.fetch_index as u32
    }
}
