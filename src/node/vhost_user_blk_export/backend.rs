/**
 * vhost-user backend implementation (i.e. mostly communication over the vhost-user socket).
 */
use crate::error::{BlockError, BlockResult};
use crate::helpers::{self, FlatSize, OwnedFd};
use crate::monitor;
use crate::node::exports::BackgroundOpReq;
use crate::node::vhost_user_blk_export::guest_memory::{GuestMemory, MemRegion};
use crate::node::vhost_user_blk_export::protocol::{
    SingleVhostMemRegion, VhostMemRegion, VhostMemRegionHeader, VhostUserFeature, VhostUserMessage,
    VhostUserMessageFlag, VhostUserMessageHeader, VhostUserProtocolFeature, VhostUserRequest,
    VhostVringAddress, VhostVringState, VirtioDeviceConfigSpaceHeader, VringCallFlag,
};
use crate::node::vhost_user_blk_export::traits::VirtioDevice;
use crate::node::vhost_user_blk_export::virtq::{
    CallBatching, VhostUserVirtq, VirtqRingAddresses, VirtqThreadNotification,
};
use crate::node::{NodeUser, PollableNodeStopMode};
use std::collections::VecDeque;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::UnixStream;
use tokio::sync::mpsc;

/// Represents a vhost-user back-end, which consists of the virtio device serving requests, and a
/// number of virt queues.
pub struct VhostUserBackend<V: VirtioDevice> {
    /// Connection to the front-end
    con: Option<UnixStream>,
    /// Whether this device is connected (important for the monitor-request-processing thread to
    /// determine whether a shutdown is safe or not)
    connected: Arc<AtomicBool>,

    /// If the reply_ack feature was negotiated
    reply_ack: bool,

    /// Collection of virt queues
    virtqs: Vec<VhostUserVirtq>,
    /// Handle on the thread that distributes monitor requests to all virt queues
    _request_distributor_thread: monitor::ThreadHandle,
    /// Virtio device serving requests
    device: Arc<V>,
    /// Current memory regions
    mem_regions: Vec<MemRegion>,

    /// Features set during negotiations
    features: u64,

    /// FDs received from the front-end (and not yet processed)
    fd_queue: VecDeque<OwnedFd>,
}

#[derive(Debug)]
pub enum VirtqMonitorRequest {
    Quiesce(mpsc::Sender<()>),
    Unquiesce,
    Stop(mpsc::Sender<()>),
    ChangeChildDo {
        node: Arc<NodeUser>,
        read_only: bool,
        result: mpsc::Sender<BlockResult<()>>,
        /// Whether the export is connected or not (some move operations may only be possible of
        /// the export is not currently connected)
        connected: bool,
    },
    ChangeChildRollBack(mpsc::Sender<()>),
    ChangeChildClean(mpsc::Sender<()>),
}

impl<V: VirtioDevice> VhostUserBackend<V> {
    /// Create a new vhost-user back-end object, and a receiver that will see data when the device
    /// is to be dropped.
    pub async fn new(
        device: V,
        request: mpsc::UnboundedReceiver<BackgroundOpReq>,
        virtq_threads: Vec<Option<String>>,
        call_batching: CallBatching,
        poll_duration: f32,
    ) -> BlockResult<(Self, mpsc::Receiver<()>)> {
        let features = VhostUserFeature::Version1 as u64;

        let device = Arc::new(device);

        let mut broadcasts_s = Vec::<mpsc::UnboundedSender<VirtqMonitorRequest>>::new();

        let mut virtqs = Vec::<VhostUserVirtq>::with_capacity(virtq_threads.len());
        for thread_id in virtq_threads {
            let (bc_s, bc_r) = mpsc::unbounded_channel();
            broadcasts_s.push(bc_s);
            let virtq = VhostUserVirtq::new(
                device.as_ref(),
                bc_r,
                thread_id,
                features,
                call_batching.clone(),
                poll_duration,
            )
            .await?;
            virtqs.push(virtq);
        }

        let connected = Arc::new(AtomicBool::new(false));
        let (quit_s, quit_r) = mpsc::channel::<()>(1);

        let mon = monitor::monitor();
        let request_distributor_thread = mon.add_ephemeral_thread()?;

        request_distributor_thread.spawn({
            let connected = Arc::clone(&connected);
            let device = Arc::clone(&device);

            move || {
                let fut = Self::export_request_distributor(
                    device,
                    connected,
                    request,
                    broadcasts_s,
                    quit_s,
                );
                Box::pin(fut)
            }
        });

        let backend = VhostUserBackend {
            con: None,
            connected,

            reply_ack: false,

            virtqs,
            _request_distributor_thread: request_distributor_thread,
            device,
            mem_regions: Vec::new(),

            features,

            fd_queue: VecDeque::new(),
        };

        Ok((backend, quit_r))
    }

    fn ensure_virtq(&self, index: usize) -> BlockResult<()> {
        if index < self.virtqs.len() {
            Ok(())
        } else {
            Err(format!(
                "Guest requests use of virt queue {}, but maximum index is {}",
                index,
                self.virtqs.len() - 1
            )
            .into())
        }
    }

    /// Receive an object from the front-end along with optional file descriptors (stored in
    /// `fd_queue`)
    async fn read_obj<T: FlatSize>(&mut self) -> BlockResult<T> {
        let mut con = self.con.take().unwrap().into_std().unwrap();
        let result = helpers::read_obj_and_fds(&mut con).await;
        self.con.replace(UnixStream::from_std(con).unwrap());

        let (obj, fds) = result?;
        self.fd_queue.extend(fds);
        Ok(obj)
    }

    /// Send an object to the front-end
    async fn write_obj<T: FlatSize>(&mut self, obj: T) -> BlockResult<()> {
        helpers::write_obj(self.con.as_mut().unwrap(), obj)
            .await
            .map_err(BlockError::from)
    }

    /// Receive *no* request payload from the front-end (i.e. this is a no-op, unless the front-end
    /// specifies that data will be sent, which will be an error)
    fn read_no_payload(&mut self, hdr: &VhostUserMessageHeader) -> BlockResult<()> {
        if hdr.size != 0 {
            let req: VhostUserRequest = hdr.request.try_into().unwrap();
            let actual_size = hdr.size;
            return Err(format!(
                "Expected no payload for vhost-user command {:?}, received {} bytes",
                req, actual_size,
            )
            .into());
        }

        Ok(())
    }

    /// Receive fixed-size request payload from the front-end
    async fn read_payload<T: FlatSize>(&mut self, hdr: &VhostUserMessageHeader) -> BlockResult<T> {
        if hdr.size as usize != T::SIZE {
            let req: VhostUserRequest = hdr.request.try_into().unwrap();
            let actual_size = hdr.size;
            return Err(format!(
                "Expected {} bytes of payload for vhost-user command {:?}, received {} bytes",
                T::SIZE,
                req,
                actual_size,
            )
            .into());
        }

        self.read_obj::<T>().await
    }

    /// Receive the header of some payload from the front-end
    async fn read_payload_header<T: FlatSize>(
        &mut self,
        hdr: &VhostUserMessageHeader,
    ) -> BlockResult<T> {
        if (hdr.size as usize) < T::SIZE {
            let req: VhostUserRequest = hdr.request.try_into().unwrap();
            let actual_size = hdr.size;
            return Err(format!(
                "Expected {} bytes of payload for vhost-user command {:?}, received {} bytes",
                T::SIZE,
                req,
                actual_size,
            )
            .into());
        }

        self.read_obj::<T>().await
    }

    /// Send a reply object for the given request to the front-end
    async fn write_reply<T: FlatSize>(
        &mut self,
        hdr: &VhostUserMessageHeader,
        value: T,
    ) -> BlockResult<()> {
        self.write_obj(VhostUserMessage::new_reply(hdr.request, value))
            .await
    }

    async fn write_ack(&mut self, hdr: &VhostUserMessageHeader) -> BlockResult<()> {
        if self.reply_ack && hdr.flags & (VhostUserMessageFlag::NeedReply as u32) != 0 {
            self.write_reply(hdr, 0u64).await
        } else {
            Ok(())
        }
    }

    /// Verify that no FD is in the receive queue (because none is expected)
    fn verify_no_fd(&self, hdr: &VhostUserMessageHeader) -> BlockResult<()> {
        if self.fd_queue.is_empty() {
            Ok(())
        } else {
            let req: VhostUserRequest = hdr.request.try_into().unwrap();
            Err(format!("Received superfluous FD for vhost-user command {:?}", req,).into())
        }
    }

    /// Pop an FD from the receive queue
    fn pop_fd(&mut self, hdr: &VhostUserMessageHeader) -> BlockResult<OwnedFd> {
        self.fd_queue.pop_front().ok_or_else(|| {
            let req: VhostUserRequest = hdr.request.try_into().unwrap();
            format!("Missing FD for vhost-user command {:?}", req).into()
        })
    }

    /// Update all virtq's guest memory
    async fn update_guest_memory(&self) {
        let guest_mem = Arc::new(GuestMemory::from(self.mem_regions.clone()));
        VhostUserVirtq::broadcast(
            self.virtqs.iter(),
            VirtqThreadNotification::SetGuestMemory(guest_mem),
        )
        .await;
    }

    /// Receive a message from the front-end and process it
    pub async fn process_message(&mut self) -> BlockResult<()> {
        let hdr = self.read_obj::<VhostUserMessageHeader>().await?;
        let req: VhostUserRequest = hdr.request.try_into()?;

        match req {
            VhostUserRequest::GetFeatures => {
                self.read_no_payload(&hdr)?;
                self.verify_no_fd(&hdr)?;

                let features = VhostUserFeature::RingIndirectDesc as u64
                    | VhostUserFeature::RingEventIdx as u64
                    | VhostUserFeature::ProtocolFeatures as u64
                    | VhostUserFeature::Version1 as u64
                    | VhostUserFeature::RingPacked as u64
                    | self.device.get_features();

                self.write_reply(&hdr, features).await?;
            }

            VhostUserRequest::SetFeatures => {
                let features = self.read_payload::<u64>(&hdr).await?;
                self.verify_no_fd(&hdr)?;

                if features & (VhostUserFeature::Version1 as u64) == 0 {
                    todo!("Version1 is required, but could not be negotiated");
                }

                self.device.set_features(features & 0xfff_ffff);

                VhostUserVirtq::broadcast(self.virtqs.iter(), VirtqThreadNotification::Stop).await;

                let enable = features & (VhostUserFeature::ProtocolFeatures as u64) == 0;
                // According to the specification, we must disable all queues if `enable == false`,
                // but in testing the Linux kernel does not seem to take care of enabling the
                // queues afterwards.  Therefore, do not disable them here.
                // See also: https://bugzilla.redhat.com/show_bug.cgi?id=2148352
                if enable {
                    VhostUserVirtq::broadcast(
                        self.virtqs.iter(),
                        VirtqThreadNotification::Enable(enable),
                    )
                    .await;
                }

                VhostUserVirtq::broadcast(
                    self.virtqs.iter(),
                    VirtqThreadNotification::SetFeatures(features),
                )
                .await;

                self.features = features;

                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::SetOwner => {
                self.read_no_payload(&hdr)?;
                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::SetMemTable => {
                let mem_hdr = self
                    .read_payload_header::<VhostMemRegionHeader>(&hdr)
                    .await?;

                let payload_length = hdr.size - VhostMemRegionHeader::SIZE as u32;
                if mem_hdr.num_regions * VhostMemRegion::SIZE as u32 != payload_length {
                    return Err(format!(
                        "vhost-user SetMemTable command's payload size does not match \
                         (expected {}, got {})",
                        mem_hdr.num_regions * VhostMemRegion::SIZE as u32,
                        payload_length
                    )
                    .into());
                }

                VhostUserVirtq::broadcast(
                    self.virtqs.iter(),
                    VirtqThreadNotification::InvalidateMemory,
                )
                .await;

                self.mem_regions = Vec::new();
                for _ in 0..mem_hdr.num_regions {
                    let reg: VhostMemRegion = self.read_obj().await?;
                    let fd = self.pop_fd(&hdr)?;

                    self.mem_regions.push(MemRegion::new(reg, fd)?);
                }

                self.update_guest_memory().await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::GetProtocolFeatures => {
                self.read_no_payload(&hdr)?;
                self.verify_no_fd(&hdr)?;

                let features = VhostUserProtocolFeature::Mq as u64
                    | VhostUserProtocolFeature::ReplyAck as u64
                    | VhostUserProtocolFeature::Config as u64
                    | VhostUserProtocolFeature::ConfigureMemSlots as u64;

                self.write_reply(&hdr, features).await?;
            }

            VhostUserRequest::SetProtocolFeatures => {
                let features = self.read_payload::<u64>(&hdr).await?;

                self.reply_ack = features & (VhostUserProtocolFeature::ReplyAck as u64) != 0;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::GetQueueNum => {
                self.verify_no_fd(&hdr)?;

                let num_queues: u64 = self.device.get_queue_num();

                self.write_reply(&hdr, num_queues).await?;
            }

            VhostUserRequest::SetVringEnable => {
                self.verify_no_fd(&hdr)?;
                let vring = self.read_payload::<VhostVringState>(&hdr).await?;
                let index = vring.index as usize;

                self.ensure_virtq(index)?;

                self.virtqs[index]
                    .notify(VirtqThreadNotification::Enable(vring.num != 0))
                    .await;

                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::SetVringNum => {
                let vring = self.read_payload::<VhostVringState>(&hdr).await?;
                let index = vring.index as usize;

                let queue_size = vring.num;
                if !queue_size.is_power_of_two() || (queue_size - 1) > u16::MAX as u32 {
                    return Err(format!("vhost-user queue size {} is invalid", queue_size).into());
                }

                self.ensure_virtq(index)?;

                self.virtqs[index]
                    .notify(VirtqThreadNotification::SetQueueSize(queue_size as usize))
                    .await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::SetVringAddr => {
                let addr = self.read_payload::<VhostVringAddress>(&hdr).await?;
                let index = addr.index as usize;

                self.ensure_virtq(index)?;

                self.virtqs[index]
                    .notify(VirtqThreadNotification::UpdateRings(VirtqRingAddresses {
                        descriptor_ring: addr.descriptor,
                        used_ring: addr.used,
                        available_ring: addr.available,
                    }))
                    .await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::SetVringBase => {
                let vring = self.read_payload::<VhostVringState>(&hdr).await?;
                let index = vring.index as usize;

                self.ensure_virtq(index)?;

                self.virtqs[index]
                    .notify(VirtqThreadNotification::SetBase(vring.num))
                    .await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::GetVringBase => {
                let vring = self.read_payload::<VhostVringState>(&hdr).await?;
                let index = vring.index as usize;

                self.ensure_virtq(index)?;
                self.verify_no_fd(&hdr)?;

                self.virtqs[index]
                    .notify(VirtqThreadNotification::Stop)
                    .await;

                let (channel_s, mut channel_r) = mpsc::channel::<u32>(1);
                self.virtqs[index]
                    .notify(VirtqThreadNotification::GetBase(channel_s))
                    .await;

                let vring = VhostVringState {
                    index: index as u32,
                    num: channel_r.recv().await.unwrap(),
                };
                self.write_reply(&hdr, vring).await?;
            }

            VhostUserRequest::SetVringKick => {
                let data = self.read_payload::<u64>(&hdr).await?;
                let index = (data & 0xff) as usize;

                self.ensure_virtq(index)?;

                let fd = if data & VringCallFlag::InvalidFd as u64 != 0 {
                    None
                } else {
                    Some(self.pop_fd(&hdr)?)
                };

                self.virtqs[index]
                    .notify(VirtqThreadNotification::SetKickFd(fd))
                    .await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::SetVringCall => {
                let data = self.read_payload::<u64>(&hdr).await?;
                let index = (data & 0xff) as usize;

                self.ensure_virtq(index)?;

                let fd = if data & VringCallFlag::InvalidFd as u64 != 0 {
                    None
                } else {
                    Some(self.pop_fd(&hdr)?)
                };

                self.virtqs[index]
                    .notify(VirtqThreadNotification::SetCallFd(fd))
                    .await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::SetVringErr => {
                let data = self.read_payload::<u64>(&hdr).await?;
                let index = (data & 0xff) as usize;

                self.ensure_virtq(index)?;

                let fd = if data & VringCallFlag::InvalidFd as u64 != 0 {
                    None
                } else {
                    Some(self.pop_fd(&hdr)?)
                };

                self.virtqs[index]
                    .notify(VirtqThreadNotification::SetErrFd(fd))
                    .await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::GetConfig => {
                let cfg_hdr = self
                    .read_payload_header::<VirtioDeviceConfigSpaceHeader>(&hdr)
                    .await?;

                let payload_length = hdr.size - VirtioDeviceConfigSpaceHeader::SIZE as u32;
                if cfg_hdr.size != payload_length {
                    let cfg_size = cfg_hdr.size;
                    return Err(format!(
                        "vhost-user GetConfig command's payload size does not match \
                         (expected {}, got {})",
                        cfg_size, payload_length
                    )
                    .into());
                }

                let mut config = vec![0u8; cfg_hdr.size as usize];
                self.con.as_mut().unwrap().read_exact(&mut config).await?;

                self.verify_no_fd(&hdr)?;

                let (offset, size) = (cfg_hdr.offset as usize, cfg_hdr.size as usize);

                let mut reply_header = VhostUserMessage::new_reply(hdr.request, cfg_hdr);
                reply_header.header.size += size as u32;
                self.write_obj(reply_header).await?;

                self.device.get_config(offset, &mut config);
                self.con.as_mut().unwrap().write_all(&config).await?;
            }

            VhostUserRequest::GetMaxMemSlots => {
                self.read_no_payload(&hdr)?;
                self.verify_no_fd(&hdr)?;
                self.write_reply(&hdr, 16u64).await?;
            }

            VhostUserRequest::AddMemReg => {
                let region = self
                    .read_payload::<SingleVhostMemRegion>(&hdr)
                    .await?
                    .region;

                let fd = self.pop_fd(&hdr)?;
                self.mem_regions.push(MemRegion::new(region, fd)?);

                self.update_guest_memory().await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            VhostUserRequest::RemMemReg => {
                let region = self
                    .read_payload::<SingleVhostMemRegion>(&hdr)
                    .await?
                    .region;

                // Must ignore one potential FD
                let _: Result<OwnedFd, _> = self.pop_fd(&hdr);

                let mut found = false;
                for i in 0..self.mem_regions.len() {
                    if self.mem_regions[i] == region {
                        self.mem_regions.swap_remove(i);
                        found = true;
                        break;
                    }
                }

                if !found {
                    return Err("Frontend tried to remove unknown memory region".into());
                }

                self.update_guest_memory().await;

                self.verify_no_fd(&hdr)?;
                self.write_ack(&hdr).await?;
            }

            _ => todo!("Unimplemented vhost-user request {:?}", req),
        }

        Ok(())
    }

    /// Connect to the given unix socket
    pub fn connect(&mut self, con: UnixStream) {
        self.connected.store(true, Ordering::Relaxed);
        self.con = Some(con);
    }

    /// Drop the current connection
    pub async fn disconnect(&mut self) {
        VhostUserVirtq::broadcast(self.virtqs.iter(), VirtqThreadNotification::Stop).await;
        VhostUserVirtq::broadcast(
            self.virtqs.iter(),
            VirtqThreadNotification::InvalidateClient,
        )
        .await;

        self.mem_regions.clear();

        self.con.take();
        self.connected.store(false, Ordering::Relaxed);
    }

    async fn export_request_distributor(
        device: Arc<V>,
        connected: Arc<AtomicBool>,
        mut request: mpsc::UnboundedReceiver<BackgroundOpReq>,
        mut broadcasts: Vec<mpsc::UnboundedSender<VirtqMonitorRequest>>,
        quit: mpsc::Sender<()>,
    ) {
        let mut pre_reopen_node: Option<Arc<NodeUser>> = None;

        while let Some(req) = request.recv().await {
            match req {
                BackgroundOpReq::Quiesce(ack) => {
                    let (vq_ack_s, mut vq_ack_r) = mpsc::channel(broadcasts.len());
                    for s in broadcasts.iter_mut() {
                        s.send(VirtqMonitorRequest::Quiesce(vq_ack_s.clone()))
                            .unwrap();
                    }
                    for _ in 0..broadcasts.len() {
                        let _: Option<()> = vq_ack_r.recv().await;
                    }
                    ack.send(()).unwrap();
                }

                BackgroundOpReq::Unquiesce => {
                    for s in broadcasts.iter_mut() {
                        s.send(VirtqMonitorRequest::Unquiesce).unwrap();
                    }
                }

                BackgroundOpReq::Stop(mode, ack) => {
                    match mode {
                        PollableNodeStopMode::Safe => {
                            if connected.load(Ordering::Relaxed) {
                                ack.send(Err("vhost-user-blk device is connected".into()))
                                    .unwrap();
                                continue;
                            }
                        }
                        PollableNodeStopMode::Hard => (),
                        PollableNodeStopMode::CopyComplete { switch_over: _ } => {
                            ack.send(Err(
                                "'copy-complete' stop mode not supported by vhost-user-blk exports"
                                    .into(),
                            ))
                            .unwrap();
                            continue;
                        }
                    }

                    let (vq_ack_s, mut vq_ack_r) = mpsc::channel::<()>(broadcasts.len());
                    for s in broadcasts.iter_mut() {
                        // We want to quit the queues, so we can ignore whether the queues receive
                        // this message or have already quit for some reason
                        let _: Result<(), _> = s.send(VirtqMonitorRequest::Stop(vq_ack_s.clone()));
                    }
                    for _ in 0..broadcasts.len() {
                        let _: Option<()> = vq_ack_r.recv().await;
                    }

                    let _: Result<(), _> = quit.try_send(());
                    ack.send(Ok(())).unwrap();
                    break;
                }

                BackgroundOpReq::ChangeChildDo {
                    exported: node,
                    read_only,
                    result: ack,
                } => {
                    let (vq_ack_s, mut vq_ack_r) = mpsc::channel(broadcasts.len());
                    for s in broadcasts.iter_mut() {
                        s.send(VirtqMonitorRequest::ChangeChildDo {
                            node: Arc::clone(&node),
                            read_only,
                            result: vq_ack_s.clone(),
                            connected: connected.load(Ordering::Relaxed),
                        })
                        .unwrap();
                    }
                    let mut result = Ok(());
                    for _ in 0..broadcasts.len() {
                        if let Some(Err(err)) = vq_ack_r.recv().await {
                            result = Err(err);
                        }
                    }

                    if result.is_ok() {
                        let old_node = device.move_node(node);
                        pre_reopen_node = Some(old_node);
                    } else {
                        let (vq_ack_s, mut vq_ack_r) = mpsc::channel(broadcasts.len());
                        for s in broadcasts.iter_mut() {
                            s.send(VirtqMonitorRequest::ChangeChildRollBack(vq_ack_s.clone()))
                                .unwrap();
                        }
                        for _ in 0..broadcasts.len() {
                            let _: Option<()> = vq_ack_r.recv().await;
                        }
                    }

                    ack.send(result).unwrap();
                }

                BackgroundOpReq::ChangeChildClean(ack) => {
                    let (vq_ack_s, mut vq_ack_r) = mpsc::channel(broadcasts.len());
                    for s in broadcasts.iter_mut() {
                        s.send(VirtqMonitorRequest::ChangeChildClean(vq_ack_s.clone()))
                            .unwrap();
                    }
                    for _ in 0..broadcasts.len() {
                        let _: Option<()> = vq_ack_r.recv().await;
                    }
                    pre_reopen_node.take();
                    ack.send(()).unwrap();
                }

                BackgroundOpReq::ChangeChildRollBack(ack) => {
                    device.move_node(pre_reopen_node.take().unwrap());
                    let (vq_ack_s, mut vq_ack_r) = mpsc::channel(broadcasts.len());
                    for s in broadcasts.iter_mut() {
                        s.send(VirtqMonitorRequest::ChangeChildRollBack(vq_ack_s.clone()))
                            .unwrap();
                    }
                    for _ in 0..broadcasts.len() {
                        let _: Option<()> = vq_ack_r.recv().await;
                    }
                    ack.send(()).unwrap();
                }
            }
        }
    }
}
