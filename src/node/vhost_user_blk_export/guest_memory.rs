/**
 * Manages guest memory areas (received over the vhost-user socket), and provides methods to turn
 * memory areas specified by address and length into slices.
 */
use crate::error::BlockResult;
use crate::helpers::{FlatSize, OwnedFd};
use crate::node::vhost_user_blk_export::protocol::VhostMemRegion;
use std::sync::Arc;

/// Represents one VM memory region
#[derive(Debug)]
pub struct MemRegion {
    /// Base guest address
    guest_address: u64,
    /// Region size
    size: u64,
    /// Base user-space address (i.e. as mapped into qemu)
    user_address: u64,

    /// Pointer to a mapping of this region in this process
    mapping_base: *mut u8,

    /// Resources to be dropped when the last copy is dropped
    resources: Arc<MemRegionResources>,
}

/// Resources that belong to a memory region that must be deleted when the last reference to the
/// region is dropped
#[derive(Debug)]
pub struct MemRegionResources {
    /// The mapping (to be unmapped)
    mapping: (*mut libc::c_void, usize),

    /// Shared memory FD (to be closed)
    _fd: OwnedFd,
}

// Blocked because of the raw pointer, but is actually safe
unsafe impl Send for MemRegion {}
unsafe impl Sync for MemRegion {}

/// Representation of all VM memory
#[derive(Debug)]
pub struct GuestMemory {
    /// All VM memory regions
    regions: Vec<MemRegion>,
}

impl GuestMemory {
    /// Construct a `GuestMemory` object
    pub fn from(mut regions: Vec<MemRegion>) -> Self {
        // Searched from front to back, so sort by size, giving the largest areas the
        // greatest chance to be found earlier
        regions.sort_by_key(|x| x.size);

        GuestMemory { regions }
    }

    /// Look up a user address range and return the corresponding slice
    pub fn map_user<T: FlatSize>(&self, user_addr: u64, count: usize) -> BlockResult<&mut [T]> {
        let size = count * T::SIZE;
        for region in &self.regions {
            if let Some(offset) = region.offset_of_user(user_addr, size as u64) {
                let slice = unsafe {
                    std::slice::from_raw_parts_mut(region.mapping_base.add(offset) as *mut T, count)
                };
                return Ok(slice);
            }
        }
        Err(format!("User memory area 0x{:x}+{} not found", user_addr, count).into())
    }

    /// Look up a guest address range and return both the corresponding slice and the region in
    /// which it was found
    fn do_map_guest<T: FlatSize>(
        &self,
        guest_addr: u64,
        count: usize,
    ) -> BlockResult<(&mut [T], &MemRegion)> {
        for region in &self.regions {
            if let Some(slice) = region.map_guest(guest_addr, count) {
                return Ok((slice, region));
            }
        }
        Err(format!("Guest memory area 0x{:x}+{} not found", guest_addr, count).into())
    }

    /// Look up a guest address range, return the corresponding slice, and store the region in
    /// which it was found in `current_region`.  On subsequent lookups, try `current_region` first.
    pub fn map_guest_cached<'a, T: FlatSize>(
        &'a self,
        current_region: &mut Option<&'a MemRegion>,
        guest_addr: u64,
        count: usize,
    ) -> BlockResult<&'a mut [T]> {
        if let Some(slice) = current_region.and_then(|r| r.map_guest(guest_addr, count)) {
            Ok(slice)
        } else {
            let (slice, region) = self.do_map_guest::<T>(guest_addr, count)?;
            current_region.replace(region);
            Ok(slice)
        }
    }
}

impl MemRegion {
    /// Map the given memory region
    pub fn new(reg: VhostMemRegion, fd: OwnedFd) -> BlockResult<Self> {
        let size: libc::size_t = reg.size.try_into()?;
        let offset: libc::off_t = reg.mmap_offset.try_into()?;

        let mapping_base = unsafe {
            libc::mmap(
                std::ptr::null_mut(),
                size,
                libc::PROT_READ | libc::PROT_WRITE,
                libc::MAP_SHARED,
                *fd,
                offset,
            )
        };

        if mapping_base == libc::MAP_FAILED {
            return Err(std::io::Error::last_os_error().into());
        }

        Ok(MemRegion {
            guest_address: reg.guest_address,
            size: reg.size,
            user_address: reg.user_address,

            mapping_base: mapping_base as *mut u8,

            resources: Arc::new(MemRegionResources {
                mapping: (mapping_base, reg.size as usize),
                _fd: fd,
            }),
        })
    }

    /// Return the offset of the given user address range in this region, if fully contained
    fn offset_of_user(&self, user_address: u64, size: u64) -> Option<usize> {
        if self.user_address <= user_address && self.user_address + self.size >= user_address + size
        {
            Some((user_address - self.user_address).try_into().unwrap())
        } else {
            None
        }
    }

    /// Return the offset of the given guest address range in this region, if fully contained
    fn offset_of_guest(&self, guest_address: u64, size: u64) -> Option<usize> {
        if self.guest_address <= guest_address
            && self.guest_address + self.size >= guest_address + size
        {
            Some((guest_address - self.guest_address).try_into().unwrap())
        } else {
            None
        }
    }

    /// Return the slice corresponding to the given guest address range if in this region
    fn map_guest<T: FlatSize>(&self, guest_addr: u64, count: usize) -> Option<&mut [T]> {
        let size = count * T::SIZE;
        let offset = self.offset_of_guest(guest_addr, size as u64)?;
        let slice = unsafe {
            std::slice::from_raw_parts_mut(self.mapping_base.add(offset) as *mut T, count)
        };
        Some(slice)
    }
}

impl Clone for MemRegion {
    fn clone(&self) -> Self {
        MemRegion {
            guest_address: self.guest_address,
            size: self.size,
            user_address: self.user_address,
            mapping_base: self.mapping_base,
            resources: Arc::clone(&self.resources),
        }
    }
}

impl PartialEq<VhostMemRegion> for MemRegion {
    fn eq(&self, vhost: &VhostMemRegion) -> bool {
        self.guest_address == vhost.guest_address
            && self.size == vhost.size
            && self.user_address == vhost.user_address
    }
}

impl Drop for MemRegionResources {
    /// Unmap the mapping (the owned FD will be closed automatically)
    fn drop(&mut self) {
        unsafe { libc::munmap(self.mapping.0, self.mapping.1) };
    }
}
