/**
 * Traits used in the vhost-user-blk export code
 */
use crate::error::BlockResult;
use crate::helpers::BlockFutureResult;
use crate::node::vhost_user_blk_export::backend::VirtqMonitorRequest;
use crate::node::vhost_user_blk_export::virtq::ReqBuffers;
use crate::node::NodeUser;
use std::sync::Arc;

/// A virtio device as used by the vhost-user backend implementation
pub trait VirtioDevice: Send + Sync + 'static {
    /// Type containing the device's thread data parameters (which are sent to the virt queue
    /// thread, which then uses it to create the per-thread data (`VirtioDeviceThreadData`)
    /// directly in that thread, so that `VirtioDeviceThreadData` does not need to implement `Send`
    /// or `Sync`).
    type ThreadParam: VirtioDeviceThreadParam;

    /// Get features supported by this device
    fn get_features(&self) -> u64;
    /// Set features negotiated with the driver
    fn set_features(&self, features: u64);
    /// Get the maximum number of supported queues
    fn get_queue_num(&self) -> u64;
    /// Get part of the configuration space, and return the number of bytes actually read
    fn get_config(&self, offset: usize, buf: &mut [u8]) -> usize;
    /// Get the parameters needed to create the per-thread data in the virt queue thread
    fn get_thread_param(&self) -> Self::ThreadParam;

    /// Signal a move to a different node (all queues have already agreed through
    /// `VirtioDeviceThreadData::handle_monitor_request()`, so this must succeed).  Must return the
    /// old node.
    fn move_node(&self, node: Arc<NodeUser>) -> Arc<NodeUser>;
}

/// Parameters to create thread-specific data (`VirtioDeviceThreadData`) in the target thread.  In
/// contrast to the actual data, these parameters must implement `Send` and `Sync` so they can be
/// created in the central thread, and then be sent to the virt queue thread, where the
/// thread-specific data (which does not need to implement `Send` or `Sync`) is constructed from
/// them.
pub trait VirtioDeviceThreadParam: Send + Sync + 'static {
    /// Type of the thread-specific data created from these parameters
    type ThreadData: VirtioDeviceThreadData;

    /// Turn these parameters into thread-specific data
    fn into_data(self) -> BlockResult<Self::ThreadData>;
}

/// Thread-specific data for a virtio device.  Requests are created in a virt queue thread based on
/// this data.  Types implementing this trait do not need to implement `Send` or `Sync`, they will
/// be pinned to the respective virt queue thread.
pub trait VirtioDeviceThreadData: Unpin {
    /// Create a new request off of the given descriptor iterator
    fn request<'a, I: DescriptorIterator<'a>>(
        &'a self,
        iter: &mut I,
    ) -> BlockResult<BlockFutureResult<'a, ()>>;

    /// Handle a monitor request that cannot be handled by the generic vhost-user code
    fn handle_monitor_request(&self, req: VirtqMonitorRequest);
}

/// Iterators over available virtio descriptors in a descriptor chain must have `&'a mut [u8]`
/// slices as the items they return, and they must keep track of the buffers taken from the root
/// descriptor ring (i.e. disregarding indirect descriptors), which are collected in a `ReqBuffers`
/// object.
/// Descriptor iterator's `Iterator::next()` must not return zero-length buffers.
pub trait DescriptorIterator<'a>: Iterator<Item = &'a mut [u8]> {
    /// Whether there are any descriptors left in the chain.  Note that this may include
    /// zero-length descriptors, which will not be returned by `next()`, so if this returns
    /// `false`, that does not mean that `next()` will return another descriptor.
    fn is_empty(&self) -> bool;
    /// After iterating, get a `ReqBuffers` object that describes which descriptors were taken from
    /// the root descriptor ring, i.e. which must be returned to the device as used descriptors.
    fn get_buffers(self) -> ReqBuffers;
}
