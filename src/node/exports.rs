use crate::error::{BlockError, BlockResult};
use crate::helpers::{BlockFutureResult, BoxedFuture, InfallibleFuture, ThreadBound};
use crate::node::{
    BackgroundOpResult, ChangeGraphStep, IoQueueDriverData, Node, NodeBasicInfo, NodeCacheConfig,
    NodeConfig, NodeConfigOrReference, NodeDriverConfig, NodeDriverData, NodeLimits, NodePermPair,
    NodeUser, NodeUserBuilder, PollableNodeStopMode,
};
use crate::server::ServerNode;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::sync::atomic::{AtomicU64, AtomicUsize};
use std::sync::{Arc, Mutex};
use tokio::sync::{mpsc, oneshot};

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config<T: Clone + Send + Sync + ImmutableExportConfig> {
    exported: NodeConfigOrReference,

    #[serde(flatten)]
    immutable: T,
}

pub struct Data<C: PartialEq + Send + Sync + ImmutableExportConfig> {
    channel: mpsc::UnboundedSender<BackgroundOpReq>,
    background_op_data: ThreadBound<C::BackgroundOpData>,

    mut_data: Arc<Mutex<MutData>>,
    config: C,
}

struct MutData {
    exported: Arc<NodeUser>,
    pre_reopen_exported: Option<Arc<NodeUser>>,
}

#[async_trait(?Send)]
pub trait ImmutableExportConfig {
    type BackgroundOpData: BackgroundOpData;

    fn construct_node_user(
        &self,
        init: NodeUserBuilder,
        read_only: bool,
    ) -> BlockResult<NodeUserBuilder>;
    async fn create_background_op_data(
        &self,
        node_name: &str,
        exported: &Arc<NodeUser>,
        channel_r: mpsc::UnboundedReceiver<BackgroundOpReq>,
        read_only: bool,
    ) -> BlockResult<Self::BackgroundOpData>;
}

#[async_trait(?Send)]
pub trait BackgroundOpData {
    fn background_operation(self) -> (BlockFutureResult<'static, ()>, Option<ServerNode>);
    /// Drop this object.  Provided so that any `ThreadBound<_>` object can safely be dropped in
    /// their home thread.
    async fn async_drop(self);
}

#[derive(Debug)]
pub enum BackgroundOpReq {
    Quiesce(oneshot::Sender<()>),
    Unquiesce,
    Stop(PollableNodeStopMode, oneshot::Sender<BlockResult<()>>),

    ChangeChildDo {
        exported: Arc<NodeUser>,
        read_only: bool,
        result: oneshot::Sender<BlockResult<()>>,
    },

    ChangeChildClean(oneshot::Sender<()>),
    ChangeChildRollBack(oneshot::Sender<()>),
}

impl<T: Clone + Send + Sync + ImmutableExportConfig> Config<T> {
    pub fn split_tree(&mut self, vec: &mut Vec<NodeConfig>) -> BlockResult<()> {
        self.exported.split_tree(vec)
    }
}

impl<C: Clone + PartialEq + Send + Sync + ImmutableExportConfig> Data<C> {
    pub async fn new(
        node_name: &str,
        opts: Config<C>,
        read_only: bool,
        _cache: &NodeCacheConfig,
    ) -> BlockResult<Box<Self>> {
        let exported = opts.exported.lookup()?;

        let opts = opts.immutable;
        let node_user = NodeUser::builder(node_name, "exported");
        let node_user = opts.construct_node_user(node_user, read_only)?;
        let exported = exported.add_user(node_user).await?;

        let (channel_w, channel_r) = mpsc::unbounded_channel();
        let background_op_data = opts
            .create_background_op_data(node_name, &exported, channel_r, read_only)
            .await?;

        // Safe because all operations that could drop this (`background_operation()` and
        // `drain_caches()`) are run in the main thread
        let background_op_data = unsafe { ThreadBound::new_unsafe(background_op_data) };

        let mut_data = MutData {
            exported,
            pre_reopen_exported: None,
        };

        Ok(Box::new(Data {
            channel: channel_w,
            background_op_data,

            mut_data: Arc::new(Mutex::new(mut_data)),
            config: opts,
        }))
    }
}

#[async_trait]
impl<C: Clone + PartialEq + Send + Sync + ImmutableExportConfig> NodeDriverData for Data<C>
where
    Config<C>: TryFrom<NodeDriverConfig, Error = BlockError>,
    for<'a> &'a Config<C>: TryFrom<&'a NodeDriverConfig, Error = BlockError>,
{
    fn background_operation(
        &self,
    ) -> Option<(BoxedFuture<'static, BackgroundOpResult>, Option<ServerNode>)> {
        let (fut, server) = self
            .background_op_data
            .take()
            .unwrap()
            .background_operation();

        Some((
            Box::pin(async move {
                let result = fut.await;

                BackgroundOpResult {
                    result,
                    auto_fade: true,
                }
            }),
            server,
        ))
    }

    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        // Not accessible by parents, so does not matter
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(0),
                request_alignment: AtomicUsize::new(1),
                memory_alignment: AtomicUsize::new(1),
            },
        })
    }

    fn get_successor(&self) -> Option<Arc<Node>> {
        Some(Arc::clone(self.mut_data.lock().unwrap().exported.node()))
    }

    fn drain_caches(&mut self) -> InfallibleFuture {
        Box::pin(async move {
            if let Some(bg_op) = self.background_op_data.take() {
                bg_op.async_drop().await;
            }
        })
    }

    fn new_queue(&self) -> BlockResult<Box<dyn IoQueueDriverData>> {
        Err("Cannot use pure export node as child node".into())
    }

    fn get_children(&self) -> Vec<Arc<Node>> {
        let exported = Arc::clone(self.mut_data.lock().unwrap().exported.node());
        vec![exported]
    }

    fn get_children_after_reopen(&self, opts: &NodeConfig) -> BlockResult<Vec<Arc<Node>>> {
        let opts: &Config<C> = (&opts.driver).try_into()?;
        let exported = opts.exported.lookup()?;
        Ok(vec![exported])
    }

    fn quiesce(&self) -> InfallibleFuture {
        let (result_w, result) = oneshot::channel();
        // Ignore finished background operation; if it is done, we are stopped
        let _: Result<(), _> = self.channel.send(BackgroundOpReq::Quiesce(result_w));
        Box::pin(async move { result.await.unwrap_or(()) })
    }

    fn unquiesce(&self) {
        let _: Result<(), _> = self.channel.send(BackgroundOpReq::Unquiesce);
    }

    fn stop<'a>(&self, mode: PollableNodeStopMode) -> BlockFutureResult<'a, ()> {
        let (result_w, result) = oneshot::channel();
        // Ignore finished background operation; if it is done, we are stopped
        let _: Result<(), _> = self.channel.send(BackgroundOpReq::Stop(mode, result_w));
        Box::pin(async move { result.await.unwrap_or(Ok(())) })
    }

    fn reopen_change_graph<'a>(
        &'a self,
        opts: &'a NodeConfig,
        perms: NodePermPair,
        read_only: bool,
        step: ChangeGraphStep,
    ) -> BlockFutureResult<'a, ()> {
        Box::pin(async move {
            if perms.has_any() {
                return Err("Cannot create parent nodes on bench nodes".into());
            }

            let my_node_name = &opts.node_name;
            let opts: &Config<C> = (&opts.driver).try_into()?;
            let new_exported = opts.exported.lookup()?;
            let mut data = self.mut_data.lock().unwrap();

            match step {
                ChangeGraphStep::Release => {
                    data.exported
                        .set_perms_in_reopen_change_graph(NodePermPair::default())?;
                }

                ChangeGraphStep::Acquire => {
                    let node_user = NodeUser::builder(my_node_name, "exported");
                    let node_user = self.config.construct_node_user(node_user, read_only)?;
                    let new_exported = new_exported.add_user_in_reopen_change_graph(node_user)?;
                    let old_exported = std::mem::replace(&mut data.exported, new_exported);
                    data.pre_reopen_exported.replace(old_exported);
                }
            }

            Ok(())
        })
    }

    fn reopen_do(
        &self,
        opts: NodeConfig,
        _perms: NodePermPair,
        read_only: bool,
    ) -> BlockFutureResult<()> {
        Box::pin(async move {
            let opts: Config<C> = opts.driver.try_into()?;
            if opts.immutable != self.config {
                return Err("Attempting to change an option that cannot be changed".into());
            }

            let exported = Arc::clone(&self.mut_data.lock().unwrap().exported);
            let (result_w, result) = oneshot::channel();
            // Ignore finished background operation
            let _: Result<(), _> = self.channel.send(BackgroundOpReq::ChangeChildDo {
                exported,
                read_only,
                result: result_w,
            });
            result.await.unwrap_or(Ok(()))
        })
    }

    fn reopen_clean_async(&self) -> InfallibleFuture {
        {
            let mut data = self.mut_data.lock().unwrap();
            data.pre_reopen_exported.take();
        }

        let (ack_w, ack) = oneshot::channel();
        // Ignore finished background operation
        let _: Result<(), _> = self.channel.send(BackgroundOpReq::ChangeChildClean(ack_w));
        Box::pin(async move { ack.await.unwrap_or(()) })
    }

    fn reopen_roll_back_async(&self) -> InfallibleFuture {
        {
            let mut data = self.mut_data.lock().unwrap();
            if let Some(old_exported) = data.pre_reopen_exported.take() {
                data.exported = old_exported;
            }
        }

        let (ack_w, ack) = oneshot::channel();
        // Ignore finished background operation
        let _: Result<(), _> = self
            .channel
            .send(BackgroundOpReq::ChangeChildRollBack(ack_w));
        Box::pin(async move { ack.await.unwrap_or(()) })
    }
}
