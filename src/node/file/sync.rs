use crate::error::BlockResult;
use crate::helpers::{BlockFutureResult, IoBufferMut, IoBufferRef};
use crate::node::{
    IoQueueDriverData, Node, NodeBasicInfo, NodeCacheConfig, NodeConfig, NodeDriverData,
    NodeLimits, NodePermPair,
};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::fs::{self, OpenOptions};
use std::os::unix::fs::{FileExt, OpenOptionsExt};
use std::sync::atomic::{AtomicU64, AtomicUsize};
use std::sync::{Arc, Mutex};

struct MutData {
    /// Cloned to make files to put into `Queue`s
    orig_file: fs::File,
    direct_io: bool,
}

pub struct Data {
    mut_data: Arc<Mutex<MutData>>,
    pre_reopen: Mutex<Option<MutData>>,
}

pub struct Queue {
    file: fs::File,
    mut_data: Arc<Mutex<MutData>>,
    pre_reopen_file: Option<fs::File>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub filename: String,
}

impl Config {
    #[allow(clippy::ptr_arg)]
    pub fn split_tree(&mut self, _vec: &mut Vec<NodeConfig>) -> BlockResult<()> {
        Ok(())
    }
}

impl Data {
    pub async fn new(
        _name: &str,
        opts: Config,
        read_only: bool,
        cache: &NodeCacheConfig,
    ) -> BlockResult<Box<Self>> {
        let mut_data = MutData::new(opts, read_only, cache)?;
        Ok(Box::new(Data {
            mut_data: Arc::new(Mutex::new(mut_data)),
            pre_reopen: Default::default(),
        }))
    }
}

impl MutData {
    fn new(opts: Config, read_only: bool, cache: &NodeCacheConfig) -> BlockResult<MutData> {
        let direct_io = cache.direct;

        let mut file_opts = OpenOptions::new();
        file_opts.read(true).write(!read_only);
        if direct_io {
            file_opts.custom_flags(libc::O_DIRECT);
        }
        let file = file_opts.open(opts.filename)?;

        Ok(MutData {
            orig_file: file,
            direct_io,
        })
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        let data = self.mut_data.lock().unwrap();
        // TODO: Probe
        let (req_align, mem_align) = if data.direct_io { (4096, 4096) } else { (1, 1) };
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(data.orig_file.metadata()?.len()),
                request_alignment: AtomicUsize::new(req_align),
                memory_alignment: AtomicUsize::new(mem_align),
            },
        })
    }

    fn new_queue(&self) -> BlockResult<Box<dyn IoQueueDriverData>> {
        let data = self.mut_data.lock().unwrap();
        Ok(Box::new(Queue {
            file: data.orig_file.try_clone()?,
            mut_data: Arc::clone(&self.mut_data),
            pre_reopen_file: None,
        }))
    }

    fn get_children(&self) -> Vec<Arc<Node>> {
        Vec::new() // no children
    }

    fn get_children_after_reopen(&self, _opts: &NodeConfig) -> BlockResult<Vec<Arc<Node>>> {
        Ok(Vec::new()) // no children
    }

    fn reopen_do(
        &self,
        opts: NodeConfig,
        _perms: NodePermPair,
        read_only: bool,
    ) -> BlockFutureResult<()> {
        Box::pin(async move {
            use crate::node::file;

            let cache = &opts.cache;
            let opts: file::Config = opts.driver.try_into()?;
            let opts: file::AioConfig = opts.into();
            let opts: Config = opts.try_into()?;

            let new_data = MutData::new(opts, read_only, cache)?;
            let old_data = std::mem::replace(&mut *self.mut_data.lock().unwrap(), new_data);
            self.pre_reopen.lock().unwrap().replace(old_data);

            Ok(())
        })
    }

    fn reopen_clean(&self) {
        self.pre_reopen.lock().unwrap().take();
    }

    fn reopen_roll_back(&self) {
        if let Some(old_data) = self.pre_reopen.lock().unwrap().take() {
            *self.mut_data.lock().unwrap() = old_data;
        }
    }
}

impl IoQueueDriverData for Queue {
    fn read<'a>(&'a self, buf: IoBufferMut<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        Box::pin(async move {
            self.file.read_exact_at(buf.into_slice(), offset)?;
            Ok(())
        })
    }

    fn write<'a>(&'a self, buf: IoBufferRef<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        Box::pin(async move {
            self.file.write_all_at(buf.into_slice(), offset)?;
            Ok(())
        })
    }

    fn flush(&self) -> BlockFutureResult<'_, ()> {
        Box::pin(async move {
            self.file.sync_data()?;
            Ok(())
        })
    }

    fn reopen_do(&mut self) -> BlockResult<()> {
        let data = self.mut_data.lock().unwrap();
        let new_file = data.orig_file.try_clone()?;
        let old_file = std::mem::replace(&mut self.file, new_file);
        self.pre_reopen_file.replace(old_file);
        Ok(())
    }

    fn reopen_clean(&mut self) {
        self.pre_reopen_file.take();
    }

    fn reopen_roll_back(&mut self) {
        let old_file = self.pre_reopen_file.take().unwrap();
        self.file = old_file;
    }
}
