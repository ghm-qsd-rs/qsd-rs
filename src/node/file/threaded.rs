use crate::error::{BlockError, BlockResult};
use crate::helpers::{BlockFutureResult, IoBufferMut, IoBufferRef};
use crate::node::{
    IoQueueDriverData, Node, NodeBasicInfo, NodeCacheConfig, NodeConfig, NodeDriverData,
    NodeLimits, NodePermPair,
};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::collections::LinkedList;
use std::fs::{self, OpenOptions};
use std::future::Future;
use std::io;
use std::marker::PhantomData;
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::io::{AsRawFd, RawFd};
use std::pin::Pin;
use std::sync::atomic::{AtomicU64, AtomicUsize, Ordering};
use std::sync::mpsc::{self, Sender};
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll, Waker};
use std::thread::{self, JoinHandle};

extern "C" {
    fn pread(
        fildes: libc::c_int,
        buf: *mut libc::c_void,
        nbyte: libc::size_t,
        offset: libc::off_t,
    ) -> libc::ssize_t;

    fn pwrite(
        fildes: libc::c_int,
        buf: *const libc::c_void,
        nbyte: libc::size_t,
        offset: libc::off_t,
    ) -> libc::ssize_t;

    fn fsync(fildes: libc::c_int) -> libc::c_int;
}

struct MutData {
    orig_file: fs::File,
    direct_io: bool,
}

pub struct Data {
    mut_data: Arc<Mutex<MutData>>,
    pre_reopen: Mutex<Option<MutData>>,
}

pub struct Queue {
    file: fs::File,

    pread_pool: IoThreadPool<Pread>,
    pwrite_pool: IoThreadPool<Pwrite>,

    mut_data: Arc<Mutex<MutData>>,
    pre_reopen_file: Option<fs::File>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub filename: String,
    pub read_only: Option<bool>,
    pub auto_read_only: Option<bool>,
}

impl Config {
    #[allow(clippy::ptr_arg)]
    pub fn split_tree(&mut self, _vec: &mut Vec<NodeConfig>) -> BlockResult<()> {
        Ok(())
    }
}

impl Data {
    pub async fn new(
        _name: &str,
        opts: Config,
        read_only: bool,
        cache: &NodeCacheConfig,
    ) -> BlockResult<Box<Self>> {
        let mut_data = MutData::new(opts, read_only, cache)?;
        Ok(Box::new(Data {
            mut_data: Arc::new(Mutex::new(mut_data)),
            pre_reopen: Default::default(),
        }))
    }
}

impl MutData {
    fn new(opts: Config, read_only: bool, cache: &NodeCacheConfig) -> BlockResult<MutData> {
        let direct_io = cache.direct;

        let mut file_opts = OpenOptions::new();
        file_opts.read(true).write(!read_only);
        if direct_io {
            file_opts.custom_flags(libc::O_DIRECT);
        }
        let file = file_opts.open(opts.filename)?;

        Ok(MutData {
            orig_file: file,
            direct_io,
        })
    }
}

#[derive(Default)]
struct DoneSignal {
    done: bool,
    result: Option<BlockResult<()>>,
    waker: Option<Waker>,
}

struct Pread {
    fd: RawFd,
    offset: u64,
}

struct Pwrite {
    fd: RawFd,
    offset: u64,
}

enum BlockingIoBuffer<'a> {
    Mutable(IoBufferMut<'a>),
    Immutable(IoBufferRef<'a>),
}

trait BlockingIo {
    fn process(&mut self, buf: Option<BlockingIoBuffer<'_>>) -> BlockResult<()>;
    fn new_quit() -> Self;
    fn is_quit_signal(&self) -> bool;
}

#[derive(Default)]
struct IoWrapper<'a> {
    ds: Arc<Mutex<DoneSignal>>,

    // Marker for the buffer used internally
    _lifetime: PhantomData<&'a mut [u8]>,
}

impl Pread {
    fn new(fd: RawFd, offset: u64) -> Self {
        Pread { fd, offset }
    }
}

impl BlockingIo for Pread {
    fn process(&mut self, buf: Option<BlockingIoBuffer<'_>>) -> BlockResult<()> {
        let buf = match buf {
            Some(BlockingIoBuffer::Mutable(buf)) => buf.into_slice(),
            _ => {
                return Err(BlockError::from_desc(
                    "Invalid or no buffer given to pread".into(),
                ))
            }
        };

        let mut completed = 0usize;
        let len = buf.len();

        while completed < len {
            let slice = &mut buf[completed..len];
            let off = self.offset + completed as u64;

            let bytes_read = unsafe {
                pread(
                    self.fd,
                    slice.as_mut_ptr() as *mut libc::c_void,
                    slice.len(),
                    off.try_into()?,
                )
            };
            if bytes_read < 0 {
                return Err(io::Error::last_os_error().into());
            }

            completed += bytes_read as usize;
        }

        Ok(())
    }

    fn new_quit() -> Self {
        Pread { fd: -1, offset: 0 }
    }

    fn is_quit_signal(&self) -> bool {
        self.fd < 0
    }
}

impl Pwrite {
    fn new(fd: RawFd, offset: u64) -> Self {
        Pwrite { fd, offset }
    }
}

impl BlockingIo for Pwrite {
    fn process(&mut self, buf: Option<BlockingIoBuffer<'_>>) -> BlockResult<()> {
        let buf = match buf {
            Some(BlockingIoBuffer::Immutable(buf)) => buf.into_slice(),
            _ => {
                return Err(BlockError::from_desc(
                    "Invalid or no buffer given to pwrite".into(),
                ))
            }
        };

        let mut completed = 0usize;
        let len = buf.len();

        while completed < len {
            let slice = &buf[completed..len];
            let off = self.offset + completed as u64;

            let bytes_written = unsafe {
                pwrite(
                    self.fd,
                    slice.as_ptr() as *const libc::c_void,
                    slice.len(),
                    off.try_into()?,
                )
            };
            if bytes_written < 0 {
                return Err(io::Error::last_os_error().into());
            }

            completed += bytes_written as usize;
        }

        Ok(())
    }

    fn new_quit() -> Self {
        Pwrite { fd: -1, offset: 0 }
    }

    fn is_quit_signal(&self) -> bool {
        self.fd < 0
    }
}

struct IoWorkUnit<T: 'static + BlockingIo + Send + Sync> {
    obj: T,
    buf: Option<BlockingIoBuffer<'static>>,
    ds: Arc<Mutex<DoneSignal>>,
}

struct SettleWaiterWaker {
    offset: usize,
    target: usize,
    waker: Waker,
}

struct IoThreadPool<T: 'static + BlockingIo + Send + Sync> {
    handles: Vec<JoinHandle<()>>,
    input: Mutex<Sender<IoWorkUnit<T>>>,

    submitted: AtomicUsize,
    settled: Arc<AtomicUsize>,

    settle_waiters_count: Arc<AtomicUsize>,
    settle_waiters: Arc<Mutex<LinkedList<SettleWaiterWaker>>>,
}

struct SettleWaiter {
    /// Original value of `settled` when this object was created; subtract this from `settled`
    /// after fetching it and before comparing it to `target`, to get around overflow problems
    offset: usize,

    target: usize,
    settled: Arc<AtomicUsize>,

    settle_waiters_count: Arc<AtomicUsize>,
    /// Present only if `self` is not already in the list
    /// .0: offset; .1: target; .2: waker
    settle_waiters: Option<Arc<Mutex<LinkedList<SettleWaiterWaker>>>>,
}

impl<T: 'static + BlockingIo + Send + Sync> IoThreadPool<T> {
    fn new(count: usize) -> Self {
        let (s, r) = mpsc::channel::<IoWorkUnit<T>>();
        let r = Arc::new(Mutex::new(r));
        let settled = Arc::new(AtomicUsize::new(0));
        let settle_waiters_count = Arc::new(AtomicUsize::new(0));
        let settle_waiters = Arc::new(Mutex::new(LinkedList::<SettleWaiterWaker>::new()));

        let handles = (0..count)
            .map(|_| {
                let r = Arc::clone(&r);
                let settled = Arc::clone(&settled);
                let settle_waiters_count = Arc::clone(&settle_waiters_count);
                let settle_waiters = Arc::clone(&settle_waiters);

                thread::spawn(move || loop {
                    let mut unit = r.lock().unwrap().recv().unwrap();
                    if unit.obj.is_quit_signal() {
                        let mut ds = unit.ds.lock().unwrap();
                        ds.done = true;
                        break;
                    }

                    let result = unit.obj.process(unit.buf);
                    settled.fetch_add(1, Ordering::Relaxed);
                    let waker = {
                        let mut ds = unit.ds.lock().unwrap();
                        ds.result = Some(result);
                        ds.done = true;
                        ds.waker.clone()
                    };
                    if let Some(waker) = waker {
                        waker.wake_by_ref();
                    }

                    if settle_waiters_count.load(Ordering::Relaxed) > 0 {
                        let mut settle_waiters = settle_waiters.lock().unwrap();
                        let settled = settled.load(Ordering::Relaxed);

                        while let Some(f) = settle_waiters.front() {
                            if settled.wrapping_sub(f.offset) >= f.target {
                                settle_waiters_count.fetch_sub(1, Ordering::Relaxed);
                                let f = settle_waiters.pop_front().unwrap();
                                f.waker.wake();
                            } else {
                                break;
                            }
                        }
                    }
                })
            })
            .collect();

        IoThreadPool {
            handles,
            input: Mutex::new(s),

            submitted: AtomicUsize::new(0),
            settled,

            settle_waiters_count,
            settle_waiters,
        }
    }

    fn do_submit<'a>(&self, obj: T, buf: Option<BlockingIoBuffer<'a>>) -> IoWrapper<'a> {
        let buf = buf.map(|buf| unsafe {
            std::mem::transmute::<BlockingIoBuffer<'a>, BlockingIoBuffer<'static>>(buf)
        });
        let unit = IoWorkUnit {
            obj,
            buf,
            ds: Default::default(),
        };

        let wrapper = IoWrapper {
            ds: Arc::clone(&unit.ds),
            _lifetime: PhantomData,
        };

        self.submitted.fetch_add(1, Ordering::Relaxed);

        let s = { self.input.lock().unwrap().clone() };
        s.send(unit).unwrap();

        wrapper
    }

    fn submit_buf<'a>(&self, obj: T, buf: IoBufferRef<'a>) -> IoWrapper<'a> {
        self.do_submit(obj, Some(BlockingIoBuffer::Immutable(buf)))
    }

    fn submit_mut_buf<'a>(&self, obj: T, buf: IoBufferMut<'a>) -> IoWrapper<'a> {
        self.do_submit(obj, Some(BlockingIoBuffer::Mutable(buf)))
    }

    /// Return an `impl Future` that waits until all requests submitted so far have been settled.
    fn settle_current(&self) -> SettleWaiter {
        let target = self.submitted.load(Ordering::Relaxed);
        let offset = self.settled.load(Ordering::Relaxed);

        SettleWaiter {
            offset,
            target: target.wrapping_sub(offset),
            settled: Arc::clone(&self.settled),
            settle_waiters_count: Arc::clone(&self.settle_waiters_count),
            settle_waiters: Some(Arc::clone(&self.settle_waiters)),
        }
    }
}
impl<'a> Future for IoWrapper<'a> {
    type Output = BlockResult<()>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let result = {
            let mut ds = self.ds.lock().unwrap();
            if ds.result.is_none() && ds.waker.is_none() {
                ds.waker = Some(cx.waker().clone());
            }
            ds.result.take()
        };

        if let Some(result) = result {
            Poll::Ready(result)
        } else {
            Poll::Pending
        }
    }
}

impl<'a> Drop for IoWrapper<'a> {
    fn drop(&mut self) {
        // We must wait for completion of this request before dropping it, because the worker
        // thread relies on the buffers living until completion.
        // TODO: This should not be done with a busy loop.
        while !self.ds.lock().unwrap().done {}
    }
}

impl Future for SettleWaiter {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let current = self
            .settled
            .load(Ordering::Relaxed)
            .wrapping_sub(self.offset);

        if current >= self.target {
            Poll::Ready(())
        } else if let Some(sw) = self.settle_waiters.take() {
            let sww = SettleWaiterWaker {
                offset: self.offset,
                target: self.target,
                waker: cx.waker().clone(),
            };

            sw.lock().unwrap().push_back(sww);
            self.settle_waiters_count.fetch_add(1, Ordering::Relaxed);

            // Poll again to prevent TOCTTOU window
            self.poll(cx)
        } else {
            Poll::Pending
        }
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        let data = self.mut_data.lock().unwrap();
        // TODO: Probe
        let (req_align, mem_align) = if data.direct_io { (4096, 4096) } else { (1, 1) };
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(data.orig_file.metadata()?.len()),
                request_alignment: AtomicUsize::new(req_align),
                memory_alignment: AtomicUsize::new(mem_align),
            },
        })
    }

    fn new_queue(&self) -> BlockResult<Box<dyn IoQueueDriverData>> {
        let data = self.mut_data.lock().unwrap();
        Ok(Box::new(Queue {
            file: data.orig_file.try_clone()?,
            pread_pool: IoThreadPool::new(16),
            pwrite_pool: IoThreadPool::new(16),
            mut_data: Arc::clone(&self.mut_data),
            pre_reopen_file: None,
        }))
    }

    fn get_children(&self) -> Vec<Arc<Node>> {
        Vec::new() // no children
    }

    fn get_children_after_reopen(&self, _opts: &NodeConfig) -> BlockResult<Vec<Arc<Node>>> {
        Ok(Vec::new()) // no children
    }

    fn reopen_do(
        &self,
        opts: NodeConfig,
        _perms: NodePermPair,
        read_only: bool,
    ) -> BlockFutureResult<()> {
        Box::pin(async move {
            use crate::node::file;

            let cache = &opts.cache;
            let opts: file::Config = opts.driver.try_into()?;
            let opts: file::AioConfig = opts.into();
            let opts: Config = opts.try_into()?;

            let new_data = MutData::new(opts, read_only, cache)?;
            let old_data = std::mem::replace(&mut *self.mut_data.lock().unwrap(), new_data);
            self.pre_reopen.lock().unwrap().replace(old_data);

            Ok(())
        })
    }

    fn reopen_clean(&self) {
        self.pre_reopen.lock().unwrap().take();
    }

    fn reopen_roll_back(&self) {
        if let Some(old_data) = self.pre_reopen.lock().unwrap().take() {
            *self.mut_data.lock().unwrap() = old_data;
        }
    }
}

impl IoQueueDriverData for Queue {
    fn read<'a>(&'a self, buf: IoBufferMut<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        let p = Pread::new(self.file.as_raw_fd(), offset);
        Box::pin(self.pread_pool.submit_mut_buf(p, buf))
    }

    fn write<'a>(&'a self, buf: IoBufferRef<'a>, offset: u64) -> BlockFutureResult<'a, ()> {
        let p = Pwrite::new(self.file.as_raw_fd(), offset);
        Box::pin(self.pwrite_pool.submit_buf(p, buf))
    }

    fn flush(&self) -> BlockFutureResult<'_, ()> {
        Box::pin(async move {
            self.pwrite_pool.settle_current().await;
            let ret = unsafe { fsync(self.file.as_raw_fd()) };
            if ret < 0 {
                Err(io::Error::last_os_error().into())
            } else {
                Ok(())
            }
        })
    }

    fn reopen_do(&mut self) -> BlockResult<()> {
        let data = self.mut_data.lock().unwrap();
        let new_file = data.orig_file.try_clone()?;
        let old_file = std::mem::replace(&mut self.file, new_file);
        self.pre_reopen_file.replace(old_file);
        Ok(())
    }

    fn reopen_clean(&mut self) {
        self.pre_reopen_file.take();
    }

    fn reopen_roll_back(&mut self) {
        let old_file = self.pre_reopen_file.take().unwrap();
        self.file = old_file;
    }
}

impl<T: BlockingIo + Send + Sync> Drop for IoThreadPool<T> {
    fn drop(&mut self) {
        // Keep the `IoWrapper` objects because they are checked for `.done == true` when dropped
        // (which we can only assume to be true after the `.join()`)
        let _io_wrappers = (0..self.handles.len())
            .map(|_| {
                let obj = <T as BlockingIo>::new_quit();
                self.do_submit(obj, None)
            })
            .collect::<Vec<IoWrapper>>();

        for handle in self.handles.drain(..) {
            let _: Result<(), _> = handle.join();
        }
    }
}
