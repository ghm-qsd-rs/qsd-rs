mod blkio;
mod sync;
mod threaded;

use crate::error::BlockResult;
use crate::node::{NodeCacheConfig, NodeConfig, NodeDriverData};
use crate::splittable_enum;
use serde::{Deserialize, Serialize};

pub struct Data {}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged, rename_all = "kebab-case")]
pub enum Config {
    SpecificAio(AioConfig),
    DefaultAio(blkio::Config),
}

splittable_enum! {
    #[derive(Clone, Debug, Deserialize, Serialize)]
    #[serde(tag = "aio", rename_all = "kebab-case")]
    pub enum AioConfig {
        Blkio(blkio::Config),
        Sync(sync::Config),
        Threads(threaded::Config),
    }
}

impl Config {
    #[allow(clippy::ptr_arg)]
    pub fn split_tree(&mut self, vec: &mut Vec<NodeConfig>) -> BlockResult<()> {
        match self {
            Config::SpecificAio(AioConfig::Blkio(o)) | Config::DefaultAio(o) => o.split_tree(vec),
            Config::SpecificAio(AioConfig::Sync(o)) => o.split_tree(vec),
            Config::SpecificAio(AioConfig::Threads(o)) => o.split_tree(vec),
        }
    }
}

impl Data {
    // This does not return `Self` because it just acts as a gateway to the different back ends
    #[allow(clippy::new_ret_no_self)]
    pub async fn new(
        name: &str,
        opts: Config,
        read_only: bool,
        cache: &NodeCacheConfig,
    ) -> BlockResult<Box<dyn NodeDriverData + Send + Sync>> {
        let data: Box<dyn NodeDriverData + Send + Sync> = match opts.into() {
            AioConfig::Blkio(o) => blkio::Data::new(name, o, read_only, cache).await?,
            AioConfig::Sync(o) => sync::Data::new(name, o, read_only, cache).await?,
            AioConfig::Threads(o) => threaded::Data::new(name, o, read_only, cache).await?,
        };

        Ok(data)
    }
}

impl From<Config> for AioConfig {
    fn from(cfg: Config) -> Self {
        match cfg {
            Config::SpecificAio(aio) => aio,
            Config::DefaultAio(blkio) => AioConfig::Blkio(blkio),
        }
    }
}
