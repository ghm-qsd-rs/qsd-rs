use crate::error::BlockResult;
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};
use std::alloc::{self, GlobalAlloc};
use std::future::Future;
use std::marker::PhantomData;
use std::os::unix::io::{AsRawFd, RawFd};
use std::pin::Pin;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};
use tokio::sync::oneshot;

mod cache;
pub mod nbd;
mod network;
mod threads;

pub use cache::*;
pub use network::*;
pub use threads::*;


pub type BoxedFuture<'a, T> = Pin<Box<dyn Future<Output = T> + 'a>>;

/// Describes `Future` objects that will return a `BlockResult<T>`.  This should be the return type
/// of all quasi-async I/O functions returning a `BlockResult<_>`, i.e. functions that are not
/// async, but can be used as such.  For example, the following two functions are semantically
/// similar:
///
/// ```rust
/// async fn foo(buf: &[u8]) -> BlockResult<()>;
/// fn foo<'a>(buf: &'a [u8]) -> BlockFutureResult<'a, ()>;
/// ```
///
/// Except that the second function is guaranteed to return a boxed future, which is why we use
/// this style.
pub type BlockFutureResult<'a, T> = BoxedFuture<'a, BlockResult<T>>;

/// Boxed future type for functions that are infallible
pub type InfallibleFuture<'a> = BoxedFuture<'a, ()>;


/// Encapsulates a raw fd that is closed when dropped
#[derive(Debug)]
pub struct OwnedFd {
    fd: RawFd,
}

impl From<RawFd> for OwnedFd {
    fn from(fd: RawFd) -> Self {
        OwnedFd { fd }
    }
}

impl std::ops::Deref for OwnedFd {
    type Target = RawFd;

    fn deref(&self) -> &RawFd {
        &self.fd
    }
}

impl AsRawFd for OwnedFd {
    fn as_raw_fd(&self) -> RawFd {
        self.fd
    }
}

impl Clone for OwnedFd {
    fn clone(&self) -> Self {
        let duped = unsafe { libc::dup(self.fd) };
        assert!(duped >= 0);
        OwnedFd { fd: duped }
    }
}

impl Drop for OwnedFd {
    fn drop(&mut self) {
        unsafe { libc::close(self.fd) };
    }
}


#[macro_export]
macro_rules! numerical_enum {
    (
        $(#[$attr:meta])*
        pub enum $enum_name:ident as $repr:tt {
            $(
                $(#[$id_attr:meta])*
                $identifier:ident = $value:literal,
            )+
        }
    ) => {
        $(#[$attr])*
        #[derive(Copy, Clone, Debug, Eq, PartialEq)]
        #[repr($repr)]
        pub enum $enum_name {
            $(
                $(#[$id_attr])*
                $identifier = $value,
            )+
        }

        impl TryFrom<$repr> for $enum_name {
            type Error = $crate::error::BlockError;
            fn try_from(val: $repr) -> $crate::error::BlockResult<Self> {
                match val {
                    $($value => Ok($enum_name::$identifier),)*
                    _ => Err($crate::error::BlockError::from_desc(format!(
                        "Invalid value for {}: {:x}",
                        stringify!($enum_name),
                        val
                    ))),
                }
            }
        }
    }
}

pub trait FlatSize {
    const SIZE: usize;
}

// TODO: Should be a procedural (derivable) macro
#[macro_export]
macro_rules! flat_size {
    (
        $(#[$attr:meta])*
        pub struct $struct_name:ident {
            $(pub $identifier:ident: $type:ty,)+
        }
    ) => {
        $(#[$attr])*
        pub struct $struct_name {
            $(pub $identifier: $type,)+
        }

        impl $crate::helpers::FlatSize for $struct_name {
            const SIZE: usize = $(<$type>::SIZE +)+ 0;
        }
    }
}

macro_rules! impl_flat_size_for_primitive {
    ($type:tt) => {
        impl $crate::helpers::FlatSize for $type {
            const SIZE: usize = std::mem::size_of::<Self>();
        }
    };
}

impl_flat_size_for_primitive!(u8);
impl_flat_size_for_primitive!(u16);
impl_flat_size_for_primitive!(u32);
impl_flat_size_for_primitive!(u64);
impl_flat_size_for_primitive!(usize);
impl_flat_size_for_primitive!(i8);
impl_flat_size_for_primitive!(i16);
impl_flat_size_for_primitive!(i32);
impl_flat_size_for_primitive!(i64);
impl_flat_size_for_primitive!(isize);

impl_flat_size_for_primitive!([u8; 1]);
impl_flat_size_for_primitive!([u8; 2]);
impl_flat_size_for_primitive!([u8; 3]);
impl_flat_size_for_primitive!([u8; 4]);


#[macro_export]
macro_rules! splittable_enum {
    (
        $(#[$attr:meta])*
        pub enum $enum_name:ident {
            $($variant:ident($encapsulated:path),)+
        }
    ) => {
        $(#[$attr])*
        pub enum $enum_name {
            $($variant($encapsulated),)+
        }

        $(
            impl TryFrom<$enum_name> for $encapsulated {
                type Error = $crate::error::BlockError;
                fn try_from(val: $enum_name) -> $crate::error::BlockResult<$encapsulated> {
                    match val {
                        $enum_name::$variant(obj) => Ok(obj),
                        #[allow(unreachable_patterns)]
                        _ => Err(format!(
                                "{:?} is not of variant {}",
                                val,
                                stringify!($variant),
                            ).into()),
                    }
                }
            }
        )*

        $(
            impl<'a> TryFrom<&'a $enum_name> for &'a $encapsulated {
                type Error = $crate::error::BlockError;
                fn try_from(val: &'a $enum_name) -> $crate::error::BlockResult<&'a $encapsulated> {
                    match val {
                        $enum_name::$variant(obj) => Ok(obj),
                        #[allow(unreachable_patterns)]
                        _ => Err(format!(
                                "{:?} is not of variant {}",
                                val,
                                stringify!($variant),
                            ).into()),
                    }
                }
            }
        )*

        $(
            impl<'a> TryFrom<&'a mut $enum_name> for &'a  mut$encapsulated {
                type Error = $crate::error::BlockError;
                fn try_from(val: &'a mut $enum_name) -> $crate::error::BlockResult<&'a mut $encapsulated> {
                    match val {
                        $enum_name::$variant(obj) => Ok(obj),
                        #[allow(unreachable_patterns)]
                        _ => Err(format!(
                                "{:?} is not of variant {}",
                                val,
                                stringify!($variant),
                            ).into()),
                    }
                }
            }
        )*

        $(
            impl From<$encapsulated> for $enum_name {
                fn from(val: $encapsulated) -> $enum_name {
                    $enum_name::$variant(val)
                }
            }
        )*
    }
}


pub trait IteratorExtensions: Iterator {
    fn try_any<E, F: FnMut(<Self as Iterator>::Item) -> Result<bool, E>>(
        &mut self,
        mut f: F,
    ) -> Result<bool, E> {
        for x in self {
            if f(x)? {
                return Ok(true);
            }
        }
        Ok(false)
    }
}

impl<I: Iterator> IteratorExtensions for I {}


/// Iterates over a `Vec<W>`, where `W` is a type that (by reference) can be converted to an
/// `Option<S>`, using a function `Fn(&W) -> Option<S>`.  Whenever this function returns `None` for
/// an element of the vector, that element is removed (using `swap_remove()`).
/// The idea is that `W` is a weak reference that can be upgraded to a strong reference `S`.
///
/// Example use cases:
/// - `W` is `sync::Weak<V>`, `S` is `Arc<V>`, `F` is `sync::Weak::upgrade`
/// - `W` is `rc::Weak<V>`, `S` is `Rc<V>`, `F` is `rc::Weak::upgrade`
pub struct WeakAutoDeleteIterator<'a, W, S, F: Fn(&W) -> Option<S>> {
    vec: &'a mut Vec<W>,
    index: usize,
    upgrade: F,
}


impl<'a, W, S, F: Fn(&W) -> Option<S>> WeakAutoDeleteIterator<'a, W, S, F> {
    /// Create a `WeakAutoDeleteIterator`.  Example upgrade functions are `sync::Weak::upgrade` if
    /// `W` is `sync::Weak<_>`, or `rc::Weak::upgrade` if `W` is `rc::Weak<_>`.
    pub fn from_vec(vec: &'a mut Vec<W>, upgrade: F) -> Self {
        WeakAutoDeleteIterator {
            vec,
            index: 0,
            upgrade,
        }
    }
}

impl<'a, W, S, F: Fn(&W) -> Option<S>> Iterator for WeakAutoDeleteIterator<'a, W, S, F> {
    type Item = S;

    fn next(&mut self) -> Option<S> {
        while self.index < self.vec.len() {
            if let Some(item) = (self.upgrade)(&self.vec[self.index]) {
                self.index += 1;
                return Some(item);
            }
            self.vec.swap_remove(self.index);
        }
        None
    }
}


// TODO: Replace by int_roundings once that is stable
pub trait IntAlignment: Sized {
    /// Align `self` down to the closest value less or equal to `self` that is aligned to
    /// `alignment`.  Returns `None` if and only if there is no such value.
    /// `alignment` must be a power of two.
    fn align_down<T: Into<Self>>(self, alignment: T) -> Option<Self>;

    /// Align `self` up to the closest value greater or equal to `self` that is aligned to
    /// `alignment`.  Returns `None` if and only if there is no such value.
    /// `alignment` must be a power of two.
    fn align_up<T: Into<Self>>(self, alignment: T) -> Option<Self>;
}

macro_rules! impl_int_alignment_for_primitive {
    ($type:tt) => {
        impl IntAlignment for $type {
            fn align_down<T: Into<Self>>(self, alignment: T) -> Option<Self> {
                let alignment: Self = alignment.into();
                debug_assert!(alignment.is_power_of_two());

                Some(self & !(alignment - 1))
            }

            fn align_up<T: Into<Self>>(self, alignment: T) -> Option<Self> {
                let alignment: Self = alignment.into();
                debug_assert!(alignment.is_power_of_two());

                if self & (alignment - 1) == 0 {
                    return Some(self);
                }
                (self | (alignment - 1)).checked_add(1)
            }
        }
    };
}

impl_int_alignment_for_primitive!(u8);
impl_int_alignment_for_primitive!(u16);
impl_int_alignment_for_primitive!(u32);
impl_int_alignment_for_primitive!(u64);
impl_int_alignment_for_primitive!(usize);


pub trait Overlaps {
    fn overlaps(&self, other: &Self) -> bool;
}

impl<I: Ord> Overlaps for std::ops::Range<I> {
    fn overlaps(&self, other: &Self) -> bool {
        self.start < other.end && other.start < self.end
    }
}


pub struct IoBuffer {
    pointer: *mut u8,
    size: usize,
    layout: Option<alloc::Layout>,
}

pub struct IoBufferRef<'a> {
    pointer: *const u8,
    size: usize,
    _lifetime: PhantomData<&'a [u8]>,
}

pub struct IoBufferMut<'a> {
    pointer: *mut u8,
    size: usize,
    _lifetime: PhantomData<&'a mut [u8]>,
}

// Blocked because of the pointer, but we want this to be usable across threads
unsafe impl Send for IoBuffer {}
unsafe impl Sync for IoBuffer {}
unsafe impl<'a> Send for IoBufferRef<'a> {}
unsafe impl<'a> Sync for IoBufferRef<'a> {}
unsafe impl<'a> Send for IoBufferMut<'a> {}
unsafe impl<'a> Sync for IoBufferMut<'a> {}

impl IoBuffer {
    /// Note that the returned buffer contains uninitialized data, which however is perfectly fine
    /// for an I/O buffer.
    pub fn new(size: usize, alignment: usize) -> BlockResult<Self> {
        let layout = alloc::Layout::from_size_align(size, alignment)?;
        Self::new_with_layout(layout)
    }

    pub fn new_with_layout(layout: alloc::Layout) -> BlockResult<Self> {
        if layout.size() == 0 {
            return Ok(IoBuffer {
                pointer: std::ptr::null_mut(),
                size: 0,
                layout: None,
            });
        }

        // We guarantee the size not to be 0 and do not care about the memory being uninitialized,
        // so this is safe
        let pointer = unsafe { alloc::System.alloc(layout) };

        if pointer.is_null() {
            return Err(format!(
                "Failed to allocate memory (size={}, alignment={})",
                layout.size(),
                layout.align()
            )
            .into());
        }

        Ok(IoBuffer {
            pointer,
            size: layout.size(),
            layout: Some(layout),
        })
    }

    pub fn len(&self) -> usize {
        self.size
    }

    pub fn as_ref(&self) -> IoBufferRef<'_> {
        IoBufferRef {
            pointer: self.pointer as *const u8,
            size: self.size,
            _lifetime: PhantomData,
        }
    }

    pub fn as_ref_range(&self, range: std::ops::Range<usize>) -> IoBufferRef<'_> {
        IoBufferRef::from_slice(&self.as_ref().into_slice()[range])
    }

    pub fn as_mut(&mut self) -> IoBufferMut<'_> {
        IoBufferMut {
            pointer: self.pointer,
            size: self.size,
            _lifetime: PhantomData,
        }
    }

    pub fn as_mut_range(&mut self, range: std::ops::Range<usize>) -> IoBufferMut<'_> {
        IoBufferMut::from_slice(&mut self.as_mut().into_slice()[range])
    }
}

impl Drop for IoBuffer {
    fn drop(&mut self) {
        if let Some(layout) = self.layout {
            // Safe because we have allocated this buffer using `alloc::System`
            unsafe {
                alloc::System.dealloc(self.pointer, layout);
            }
        }
    }
}

impl<'a> IoBufferRef<'a> {
    pub fn from_slice<T: Sized>(slice: &'a [T]) -> Self {
        IoBufferRef {
            pointer: slice.as_ptr() as *const u8,
            size: std::mem::size_of_val(slice),
            _lifetime: PhantomData,
        }
    }

    pub fn try_into_owned(self, alignment: usize) -> BlockResult<IoBuffer> {
        let mut new_buf = IoBuffer::new(self.size, alignment)?;
        new_buf
            .as_mut()
            .into_slice()
            .copy_from_slice(self.into_slice());
        Ok(new_buf)
    }

    pub fn len(&self) -> usize {
        self.size
    }

    pub fn is_empty(&self) -> bool {
        self.size == 0
    }

    pub fn as_ptr(&self) -> *const u8 {
        self.pointer
    }

    /// References to IoBuffers must not be copied/cloned, so this consumes the object
    pub fn into_slice(self) -> &'a [u8] {
        // Alignment requirement is always met, resulting data is pure binary data
        unsafe { self.into_typed_slice::<u8>() }
    }

    /// Caller must ensure that alignment and length requirements are met and that the resulting
    /// data is valid
    pub unsafe fn into_typed_slice<T: Sized>(self) -> &'a [T] {
        std::slice::from_raw_parts(
            self.pointer as *const T,
            self.size / std::mem::size_of::<T>(),
        )
    }

    pub fn split_at(self, mid: usize) -> (IoBufferRef<'a>, IoBufferRef<'a>) {
        let head_len = std::cmp::min(mid, self.size);

        (
            IoBufferRef {
                pointer: self.pointer,
                size: head_len,
                _lifetime: PhantomData,
            },
            IoBufferRef {
                // Safe because we have limited this to `self.size`
                pointer: unsafe { self.pointer.add(head_len) },
                size: self.size - head_len,
                _lifetime: PhantomData,
            },
        )
    }
}

impl<'a> IoBufferMut<'a> {
    pub fn from_slice<T: Sized>(slice: &'a mut [T]) -> Self {
        IoBufferMut {
            pointer: slice.as_ptr() as *mut u8,
            size: std::mem::size_of_val(slice),
            _lifetime: PhantomData,
        }
    }

    pub fn len(&self) -> usize {
        self.size
    }

    pub fn is_empty(&self) -> bool {
        self.size == 0
    }

    pub fn as_ptr(&self) -> *const u8 {
        self.pointer
    }

    /// References to IoBuffers must not be copied/cloned, so this consumes the object
    pub fn into_slice(self) -> &'a mut [u8] {
        // Alignment requirement is always meant, resulting data is pure binary data
        unsafe { self.into_typed_slice::<u8>() }
    }

    /// Caller must ensure that alignment and length requirements are met and that the resulting
    /// data is valid
    pub unsafe fn into_typed_slice<T: Sized>(self) -> &'a mut [T] {
        std::slice::from_raw_parts_mut(self.pointer as *mut T, self.size / std::mem::size_of::<T>())
    }

    pub fn into_ref(self) -> IoBufferRef<'a> {
        IoBufferRef {
            pointer: self.pointer,
            size: self.size,
            _lifetime: PhantomData,
        }
    }

    pub fn split_at(self, mid: usize) -> (IoBufferMut<'a>, IoBufferMut<'a>) {
        let head_len = std::cmp::min(mid, self.size);

        (
            IoBufferMut {
                pointer: self.pointer,
                size: head_len,
                _lifetime: PhantomData,
            },
            IoBufferMut {
                // Safe because we have limited this to `self.size`
                pointer: unsafe { self.pointer.add(head_len) },
                size: self.size - head_len,
                _lifetime: PhantomData,
            },
        )
    }
}


/// Wrap an object of type `T` such that when it is dropped, it will be sent to a
/// `oneshot::Receiver<T>`, if any was registered.  This allows running the receiver in an async
/// context to run async drop code.
pub struct SendOnDrop<T> {
    inner: Option<T>,
    notifier: Mutex<Option<oneshot::Sender<T>>>,
}

impl<T> SendOnDrop<T> {
    /// Wrap the object in `SendOnDrop`, but without installing an on-drop receiver yet.  Dropping
    /// the object will just drop it.
    pub fn new(object: T) -> Self {
        SendOnDrop {
            inner: Some(object),
            notifier: Mutex::new(None),
        }
    }

    /// Install a receiver to retrieve the object when the wrapper is dropped.  Only one receiver
    /// can be installed at a time, so if there already is one, this will return an error.
    pub fn make_receiver(&self) -> BlockResult<oneshot::Receiver<T>> {
        let (sender, receiver) = oneshot::channel();

        let mut notifier = self.notifier.lock().unwrap();
        if notifier.is_some() {
            return Err(format!(
                "Tried to add a receiver to SendOnDrop<{}>, which already has one",
                std::any::type_name::<T>()
            )
            .into());
        }
        *notifier = Some(sender);
        Ok(receiver)
    }

    /// Install a receiver on an `Arc<SendOnDrop<T>>` while consuming the `Arc<_>`.  If there is an
    /// error because there already is a receiver installed, the `Arc<_>` is returned again.
    pub fn into_receiver(self: Arc<Self>) -> Result<oneshot::Receiver<T>, Arc<Self>> {
        self.make_receiver().map_err(|_| self)
    }

    /// Try to retrieve the wrapped value.  This will fail if there already is a receiver
    /// installed, because the receiver is guaranteed to get it.
    pub fn try_unwrap(mut self) -> Result<T, Self> {
        if self.notifier.get_mut().unwrap().is_some() {
            Err(self)
        } else {
            Ok(self.inner.take().unwrap())
        }
    }
}

impl<T: Default> Default for SendOnDrop<T> {
    fn default() -> Self {
        SendOnDrop::new(<T as Default>::default())
    }
}

impl<T> Drop for SendOnDrop<T> {
    fn drop(&mut self) {
        // If `self.inner` is `None`, the object was already taken via `try_unwrap()`.
        if let Some(inner) = self.inner.take() {
            if let Some(sender) = self.notifier.get_mut().unwrap().take() {
                let _: Result<(), _> = sender.send(inner);
            }
        }
    }
}

impl<T> std::ops::Deref for SendOnDrop<T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.inner.as_ref().unwrap()
    }
}

impl<T: std::fmt::Debug> std::fmt::Debug for SendOnDrop<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <T as std::fmt::Debug>::fmt(self.inner.as_ref().unwrap(), f)
    }
}


/// Allows joining an arbitrary number of futures.  When awaited, returns `Ok(())` on success, or
/// the first error encountered.
pub struct FutureJoin<'a> {
    /// If there's just a single future, store it here for a quicker path
    single: Option<BlockFutureResult<'a, ()>>,
    vec: Option<Vec<BlockFutureResult<'a, ()>>>,
}

impl<'a> FutureJoin<'a> {
    pub fn new() -> Self {
        FutureJoin {
            single: None,
            vec: None,
        }
    }

    /// Add a future to the mix
    pub fn push(&mut self, fut: BlockFutureResult<'a, ()>) {
        if let Some(vec) = self.vec.as_mut() {
            vec.push(fut);
        } else if let Some(single) = self.single.take() {
            self.vec = Some(vec![single, fut]);
        } else {
            self.single = Some(fut);
        }
    }
}

impl<'a> Future for FutureJoin<'a> {
    type Output = BlockResult<()>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if let Some(single) = self.single.as_mut() {
            Future::poll(single.as_mut(), cx)
        } else if let Some(vec) = self.vec.as_mut() {
            let mut i = 0;
            while i < vec.len() {
                match Future::poll(vec[i].as_mut(), cx) {
                    Poll::Ready(Ok(())) => {
                        vec.swap_remove(i);
                    }
                    Poll::Ready(Err(e)) => return Poll::Ready(Err(e)),
                    Poll::Pending => i += 1,
                }
            }
            if i == 0 {
                Poll::Ready(Ok(()))
            } else {
                Poll::Pending
            }
        } else {
            Poll::Ready(Ok(()))
        }
    }
}


#[derive(Clone, Debug, Default, PartialEq, Eq)]
/// Similar to `Option<T>`, but differentiates between `NotSpecified` (use implicit behavior) and
/// `Null` (explicitly no value).  For serialization, parent objects must ensure that
/// `Tristate::NotSpecified` is not serialized (i.e. via
/// `#[serde(skip_serializing_if = "Tristate::skip_serializing")]`).
pub enum Tristate<T> {
    #[default]
    NotSpecified,
    Null,
    Some(T),
}

struct TristateVisitor<T>(PhantomData<T>);

impl<T> Tristate<T> {
    pub fn skip_serializing(&self) -> bool {
        matches!(self, Tristate::NotSpecified)
    }
}

impl<'de, T: Deserialize<'de>> de::Visitor<'de> for TristateVisitor<T> {
    type Value = Tristate<T>;

    fn expecting(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            fmt,
            "nothing (implicit behavior), null (explicitly nothing), or a value of type {}",
            std::any::type_name::<T>()
        )
    }

    /// Explicitly nothing
    fn visit_unit<E: de::Error>(self) -> Result<Self::Value, E> {
        Ok(Tristate::Null)
    }

    fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
        let obj = T::deserialize(serde::de::value::StrDeserializer::new(v))?;
        Ok(Tristate::Some(obj))
    }

    fn visit_map<M: de::MapAccess<'de>>(self, map: M) -> Result<Self::Value, M::Error> {
        let obj = T::deserialize(de::value::MapAccessDeserializer::new(map))?;
        Ok(Tristate::Some(obj))
    }
}

impl<'de, T: Deserialize<'de>> Deserialize<'de> for Tristate<T> {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        deserializer.deserialize_any(TristateVisitor(PhantomData::<T> {}))
    }
}

impl<T: Serialize> Serialize for Tristate<T> {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        match self {
            Tristate::NotSpecified => panic!("Cannot directly serialize Tristate::NotSpecified -- parent object must skip serialization"),
            Tristate::Null => serializer.serialize_unit(),
            Tristate::Some(obj) => serializer.serialize_some(obj),
        }
    }
}
