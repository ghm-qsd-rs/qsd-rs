use crate::error::BlockResult;
use crate::helpers::{FlatSize, OwnedFd};
use serde::{Deserialize, Serialize};
use std::cmp::Ordering::{Equal, Greater, Less};
use std::future::Future;
use std::mem::size_of;
use std::os::unix::io::{AsRawFd, RawFd};
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::io::unix::AsyncFd;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt, ReadBuf};
use tokio::net::{tcp, unix, TcpListener, TcpStream, UnixListener, UnixStream};


#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(tag = "type", rename_all = "kebab-case")]
pub enum SocketAddr {
    Inet(SocketInetAddr),
    Unix(SocketUnixAddr),
}

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct SocketInetAddr {
    pub host: String,
    pub port: String, // Compatibility to CQSD
}

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct SocketUnixAddr {
    pub path: String,
}


impl SocketAddr {
    pub async fn into_listener(self) -> BlockResult<NetListener> {
        let listener = match self {
            SocketAddr::Inet(inet) => {
                NetListener::Tcp(TcpListener::bind(format!("{}:{}", inet.host, inet.port)).await?)
            }
            SocketAddr::Unix(unix) => {
                let listener = UnixListener::bind(unix.path.clone())?;
                NetListener::Unix {
                    listener,
                    absolute_path: std::path::Path::new(&unix.path).canonicalize()?,
                }
            }
        };

        Ok(listener)
    }

    pub async fn connect(self) -> BlockResult<NetStream> {
        let stream = match self {
            SocketAddr::Inet(inet) => {
                NetStream::Tcp(TcpStream::connect(format!("{}:{}", inet.host, inet.port)).await?)
            }
            SocketAddr::Unix(unix) => NetStream::Unix(UnixStream::connect(unix.path).await?),
        };

        Ok(stream)
    }
}

pub enum NetListener {
    Tcp(TcpListener),
    Unix {
        listener: UnixListener,
        absolute_path: std::path::PathBuf,
    },
}

impl NetListener {
    pub fn poll_accept(&self, cx: &mut Context<'_>) -> Poll<std::io::Result<NetStream>> {
        match self {
            NetListener::Tcp(t) => match t.poll_accept(cx) {
                Poll::Ready(Ok((stream, _))) => Poll::Ready(Ok(NetStream::Tcp(stream))),
                Poll::Ready(Err(e)) => Poll::Ready(Err(e)),
                Poll::Pending => Poll::Pending,
            },

            NetListener::Unix {
                listener,
                absolute_path: _,
            } => match listener.poll_accept(cx) {
                Poll::Ready(Ok((stream, _))) => Poll::Ready(Ok(NetStream::Unix(stream))),
                Poll::Ready(Err(e)) => Poll::Ready(Err(e)),
                Poll::Pending => Poll::Pending,
            },
        }
    }
}

impl Future for NetListener {
    type Output = std::io::Result<NetStream>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        self.poll_accept(cx)
    }
}

impl Drop for NetListener {
    fn drop(&mut self) {
        match self {
            NetListener::Tcp(_) => (),
            NetListener::Unix {
                listener: _,
                absolute_path,
            } => {
                let _: Result<(), _> = std::fs::remove_file(absolute_path);
            }
        }
    }
}


macro_rules! defer_async_read {
    ($enum_name:ident) => {
        impl AsyncRead for $enum_name {
            fn poll_read(
                mut self: Pin<&mut Self>,
                cx: &mut Context<'_>,
                buf: &mut ReadBuf<'_>,
            ) -> Poll<std::io::Result<()>> {
                match &mut *self.as_mut() {
                    $enum_name::Tcp(t) => AsyncRead::poll_read(Pin::new(t), cx, buf),
                    $enum_name::Unix(u) => AsyncRead::poll_read(Pin::new(u), cx, buf),
                }
            }
        }
    };
}

macro_rules! defer_async_write {
    ($enum_name:ident) => {
        impl AsyncWrite for $enum_name {
            fn poll_write(
                mut self: Pin<&mut Self>,
                cx: &mut Context<'_>,
                buf: &[u8],
            ) -> Poll<std::io::Result<usize>> {
                match &mut *self.as_mut() {
                    $enum_name::Tcp(t) => AsyncWrite::poll_write(Pin::new(t), cx, buf),
                    $enum_name::Unix(u) => AsyncWrite::poll_write(Pin::new(u), cx, buf),
                }
            }

            fn poll_write_vectored(
                mut self: Pin<&mut Self>,
                cx: &mut Context<'_>,
                bufs: &[std::io::IoSlice<'_>],
            ) -> Poll<std::io::Result<usize>> {
                match &mut *self.as_mut() {
                    $enum_name::Tcp(t) => AsyncWrite::poll_write_vectored(Pin::new(t), cx, bufs),
                    $enum_name::Unix(u) => AsyncWrite::poll_write_vectored(Pin::new(u), cx, bufs),
                }
            }

            fn is_write_vectored(&self) -> bool {
                match self {
                    $enum_name::Tcp(t) => AsyncWrite::is_write_vectored(t),
                    $enum_name::Unix(u) => AsyncWrite::is_write_vectored(u),
                }
            }

            fn poll_flush(
                mut self: Pin<&mut Self>,
                cx: &mut Context<'_>,
            ) -> Poll<std::io::Result<()>> {
                match &mut *self.as_mut() {
                    $enum_name::Tcp(t) => AsyncWrite::poll_flush(Pin::new(t), cx),
                    $enum_name::Unix(u) => AsyncWrite::poll_flush(Pin::new(u), cx),
                }
            }

            fn poll_shutdown(
                mut self: Pin<&mut Self>,
                cx: &mut Context<'_>,
            ) -> Poll<std::io::Result<()>> {
                match &mut *self.as_mut() {
                    $enum_name::Tcp(t) => AsyncWrite::poll_shutdown(Pin::new(t), cx),
                    $enum_name::Unix(u) => AsyncWrite::poll_shutdown(Pin::new(u), cx),
                }
            }
        }
    };
}


pub enum NetStream {
    Tcp(TcpStream),
    Unix(UnixStream),
}

impl NetStream {
    pub fn into_split(self) -> (NetReadHalf, NetWriteHalf) {
        match self {
            NetStream::Tcp(t) => {
                let (r, w) = t.into_split();
                (NetReadHalf::Tcp(r), NetWriteHalf::Tcp(w))
            }

            NetStream::Unix(u) => {
                let (r, w) = u.into_split();
                (NetReadHalf::Unix(r), NetWriteHalf::Unix(w))
            }
        }
    }
}

defer_async_read!(NetStream);
defer_async_write!(NetStream);


pub enum NetReadHalf {
    Tcp(tcp::OwnedReadHalf),
    Unix(unix::OwnedReadHalf),
}

pub enum NetWriteHalf {
    Tcp(tcp::OwnedWriteHalf),
    Unix(unix::OwnedWriteHalf),
}

defer_async_read!(NetReadHalf);
defer_async_write!(NetWriteHalf);


/// Read a flat object of the given type from the I/O stream.
/// (Cannot be a trait because trait functions cannot trivially be async.)
#[allow(dead_code)] // Currently unused, but present for symmetry
pub async fn read_obj<C: AsyncReadExt + Unpin, T: FlatSize>(con: &mut C) -> std::io::Result<T> {
    let mut obj = unsafe {
        #[allow(clippy::uninit_assumed_init)]
        std::mem::MaybeUninit::<T>::uninit().assume_init()
    };
    let slice = unsafe { std::slice::from_raw_parts_mut(&mut obj as *mut T as *mut u8, T::SIZE) };
    con.read_exact(slice).await?;
    Ok(obj)
}

/// Write a flat object of the given type into the I/O stream.
/// (Cannot be a trait because trait functions cannot trivially be async.)
pub async fn write_obj<C: AsyncWriteExt + Unpin, T: FlatSize>(
    con: &mut C,
    obj: T,
) -> std::io::Result<()> {
    let slice = unsafe { std::slice::from_raw_parts(&obj as *const T as *const u8, T::SIZE) };
    con.write_all(slice).await
}

/// Read a flat object of the given type from a Unix network stream, together with file descriptors
/// (ancillary data).
/// Can receive at most 16 FDs at a time.
/// (Cannot be a trait because trait functions cannot trivially be async.)
pub async fn read_obj_and_fds<T: FlatSize>(
    con: &mut std::os::unix::net::UnixStream,
) -> std::io::Result<(T, Vec<OwnedFd>)> {
    let mut obj = unsafe {
        #[allow(clippy::uninit_assumed_init)]
        std::mem::MaybeUninit::<T>::uninit().assume_init()
    };

    #[derive(Copy, Clone)]
    #[repr(C, packed)]
    struct CmsgFds {
        header: libc::cmsghdr,
        fds: [RawFd; 16],
    }

    let mut cmsg = CmsgFds {
        header: libc::cmsghdr {
            cmsg_len: 0,
            cmsg_level: 0,
            cmsg_type: 0,
        },
        fds: [-1; 16],
    };

    let mut iov = libc::iovec {
        iov_base: &mut obj as *mut _ as *mut libc::c_void,
        iov_len: T::SIZE,
    };

    let mut msg = libc::msghdr {
        msg_name: std::ptr::null_mut(),
        msg_namelen: 0,
        msg_iov: &mut iov as *mut _,
        msg_iovlen: 1,
        msg_control: &mut cmsg as *mut _ as *mut libc::c_void,
        msg_controllen: size_of::<CmsgFds>(),
        msg_flags: 0,
    };

    let async_fd = AsyncFd::new(con.as_raw_fd())?;

    let mut remaining = T::SIZE;
    while remaining > 0 {
        let mut guard = async_fd.readable().await?;

        let ret = unsafe { libc::recvmsg(*async_fd.get_ref(), &mut msg as *mut _, 0) };
        match ret.cmp(&0) {
            Less => {
                let err = std::io::Error::last_os_error();
                if err.kind() == std::io::ErrorKind::WouldBlock {
                    guard.clear_ready();
                    continue;
                }
                guard.retain_ready();
                return Err(err);
            }

            Equal => return Err(std::io::Error::from(std::io::ErrorKind::ConnectionAborted)),

            Greater => {
                guard.retain_ready();
                let ret = ret as usize;

                remaining -= ret;
                if remaining > 0 {
                    iov.iov_base = (iov.iov_base as usize + ret) as *mut libc::c_void;
                    iov.iov_len = remaining;
                }
            }
        }
    }

    let mut fds = Vec::new();

    if cmsg.header.cmsg_len > 0 {
        // SOL_SOCKET?
        if cmsg.header.cmsg_level != 1 {
            let cmsg_level = cmsg.header.cmsg_level;
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!(
                    "Attempting to receive FDs through a unix socket failed \
                     (expected cmsg_level == 1, got {})",
                    cmsg_level,
                ),
            ));
        }

        // SCM_RIGHTS?
        if cmsg.header.cmsg_type != 1 {
            let cmsg_type = cmsg.header.cmsg_type;
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!(
                    "Attempting to receive FDs through a unix socket failed \
                     (expected cmsg_type == 1, got {})",
                    cmsg_type,
                ),
            ));
        }

        let fd_count = (cmsg.header.cmsg_len - size_of::<libc::cmsghdr>()) / size_of::<RawFd>();
        for i in 0..fd_count {
            fds.push(cmsg.fds[i].into());
        }
    }

    Ok((obj, fds))
}
