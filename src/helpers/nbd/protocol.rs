use crate::error::{BlockError, BlockResult};
use crate::{flat_size, numerical_enum};
use bincode::Options;
use serde::{Deserialize, Serialize};
use std::io;

flat_size! {
    #[derive(Deserialize, Serialize)]
    pub struct NbdServerHandshake {
        pub magic1: u64,
        pub magic2: u64,
        pub flags: u16,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    pub struct NbdClientFlags {
        pub flags: u32,
    }
}

numerical_enum! {
    pub enum NbdOpt as u32 {
        Abort = 2,
        List = 3,
        Info = 6,
        Go = 7,
        StructuredReply = 8,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    pub struct NbdClientOpt {
        pub magic: u64,
        pub option: u32,
        pub data_length: u32,
    }
}

numerical_enum! {
    pub enum NbdReplyType as u32 {
        Ack = 1,
        Server = 2,
        Info = 3,
        MetaContext = 4,

        ErrUnsup = 0x80000001,
        ErrPolicy = 0x80000002,
        ErrInvalid = 0x80000003,
        ErrPlatform = 0x80000004,
        ErrTlsReqd = 0x80000005,
        ErrUnknown = 0x80000006,
        ErrShutdown = 0x80000007,
        ErrBlockSizeReqd = 0x80000008,
        ErrTooBig = 0x80000009,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    pub struct NbdServerReply {
        pub magic: u64,
        pub client_option: u32,
        pub reply_type: u32,
        pub data_length: u32,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    pub struct NbdInfoExport {
        pub info_type: u16,
        pub export_size: u64,
        pub transmission_flags: u16,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    pub struct NbdInfoBlockSize {
        pub info_type: u16,
        pub minimum_block_size: u32,
        pub preferred_block_size: u32,
        pub maximum_block_size: u32,
    }
}

numerical_enum! {
    pub enum NbdRequestType as u16 {
        Read = 0,
        Write = 1,
        Disc = 2,
        Flush = 3,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    pub struct NbdRequest {
        pub magic: u32,
        pub command_flags: u16,
        pub request_type: u16,
        pub handle: u64,
        pub offset: u64,
        pub length: u32,
    }
}

numerical_enum! {
    pub enum NbdResult as u32 {
        Ok = 0,
        Eperm = 1,
        Eio = 5,
        Enomem = 12,
        Einval = 22,
        Enospc = 28,
        Eoverflow = 75,
        Enotsup = 95,
        Eshutdown = 108,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    pub struct NbdSimpleReply {
        pub magic: u32,
        pub error: u32,
        pub handle: u64,
    }
}

impl NbdServerHandshake {
    pub const FIXED_NEWSTYLE: u16 = 1;

    pub fn new(flags: u16) -> Self {
        NbdServerHandshake {
            magic1: 0x4e42444d41474943,
            magic2: 0x49484156454f5054,
            flags,
        }
    }
}

impl NbdClientFlags {
    pub fn check(&self) -> BlockResult<()> {
        if self.flags == 1 {
            Ok(())
        } else {
            Err(BlockError::from_desc(format!(
                "Unrecognized client flags: {:x}",
                self.flags
            )))
        }
    }
}

impl NbdClientOpt {
    pub fn check(&self) -> BlockResult<()> {
        if self.magic != 0x49484156454f5054 {
            return Err(BlockError::from_desc(format!(
                "Unrecognized client option magic: {:x}",
                self.magic
            )));
        }

        if self.data_length > 1024 {
            return Err(BlockError::from_desc(format!(
                "Client option data too long: {}",
                self.data_length
            )));
        }

        Ok(())
    }

    /// For NBD_OPT_GO or NBD_OPT_INFO requests, get the requested export name
    pub fn export_name(data: &[u8]) -> BlockResult<&str> {
        let bincode = bincode::DefaultOptions::new()
            .with_fixint_encoding()
            .with_big_endian();

        let len: u32 = bincode.deserialize(&data[0..=3])?;
        Ok(std::str::from_utf8(&data[4..(len as usize + 4)])?)
    }
}

impl NbdServerReply {
    pub fn new(client_option: u32, reply_type: NbdReplyType, data_length: u32) -> Self {
        NbdServerReply {
            magic: 0x3e889045565a9,
            client_option,
            reply_type: reply_type as u32,
            data_length,
        }
    }
}

numerical_enum! {
    pub enum NbdFlag as u16 {
        HasFlags = 0x1,
        ReadOnly = 0x2,
        SendFlush = 0x4,
        SendFua = 0x8,
        Rotational = 0x10,
        SendTrim = 0x20,
        SendWriteZeroes = 0x40,
        SendDf = 0x80,
        CanMultiConn = 0x100,
        SendResize = 0x200,
        SendCache = 0x400,
        SendFastZero = 0x800,
    }
}

impl NbdInfoBlockSize {
    pub fn new(minimum: u32, preferred: u32, maximum: u32) -> Self {
        NbdInfoBlockSize {
            info_type: 3,
            minimum_block_size: minimum,
            preferred_block_size: preferred,
            maximum_block_size: maximum,
        }
    }
}

impl NbdRequest {
    pub fn check(&self) -> BlockResult<()> {
        if self.magic != 0x25609513 {
            return Err(BlockError::from_desc(format!(
                "Unrecognized request magic: {:x}",
                self.magic
            )));
        }

        if self.command_flags != 0 {
            return Err(BlockError::from_desc(format!(
                "Unrecognized command flags: {:x}",
                self.command_flags
            )));
        }

        let request: BlockResult<NbdRequestType> = self.request_type.try_into();
        if request.is_err() {
            return Err(BlockError::from_desc(format!(
                "Unrecognized request: {:x}",
                self.request_type
            )));
        }

        if self.length > 32 * 1024 * 1024 {
            return Err(BlockError::from_desc(format!(
                "Request is too long: {}",
                self.length
            )));
        }

        Ok(())
    }

    pub fn request_type(&self) -> NbdRequestType {
        self.request_type.try_into().unwrap()
    }
}

impl From<BlockError> for NbdResult {
    fn from(err: BlockError) -> Self {
        match err.into_inner().kind() {
            io::ErrorKind::PermissionDenied => NbdResult::Eperm,
            io::ErrorKind::InvalidData | io::ErrorKind::InvalidInput => NbdResult::Einval,
            io::ErrorKind::StorageFull => NbdResult::Enospc,
            io::ErrorKind::OutOfMemory => NbdResult::Enomem,
            _ => NbdResult::Eio,
        }
    }
}

impl<T> From<BlockResult<T>> for NbdResult {
    fn from(res: BlockResult<T>) -> Self {
        match res {
            Ok(_) => NbdResult::Ok,
            Err(e) => e.into(),
        }
    }
}

impl NbdSimpleReply {
    pub fn new(request: NbdRequest, error: NbdResult) -> Self {
        NbdSimpleReply {
            magic: 0x67446698,
            error: error as u32,
            handle: request.handle,
        }
    }
}
