use crate::error::BlockResult;
use crate::helpers::BoxedFuture;
use crate::monitor::chardev::{Chardev, ChardevReadHalf, ChardevWriteHalf};
use crate::monitor::{self, qmp, Monitor};
use futures::FutureExt;
use serde::{Deserialize, Serialize};
use std::collections::VecDeque;
use std::future::Future;
use std::rc::Rc;
use std::task::{Context, Poll};
use tokio::sync::mpsc;

/// A prompt is an external interface to the global rsd monitor.  It allows manipulating the
/// monitor by sending QMP commands, and receives state information e.g. through QMP events.
pub struct Prompt {
    pub id: String,
    future: BoxedFuture<'static, (Chardev, BlockResult<()>)>,

    events: mpsc::UnboundedSender<PromptEvent>,
}

struct PromptImpl {
    read_half: ChardevReadHalf,
    write_half: ChardevWriteHalf,

    monitor: Rc<Monitor>,
    input_buf: VecDeque<u8>,

    events: mpsc::UnboundedReceiver<PromptEvent>,

    in_negotiation: bool,
}

#[derive(Debug)]
enum PromptEvent {
    QmpEvent(String),
    Quit,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct PromptConfig {
    id: String,
    pub chardev: String,
}

impl Prompt {
    /// Create a new prompt on the given character device (which the prompt takes ownership of)
    pub async fn new(opts: PromptConfig, chardev: Chardev) -> BlockResult<Self> {
        let (events_s, events_r) = mpsc::unbounded_channel::<PromptEvent>();

        let (rh, wh) = chardev.split();

        let prompt = PromptImpl {
            read_half: rh,
            write_half: wh,
            monitor: monitor::monitor(),
            input_buf: VecDeque::new(),
            in_negotiation: true,
            events: events_r,
        };

        Ok(Prompt {
            id: opts.id,
            future: Box::pin(prompt.run()),
            events: events_s,
        })
    }

    /// Poll the prompt.  When done, ownership of the character device is returned to the monitor.
    pub fn poll(&mut self, cx: &mut Context<'_>) -> Poll<(Chardev, BlockResult<()>)> {
        Future::poll(self.future.as_mut(), cx)
    }

    /// Submit the given event via this prompt
    pub fn submit_event(&mut self, event: &qmp::Event) {
        self.events
            .send(PromptEvent::QmpEvent(event.to_json()))
            .unwrap();
    }

    /// Ask the prompt to close
    pub fn close(&mut self) {
        self.events.send(PromptEvent::Quit).unwrap();
    }
}

/// Turns single bytes into unicode characters, assuming utf-8 encoding
struct Utf8Reader {
    collect: u32,
    len_remaining: usize,
}

impl Utf8Reader {
    /// Create a new utf-8 reader
    fn new() -> Self {
        Utf8Reader {
            collect: 0,
            len_remaining: 0,
        }
    }

    /// Push a byte into the reader.  Returns any of the following:
    /// - `Ok(Some(_))`: A complete utf-8 character has been processed, this is its unicode
    ///   representation
    /// - `Ok(None)`: The utf-8 character is incomplete, further bytes are required
    /// - `Err(_)`: Encountered invalid utf-8 encoding or invalid unicode
    fn push(&mut self, byte: u8) -> BlockResult<Option<char>> {
        if byte & 0x80 == 0x00 {
            if self.len_remaining > 0 {
                return Err("Encountered invalid UTF-8".into());
            }
            self.collect = byte as u32;
        } else if byte & 0xc0 == 0x80 {
            if self.len_remaining == 0 {
                return Err("Encountered invalid UTF-8".into());
            }
            self.collect = (self.collect << 6) | (byte & 0x3f) as u32;
            self.len_remaining -= 1;
        } else {
            if self.len_remaining > 0 {
                return Err("Encountered invalid UTF-8".into());
            }

            let mut mask: u8 = 0x1f;
            let mut len_rem: usize = 1;
            while mask != 0 {
                if byte & !mask == !mask >> 1 {
                    self.collect = (byte & mask) as u32;
                    self.len_remaining = len_rem;
                    break;
                }
                mask >>= 1;
                len_rem += 1;
            }

            if mask == 0 {
                return Err("Encountered invalid UTF-8".into());
            }
        }

        if self.len_remaining == 0 {
            match char::from_u32(self.collect) {
                Some(chr) => Ok(Some(chr)),
                None => Err(format!("Encountered invalid unicode {:x}", self.collect).into()),
            }
        } else {
            Ok(None)
        }
    }
}

impl PromptImpl {
    /// Run the prompt, taking ownership of the character device, and returning that ownership when
    /// done
    async fn run(mut self) -> (Chardev, BlockResult<()>) {
        if let Err(e) = self.send_greeting().await {
            return (self.reconstruct_chardev(), Err(e));
        }

        while !self.read_half.is_closed() {
            if let Err(e) = self.iteration().await {
                let qmp_err = qmp::error(e).to_json();
                if let Err(e) = self.write_half.write_all(qmp_err.as_bytes()).await {
                    return (self.reconstruct_chardev(), Err(e));
                }
            }
        }

        self.read_half.close().await;
        self.write_half.close().await;
        (self.reconstruct_chardev(), Ok(()))
    }

    /// When done, reconstruct the original character device from the halves into which it was
    /// split
    fn reconstruct_chardev(self) -> Chardev {
        Chardev::construct(self.read_half, self.write_half)
    }

    /// Try to read a QMP object, and process it.  While we wait for an input object to appear, we
    /// also wait for events in the queue and send them out if available.
    async fn iteration(&mut self) -> BlockResult<()> {
        let input = self.read_input_object().await?;
        if !input.contains(|c: char| !c.is_whitespace()) {
            return Ok(());
        }

        let cmd: qmp::Command = serde_json::from_str(&input)?;

        let result = if !self.in_negotiation {
            self.monitor.execute(cmd).await
        } else {
            let result = self.monitor.execute_preamble(cmd).await;
            if result.is_ok() {
                self.in_negotiation = false;
            }
            result
        };

        self.write_half
            .write_all(result.to_json().as_bytes())
            .await?;

        Ok(())
    }

    /// Send the QMP greeting
    async fn send_greeting(&mut self) -> BlockResult<()> {
        let greeting = serde_json::json!({
            "QMP": {
                "version": {
                    "qemu": {
                        "major": 7,
                        "minor": 0,
                        "micro": 0,
                    },
                    "rsd": {
                        "major": env!("CARGO_PKG_VERSION_MAJOR"),
                        "minor": env!("CARGO_PKG_VERSION_MINOR"),
                        "micro": env!("CARGO_PKG_VERSION_PATCH"),
                    },
                    "package": "",
                },
                "capabilities": [
                ],
            },
        });

        let mut s = serde_json::to_string(&greeting)?;
        s.push('\n');
        self.write_half.write_all(s.as_bytes()).await?;

        Ok(())
    }

    /// Refill an empty input buffer (reading data from the character device), and while waiting
    /// for input, also check the event channel for events to send (and send them)
    async fn refill_input_buf(&mut self) -> BlockResult<()> {
        assert!(self.input_buf.is_empty());

        self.input_buf.resize(1024, 0);

        let len = {
            // Tokio says cancelling this future can incur data loss, but we will not cancel this;
            // the `loop` runs until it is completed, and does not restart it
            let reader = self
                .read_half
                .read_partial(self.input_buf.as_mut_slices().0)
                .fuse();
            futures::pin_mut!(reader);

            loop {
                // Tokio promises that this is safely cancellable and restartable
                let event_waiter = self.events.recv().fuse();
                futures::pin_mut!(event_waiter);

                futures::select! {
                    event = event_waiter => match event.unwrap() {
                        PromptEvent::QmpEvent(event) => self.write_half.write_all(event.as_bytes()).await?,
                        PromptEvent::Quit => break 0,
                    },
                    len = reader => break len?,
                }
            }
        };

        if len == 0 {
            self.read_half.close().await;
        }

        self.input_buf.resize(len, 0);

        Ok(())
    }

    /// Read a whole JSON object from the input (enclosed into '{}').  If an event appears while we
    /// wait for something to be read, it will be sent out.
    async fn read_input_object(&mut self) -> BlockResult<String> {
        let mut input = String::new();
        let mut utf8reader = Utf8Reader::new();

        let mut brace_counter: usize = 0;
        let mut start = true;

        while brace_counter > 0 || start {
            let chr = loop {
                let byte = match self.input_buf.pop_front() {
                    Some(byte) => byte,
                    None => {
                        if self.read_half.is_closed() {
                            return Ok(input);
                        }

                        self.refill_input_buf().await?;
                        continue;
                    }
                };

                if let Some(chr) = utf8reader.push(byte)? {
                    break chr;
                }
            };

            if start && !chr.is_whitespace() {
                if chr != '{' {
                    return Err(format!("Encountered character '{}', expected '{{'", chr).into());
                }
                start = false;
            }

            if chr == '{' {
                brace_counter += 1;
            } else if chr == '}' {
                brace_counter -= 1;
            }

            input.push(chr);
        }

        Ok(input)
    }
}
