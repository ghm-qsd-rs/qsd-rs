use crate::error::BlockError;
use crate::monitor::chardev::ChardevConfig;
use crate::monitor::prompt::PromptConfig;
use crate::monitor::ObjectConfig;
use crate::node::{self, copy, NodeConfig, PollableNodeStopMode};
use crate::server::{nbd, ServerConfig};
use serde::{Deserialize, Serialize};

/// QMP commands
#[derive(Debug, Deserialize)]
#[serde(
    tag = "execute",
    content = "arguments",
    rename_all = "kebab-case",
    deny_unknown_fields
)]
pub enum Command {
    #[serde(rename = "qmp_capabilities")]
    QmpCapabilities,

    BlockdevAdd(NodeConfig),

    BlockdevDel {
        #[serde(rename = "node-name")]
        node_name: String,
    },

    #[serde(rename_all = "kebab-case")]
    BlockdevBackup {
        job_id: String,
        device: String,
        target: String,
        sync: copy::SyncMode,
        granularity: Option<usize>,
        buf_size: Option<usize>,
        filter_node_name: Option<String>,
        auto_dismiss: Option<bool>,
        auto_finalize: Option<bool>,
    },

    #[serde(rename_all = "kebab-case")]
    BlockdevMirror {
        job_id: String,
        device: String,
        target: String,
        sync: copy::SyncMode,
        granularity: Option<usize>,
        buf_size: Option<usize>,
        filter_node_name: Option<String>,
        copy_mode: Option<MirrorCopyMode>,
        auto_dismiss: Option<bool>,
        auto_finalize: Option<bool>,
    },

    BlockdevPause {
        #[serde(rename = "node-name")]
        node_name: String,
    },

    BlockdevReopen {
        options: Vec<NodeConfig>,
    },

    BlockdevResume {
        #[serde(rename = "node-name")]
        node_name: String,
    },

    BlockdevStop {
        #[serde(rename = "node-name")]
        node_name: String,
        #[serde(default)]
        mode: PollableNodeStopMode,
    },

    BlockDirtyBitmapAdd {
        node: String,
        name: String,
        granularity: Option<u64>,
        disabled: Option<bool>,
    },

    BlockDirtyBitmapClear(BlockDirtyBitmap),

    BlockDirtyBitmapDisable(BlockDirtyBitmap),

    BlockDirtyBitmapEnable(BlockDirtyBitmap),

    BlockDirtyBitmapMerge {
        node: String,
        target: String,
        bitmaps: Vec<BlockDirtyBitmapOrStr>,
    },

    BlockDirtyBitmapRemove(BlockDirtyBitmap),

    BlockExportAdd(serde_json::Value),

    BlockExportDel {
        id: String,
        #[serde(default)]
        mode: PollableNodeStopMode,
    },

    BlockExportMove {
        id: String,
        #[serde(rename = "node-name")]
        node_name: String,
    },

    BlockJobCancel {
        device: String,
        #[serde(default)]
        force: bool,
    },

    BlockJobComplete {
        device: String,
    },

    BlockJobDismiss {
        id: String,
    },

    BlockJobFinalize {
        id: String,
    },

    BlockJobPause {
        device: String,
    },

    BlockJobResume {
        device: String,
    },

    ChardevAdd(ChardevConfig),

    JobCancel {
        id: String,
    },

    JobComplete {
        id: String,
    },

    JobDismiss {
        id: String,
    },

    JobFinalize {
        id: String,
    },

    JobPause {
        id: String,
    },

    JobResume {
        id: String,
    },

    MonitorAdd(PromptConfig),

    NbdServerStart(nbd::ServerConfig),

    // Marked optional so that 'arguments' becomes optional
    NbdServerStop(Option<NbdServerStopArgs>),

    ObjectAdd(ObjectConfig),

    ObjectDel {
        id: String,
    },

    QomSet {
        path: String,
        property: String,
        value: String,
    },

    QueryBlockJobs,

    QueryJobs,

    ServerStart(ServerConfig),

    ServerStop {
        id: String,
    },

    Quit,

    XDbgDumpBitmap {
        node: String,
        name: String,
        clear: Option<bool>,
    },
}

#[derive(Debug, Default, Deserialize)]
#[serde(default)]
pub struct NbdServerStopArgs {
    pub id: Option<String>,
}

#[derive(Debug, Default, Deserialize)]
pub struct BlockDirtyBitmap {
    pub node: String,
    pub name: String,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum BlockDirtyBitmapOrStr {
    Local(String),
    External(BlockDirtyBitmap),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum MirrorCopyMode {
    Background,
    WriteBlocking,
}

/// QMP command replies
#[derive(Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum Reply {
    Return(serde_json::Value),
    Error(Error),
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct JobInfo {
    id: String,
    #[serde(rename = "type")]
    job_type: JobType,
    status: JobStatus,
    current_progress: u64,
    total_progress: u64,
    #[serde(skip_serializing_if = "Option::is_none")]
    error: Option<String>,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct BlockJobInfo {
    #[serde(rename = "type")]
    job_type: JobType,
    device: String,
    len: u64,
    offset: u64,
    busy: bool,
    paused: bool,
    speed: u64,
    io_status: BlockDeviceIoStatus,
    ready: bool,
    status: JobStatus,
    auto_finalize: bool,
    auto_dismiss: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    error: Option<String>,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
#[allow(dead_code)]
pub enum BlockDeviceIoStatus {
    Ok,
    Failed,
    Nospace,
}

/// QMP error reply
#[derive(Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct Error {
    class: ErrorClass,
    desc: String,
}

/// QMP error classes
#[derive(Debug, Serialize)]
enum ErrorClass {
    GenericError,
}

/// QMP event
#[derive(Clone, Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct Event {
    #[serde(flatten)]
    pub event: Events,

    timestamp: EventTime,
}

/// Enumeration of possible QMP events to occur
#[derive(Clone, Debug, Serialize)]
#[serde(tag = "event", content = "data", rename_all = "SCREAMING_SNAKE_CASE")]
#[allow(clippy::enum_variant_names)]
pub enum Events {
    BackgroundOperationCompleted {
        node_name: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        error: Option<String>,
    },

    BenchmarkDone {
        id: String,
        total: usize,
        duration: f64,
        iops: f64,
    },

    BlockExportDeleted {
        id: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        error: Option<String>,
    },

    BlockIoError {
        device: String,
        node_name: String,
        operation: IoOperationType,
        action: BlockErrorAction,
        nospace: bool,
        reason: String,
    },

    BlockJobCancelled {
        #[serde(rename = "type")]
        job_type: JobType,
        device: String,
        len: u64,
        offset: u64,
        speed: u64,
        // `error` is not part of qemu's definition, but having it will not hurt
        error: String,
    },

    BlockJobCompleted {
        #[serde(rename = "type")]
        job_type: JobType,
        device: String,
        len: u64,
        offset: u64,
        speed: u64,
        #[serde(skip_serializing_if = "Option::is_none")]
        error: Option<String>,
    },

    BlockJobError {
        device: String,
        operation: IoOperationType,
        action: BlockErrorAction,
    },

    BlockJobPending {
        #[serde(rename = "type")]
        job_type: JobType,
        id: String,
    },

    BlockJobReady {
        #[serde(rename = "type")]
        job_type: JobType,
        device: String,
        len: u64,
        offset: u64,
        speed: u64,
    },

    ChardevDeleted {
        id: String,
    },

    JobStatusChange {
        id: String,
        status: JobStatus,
    },

    NodeFaded {
        #[serde(rename = "node-name")]
        node_name: String,
    },

    NodeFadeError {
        #[serde(rename = "node-name")]
        node_name: String,
        error: String,
    },

    PromptCreationError {
        id: String,
        error: String,
    },

    PromptDeleted {
        id: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        error: Option<String>,
    },

    ServerDeleted {
        id: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        error: Option<String>,
    },
}

#[derive(Clone, Copy, Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum IoOperationType {
    Read,
    Write,
    Flush,
    Other,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum BlockErrorAction {
    Ignore,
    Report,
    Stop,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum JobType {
    Backup,
    Mirror,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum JobStatus {
    Undefined,
    Created,
    Running,
    Paused,
    Ready,
    Standby,
    Pending,
    Aborting,
    Concluded,
    Null,
}

/// The timestamp format used in QMP events
#[derive(Clone, Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
struct EventTime {
    seconds: u64,
    microseconds: u64,
}

impl Reply {
    /// Converts the reply to JSON
    pub fn to_json(&self) -> String {
        let mut s = serde_json::to_string(self).unwrap();
        s.push('\n');
        s
    }

    /// Checks whether this reply indicates success
    pub fn is_ok(&self) -> bool {
        !matches!(self, Reply::Error(_))
    }
}

impl<T: Serialize, E: Into<BlockError>> From<Result<T, E>> for Reply {
    /// Construct a QMP reply from a basically an arbitrary `Result<T, E>`
    fn from(result: Result<T, E>) -> Self {
        match result {
            Ok(x) => {
                let value = serde_json::to_value(x).unwrap();
                if matches!(value, serde_json::Value::Null) {
                    // Never return null, always return empty objects
                    Reply::Return(serde_json::json!({}))
                } else {
                    Reply::Return(value)
                }
            }
            Err(e) => Reply::Error(Error::from(e)),
        }
    }
}

impl<E: Into<BlockError>> From<E> for Error {
    /// Construct a QMP error reply from a rsd error
    fn from(err: E) -> Self {
        Error {
            class: ErrorClass::GenericError,
            desc: err.into().into_description(),
        }
    }
}

/// Construct a QMP reply indicating an error from a rsd error
pub fn error<E: Into<BlockError>>(err: E) -> Reply {
    Reply::from(Err::<(), E>(err))
}

/// Construct a QMP reply indicating success from the object to return
pub fn ok<T: Serialize>(obj: T) -> Reply {
    Reply::from(Ok::<T, BlockError>(obj))
}

impl Event {
    /// Creates a new event from the given `Events` variant
    pub fn new(event: Events) -> Self {
        let unix = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap();

        Event {
            event,
            timestamp: EventTime {
                seconds: unix.as_secs(),
                microseconds: unix.subsec_micros() as u64,
            },
        }
    }

    /// Converts the event to JSON
    pub fn to_json(&self) -> String {
        let mut s = serde_json::to_string(self).unwrap();
        s.push('\n');
        s
    }
}

/// Implementation of the `Display` trait so events can be logged to stdout when no prompt is
/// connected to the monitor
impl std::fmt::Display for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}] {}", self.timestamp, self.event)
    }
}

impl std::fmt::Display for EventTime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let duration =
            std::time::Duration::new(self.seconds, (self.microseconds * 1000).try_into().unwrap());
        let time: time::OffsetDateTime = (std::time::UNIX_EPOCH + duration).into();
        write!(
            f,
            "{}-{:02}-{:02} {:02}:{:02}:{:02}.{:06} {}",
            time.year(),
            time.month() as u8,
            time.day(),
            time.hour(),
            time.minute(),
            time.second(),
            time.microsecond(),
            time.offset(),
        )
    }
}

impl std::fmt::Display for Events {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Events::BackgroundOperationCompleted {
                node_name,
                error: None,
            } => {
                write!(
                    f,
                    "background operation on node {} completed successfully",
                    node_name
                )
            }

            Events::BackgroundOperationCompleted {
                node_name,
                error: Some(error),
            } => {
                write!(
                    f,
                    "ERROR: background operation on node {} completed with error: {}",
                    node_name, error
                )
            }

            Events::BenchmarkDone {
                id,
                total,
                duration,
                iops,
            } => {
                write!(
                    f,
                    "benchmark {} done: {} ops in {} s = {} IOPS",
                    id, total, duration, iops
                )
            }

            Events::BlockExportDeleted { id, error: None } => {
                write!(f, "block export {} deleted", id)
            }

            Events::BlockExportDeleted {
                id,
                error: Some(error),
            } => {
                write!(
                    f,
                    "ERROR: block export {} was deleted with error: {}",
                    id, error
                )
            }

            Events::BlockIoError {
                device,
                node_name,
                operation,
                action,
                nospace: _,
                reason,
            } => {
                write!(
                    f,
                    "ERROR: I/O error by device {} on node {} while attempting a {:?} (action taken: {:?}): {}",
                    device, node_name, operation, action, reason
                )
            }

            Events::BlockJobCancelled {
                job_type,
                device,
                len,
                offset,
                speed,
                error,
            } => {
                write!(
                    f,
                    "ERROR: block job {} (type {:?}) has been cancelled ({}/{}, speed={}): {}",
                    device, job_type, offset, len, speed, error
                )
            }

            Events::BlockJobCompleted {
                job_type,
                device,
                len,
                offset,
                speed,
                error: None,
            } => {
                write!(
                    f,
                    "block job {} (type {:?}) has completed ({}/{}, speed={})",
                    device, job_type, offset, len, speed
                )
            }

            Events::BlockJobCompleted {
                job_type,
                device,
                len,
                offset,
                speed,
                error: Some(error),
            } => {
                write!(
                    f,
                    "ERROR: block job {} (type {:?}) has completed ({}/{}, speed={}) with error: {}",
                    device, job_type, offset, len, speed, error
                )
            }

            Events::BlockJobError {
                device,
                operation,
                action,
            } => {
                write!(
                    f,
                    "ERROR: block job {} encountered a {:?} error (action taken: {:?})",
                    device, operation, action
                )
            }

            Events::BlockJobPending { job_type, id } => {
                write!(f, "block job {} (type {:?}) is pending", id, job_type)
            }

            Events::BlockJobReady {
                job_type,
                device,
                len,
                offset,
                speed,
            } => {
                write!(
                    f,
                    "block job {} (type {:?}) is ready ({}/{}, speed={})",
                    device, job_type, offset, len, speed
                )
            }

            Events::ChardevDeleted { id } => {
                write!(f, "character device {} deleted", id)
            }

            Events::JobStatusChange { id, status } => {
                write!(f, "job {} changed status to {:?}", id, status)
            }

            Events::NodeFaded { node_name } => {
                write!(f, "node {} faded", node_name)
            }

            Events::NodeFadeError { node_name, error } => {
                write!(f, "node {} failed to fade: {}", node_name, error)
            }

            Events::PromptCreationError { id, error } => {
                write!(f, "prompt {} creation error: {}", id, error)
            }

            Events::PromptDeleted { id, error: None } => {
                write!(f, "prompt {} deleted", id)
            }

            Events::PromptDeleted {
                id,
                error: Some(error),
            } => {
                write!(f, "ERROR: prompt {} was deleted with error: {}", id, error)
            }

            Events::ServerDeleted { id, error: None } => {
                write!(f, "server {} deleted", id)
            }

            Events::ServerDeleted {
                id,
                error: Some(error),
            } => {
                write!(f, "ERROR: server {} was deleted with error: {}", id, error)
            }
        }
    }
}

impl From<node::JobInfo> for JobInfo {
    fn from(info: node::JobInfo) -> Self {
        JobInfo {
            id: info.id,
            job_type: info.job_type,
            status: info.status,
            current_progress: info.done,
            total_progress: info.done + info.remaining,
            error: info.error,
        }
    }
}

impl From<node::JobInfo> for BlockJobInfo {
    fn from(info: node::JobInfo) -> Self {
        BlockJobInfo {
            job_type: info.job_type,
            device: info.id,
            len: info.done + info.remaining,
            offset: info.done,
            busy: info.busy,
            paused: info.status == JobStatus::Paused || info.status == JobStatus::Standby,
            speed: 0,
            io_status: BlockDeviceIoStatus::Ok, // FIXME
            ready: info.status == JobStatus::Ready || info.status == JobStatus::Standby,
            status: info.status,
            auto_finalize: info.auto_finalize,
            auto_dismiss: info.auto_dismiss,
            error: info.error,
        }
    }
}
