use crate::error::{BlockError, BlockResult};
use crate::helpers::{NetListener, NetReadHalf, NetWriteHalf, SocketAddr};
use crate::monitor::chardev::{ChardevDriverReadHalf, ChardevDriverWriteHalf};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::cell::Cell;
use std::rc::Rc;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::sync::mpsc::{self, Receiver, Sender};

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    addr: SocketAddr,
    #[serde(default)]
    server: bool,
    #[serde(default)]
    wait: bool,
}

#[derive(Default)]
pub struct ReadHalf {
    con_r: Option<NetReadHalf>,

    listener: Rc<Cell<Option<NetListener>>>,
    send_half_to_w: Option<Sender<BlockResult<NetWriteHalf>>>,
    recv_half_from_w: Option<Receiver<BlockResult<NetReadHalf>>>,

    closed: bool,
}

#[derive(Default)]
pub struct WriteHalf {
    con_w: Option<NetWriteHalf>,

    listener: Rc<Cell<Option<NetListener>>>,
    send_half_to_r: Option<Sender<BlockResult<NetReadHalf>>>,
    recv_half_from_r: Option<Receiver<BlockResult<NetWriteHalf>>>,

    closed: bool,
}

pub async fn new(
    opts: Config,
) -> BlockResult<(
    Box<dyn ChardevDriverReadHalf>,
    Box<dyn ChardevDriverWriteHalf>,
)> {
    let (rh, wh) = if opts.server && opts.wait {
        new_server_blocking(opts).await?
    } else if opts.server {
        new_server(opts).await?
    } else {
        new_client(opts).await?
    };

    Ok((Box::new(rh), Box::new(wh)))
}

async fn new_server_blocking(opts: Config) -> BlockResult<(ReadHalf, WriteHalf)> {
    let listener = opts.addr.into_listener().await?;
    let con = listener.await?;
    let (r, w) = con.into_split();

    let rh = ReadHalf {
        con_r: Some(r),
        ..Default::default()
    };

    let wh = WriteHalf {
        con_w: Some(w),
        ..Default::default()
    };

    Ok((rh, wh))
}

async fn new_server(opts: Config) -> BlockResult<(ReadHalf, WriteHalf)> {
    let listener = opts.addr.into_listener().await?;
    let listener = Rc::new(Cell::new(Some(listener)));

    let (send_r, recv_r) = mpsc::channel::<BlockResult<NetReadHalf>>(1);
    let (send_w, recv_w) = mpsc::channel::<BlockResult<NetWriteHalf>>(1);

    let rh = ReadHalf {
        listener: Rc::clone(&listener),
        send_half_to_w: Some(send_w),
        recv_half_from_w: Some(recv_r),
        ..Default::default()
    };

    let wh = WriteHalf {
        listener: Rc::clone(&listener),
        send_half_to_r: Some(send_r),
        recv_half_from_r: Some(recv_w),
        ..Default::default()
    };

    Ok((rh, wh))
}

async fn new_client(opts: Config) -> BlockResult<(ReadHalf, WriteHalf)> {
    let con = opts.addr.connect().await?;
    let (r, w) = con.into_split();

    let rh = ReadHalf {
        con_r: Some(r),
        ..Default::default()
    };

    let wh = WriteHalf {
        con_w: Some(w),
        ..Default::default()
    };

    Ok((rh, wh))
}

#[async_trait(?Send)]
impl ChardevDriverReadHalf for ReadHalf {
    async fn close(&mut self) {
        self.con_r.take();
        self.closed = true;
    }

    fn is_closed(&self) -> bool {
        self.closed
    }

    async fn read_partial(&mut self, buf: &mut [u8]) -> BlockResult<usize> {
        if self.con_r.is_none() {
            if let Some(listener) = self.listener.take() {
                self.recv_half_from_w.take();
                match listener.await {
                    Ok(con) => {
                        let (r, w) = con.into_split();
                        let existing = self.con_r.replace(r);
                        assert!(existing.is_none());
                        self.send_half_to_w
                            .take()
                            .unwrap()
                            .send(Ok(w))
                            .await
                            .ok()
                            .unwrap();
                    }

                    Err(err) => {
                        let err = BlockError::from(err);
                        self.send_half_to_w
                            .take()
                            .unwrap()
                            .send(Err(err.clone()))
                            .await
                            .ok()
                            .unwrap();
                        return Err(err);
                    }
                }
            } else if let Some(mut recv) = self.recv_half_from_w.take() {
                self.send_half_to_w.take();
                let existing = self.con_r.replace(recv.recv().await.unwrap()?);
                assert!(existing.is_none());
            }
        }

        let con_r = self
            .con_r
            .as_mut()
            .ok_or(std::io::ErrorKind::ConnectionAborted)?;

        let count = con_r.read(buf).await?;
        if count == 0 {
            self.closed = true;
        }
        Ok(count)
    }
}

#[async_trait(?Send)]
impl ChardevDriverWriteHalf for WriteHalf {
    async fn close(&mut self) {
        self.con_w.take();
        self.closed = true;
    }

    fn is_closed(&self) -> bool {
        self.closed
    }

    async fn write_all(&mut self, buf: &[u8]) -> BlockResult<()> {
        if self.con_w.is_none() {
            if let Some(listener) = self.listener.take() {
                self.recv_half_from_r.take();
                match listener.await {
                    Ok(con) => {
                        let (r, w) = con.into_split();
                        let existing = self.con_w.replace(w);
                        assert!(existing.is_none());
                        self.send_half_to_r
                            .take()
                            .unwrap()
                            .send(Ok(r))
                            .await
                            .ok()
                            .unwrap();
                    }

                    Err(err) => {
                        let err = BlockError::from(err);
                        self.send_half_to_r
                            .take()
                            .unwrap()
                            .send(Err(err.clone()))
                            .await
                            .ok()
                            .unwrap();
                        return Err(err);
                    }
                }
            } else if let Some(mut recv) = self.recv_half_from_r.take() {
                self.send_half_to_r.take();
                let existing = self.con_w.replace(recv.recv().await.unwrap()?);
                assert!(existing.is_none());
            }
        }

        let con_w = self
            .con_w
            .as_mut()
            .ok_or(std::io::ErrorKind::ConnectionAborted)?;

        con_w.write_all(buf).await?;
        Ok(())
    }
}
