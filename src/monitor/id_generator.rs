use rand::Rng;
use std::collections::HashMap;

#[derive(Default)]
pub struct IdGenerator {
    ids: HashMap<String, ()>,
}

impl IdGenerator {
    pub fn generate(&mut self, prefix: &str) -> String {
        // Use at least three digits, but more if we have used half the available ID space
        let mut range = 1000;
        let mut digits = 3;
        while range / 2 < self.ids.len() {
            range *= 10;
            digits += 1;
        }

        let mut rng = rand::thread_rng();

        loop {
            let rand = rng.gen_range(0..range);
            let id = format!("{}{:0digits$}", prefix, rand, digits = digits);

            if self.ids.insert(id.clone(), ()).is_none() {
                return id;
            }
        }
    }
}
