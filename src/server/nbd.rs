use crate::error::BlockResult;
use crate::helpers::nbd::*;
use crate::helpers::{BlockFutureResult, FlatSize, NetListener, NetStream, SocketAddr};
use crate::monitor;
use crate::node::nbd_export::Export;
use crate::server::{ServerDriverData, ServerNodeData};
use bincode::Options;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, LinkedList};
use std::future::Future;
use std::pin::Pin;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::sync::mpsc;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct ServerConfig {
    addr: SocketAddr,

    iothread: Option<String>,
}

struct Server {
    exports: Arc<Mutex<HashMap<String, Arc<Export>>>>,
    listener: NetListener,

    handshake_connections: LinkedList<HandshakeConnection>,

    quit: mpsc::Receiver<()>,
}

pub struct ServerData {
    exports: Arc<Mutex<HashMap<String, Arc<Export>>>>,
    quit: mpsc::Sender<()>,
    /// Once `exports` becomes empty, send a message through here
    exports_empty: mpsc::UnboundedSender<()>,
}

#[derive(Debug)]
pub struct NodeData {
    export: Arc<Export>,
}

/// Returned by HandshakeParams::handle_client_opt()
#[derive(Default)]
struct HandleClientOptReturn {
    drop_connection: bool,
    export: Option<Arc<Export>>,
}

struct HandshakeConnection {
    future: BlockFutureResult<'static, ()>,
}

pub async fn create_server(
    opts: ServerConfig,
) -> BlockResult<(BlockFutureResult<'static, ()>, Box<dyn ServerDriverData>)> {
    let listener = opts.addr.into_listener().await?;
    let (quit_s, quit_r) = mpsc::channel::<()>(1);
    let exports = Arc::new(Mutex::new(HashMap::new()));
    // Whenever the exports list becomes empty, a message is put in here.  The server waits for
    // messages only when it wants to quit and waits for the list to become empty, which is why the
    // channel needs to be unbounded.
    let (exports_empty_s, mut exports_empty_r) = mpsc::unbounded_channel::<()>();

    let srv_exp = ServerData {
        exports: Arc::clone(&exports),
        quit: quit_s,
        exports_empty: exports_empty_s,
    };

    let thread = monitor::monitor().get_thread_from_opt(&opts.iothread)?;
    let fut = Box::pin(async move {
        let result = thread
            .owned_run({
                let exports = Arc::clone(&exports);
                move || {
                    Box::pin(Server {
                        exports,
                        listener,
                        handshake_connections: Default::default(),
                        quit: quit_r,
                    })
                }
            })
            .await;

        // Make sure all exports know that we need to quit
        for exp in exports.lock().unwrap().values() {
            exp.quit();
        }

        // If there are still exports around, wait for them to stop
        // (This is a loop because the list may have become empty multiple times before refilling,
        // and each time `ServerData::remove_export()` will have sent a message through this
        // channel.)
        while !exports.lock().unwrap().is_empty() {
            let _: Option<()> = exports_empty_r.recv().await;
        }

        result
    });

    Ok((fut, Box::new(srv_exp)))
}

impl ServerDriverData for ServerData {
    fn add_node(&self, node_name: &str, node_data: ServerNodeData) -> BlockResult<()> {
        let node_data: NodeData = node_data.try_into()?;
        let existing = self
            .exports
            .lock()
            .unwrap()
            .insert(String::from(node_name), node_data.export);
        assert!(existing.is_none());
        Ok(())
    }

    fn stop(&mut self) {
        let _: Result<(), _> = self.quit.try_send(());
    }

    fn remove_node(&self, node_name: &str) -> BlockResult<()> {
        let mut exports = self.exports.lock().unwrap();
        if exports.remove(node_name).is_none() {
            return Err(format!("No node \"{}\" at this server", node_name).into());
        }
        if exports.is_empty() {
            let _: Result<_, _> = self.exports_empty.send(());
        }

        Ok(())
    }
}

impl Future for Server {
    type Output = BlockResult<()>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.quit.poll_recv(cx).is_ready() {
            // Close all connections
            for exp in self.exports.lock().unwrap().values() {
                exp.quit();
            }
            self.handshake_connections.clear();
            return Poll::Ready(Ok(()));
        }

        self.poll_listener(cx);
        self.poll_handshake_connections(cx);

        // Forever pending, because we are always at least waiting for new connections
        Poll::Pending
    }
}

impl Server {
    fn poll_listener(&mut self, cx: &mut Context<'_>) {
        match self.listener.poll_accept(cx) {
            Poll::Ready(Ok(socket)) => {
                let exports = Arc::clone(&self.exports);
                self.handshake_connections
                    .push_back(HandshakeConnection::new(socket, exports));
            }

            Poll::Ready(Err(err)) => {
                eprintln!("NBD accept error: {}", err);
            }

            Poll::Pending => (),
        }
    }

    fn poll_handshake_connections(&mut self, cx: &mut Context<'_>) {
        let mut con_cursor = self.handshake_connections.cursor_front_mut();
        loop {
            let result = {
                match con_cursor.current() {
                    Some(con) => Future::poll(con.future.as_mut(), cx),
                    None => break,
                }
            };

            match result {
                Poll::Ready(Ok(())) => {
                    con_cursor.remove_current();
                }
                Poll::Ready(Err(err)) => {
                    eprintln!("NBD client error: {}", err);
                    con_cursor.remove_current();
                }
                Poll::Pending => con_cursor.move_next(),
            }
        }
    }
}

struct HandshakeParams {
    con: NetStream,
    exports: Arc<Mutex<HashMap<String, Arc<Export>>>>,
}

impl HandshakeConnection {
    fn new(con: NetStream, exports: Arc<Mutex<HashMap<String, Arc<Export>>>>) -> Self {
        HandshakeConnection {
            future: Box::pin(Self::worker(con, exports)),
        }
    }

    async fn worker(
        con: NetStream,
        exports: Arc<Mutex<HashMap<String, Arc<Export>>>>,
    ) -> BlockResult<()> {
        let (con, export) = {
            let hsp = HandshakeParams { con, exports };

            match hsp.handshake_phase().await? {
                Some(x) => x,
                None => return Ok(()),
            }
        };

        export.add_connection(con);

        Ok(())
    }
}

impl HandshakeParams {
    async fn handshake_phase(mut self) -> BlockResult<Option<(NetStream, Arc<Export>)>> {
        let bincode = bincode::DefaultOptions::new()
            .with_fixint_encoding()
            .with_big_endian();

        let handshake_buf =
            bincode.serialize(&NbdServerHandshake::new(NbdServerHandshake::FIXED_NEWSTYLE))?;
        self.con.write_all(&handshake_buf).await?;

        let client_flags: NbdClientFlags = {
            let mut client_flags_buf = vec![0u8; NbdClientFlags::SIZE];
            self.con.read_exact(&mut client_flags_buf).await?;
            bincode.deserialize(&client_flags_buf)?
        };

        client_flags.check()?;

        loop {
            let opt_ret = self.handle_client_opt().await?;
            if opt_ret.drop_connection {
                return Ok(None);
            }
            if let Some(export) = opt_ret.export {
                return Ok(Some((self.con, export)));
            }
        }
    }

    async fn handle_client_opt(&mut self) -> BlockResult<HandleClientOptReturn> {
        let bincode = bincode::DefaultOptions::new()
            .with_fixint_encoding()
            .with_big_endian();

        let (client_opt, client_opt_data) = {
            let mut client_opt_buf = vec![0u8; NbdClientOpt::SIZE];
            self.con.read_exact(&mut client_opt_buf).await?;
            let client_opt: NbdClientOpt = bincode.deserialize(&client_opt_buf)?;

            client_opt.check()?;

            let mut client_opt_data = vec![0u8; client_opt.data_length.try_into()?];
            if client_opt.data_length > 0 {
                self.con.read_exact(&mut client_opt_data).await?;
            }

            (client_opt, client_opt_data)
        };

        let opt: NbdOpt = match client_opt.option.try_into() {
            Ok(opt) => opt,
            Err(_) => {
                println!("DEBUG: Unknown NBD client option {}", client_opt.option);
                self.reply(&client_opt, NbdReplyType::ErrUnsup).await?;
                return Ok(Default::default());
            }
        };

        let ret = match opt {
            NbdOpt::Abort => {
                self.reply(&client_opt, NbdReplyType::Ack).await?;
                HandleClientOptReturn {
                    drop_connection: true,
                    ..Default::default()
                }
            }

            NbdOpt::List => {
                let exports: Vec<String> = self
                    .exports
                    .lock()
                    .unwrap()
                    .values()
                    .map(|export| String::from(export.name()))
                    .collect();

                for export in exports {
                    let mut name = Vec::from(export.as_bytes());
                    let name_len = bincode.serialize(&(name.len() as u32))?;

                    let mut name_and_len = name_len;
                    name_and_len.append(&mut name);

                    self.reply_data(&client_opt, NbdReplyType::Server, Some(&name_and_len))
                        .await?;
                }

                self.reply(&client_opt, NbdReplyType::Ack).await?;
                Default::default()
            }

            NbdOpt::Info | NbdOpt::Go => {
                let name = NbdClientOpt::export_name(&client_opt_data)?;
                let export = self
                    .exports
                    .lock()
                    .unwrap()
                    .values()
                    .find(|export| export.name() == name)
                    .cloned();

                if let Some(export) = export {
                    let nbd_info_buf = bincode.serialize(&export.nbd_info())?;
                    self.reply_data(&client_opt, NbdReplyType::Info, Some(&nbd_info_buf))
                        .await?;

                    let nbd_info_buf = bincode.serialize(&export.nbd_info_block_size())?;
                    self.reply_data(&client_opt, NbdReplyType::Info, Some(&nbd_info_buf))
                        .await?;

                    self.reply(&client_opt, NbdReplyType::Ack).await?;

                    if opt == NbdOpt::Go {
                        HandleClientOptReturn {
                            export: Some(export),
                            ..Default::default()
                        }
                    } else {
                        Default::default()
                    }
                } else {
                    self.reply(&client_opt, NbdReplyType::ErrUnknown).await?;
                    Default::default()
                }
            }

            NbdOpt::StructuredReply => {
                self.reply(&client_opt, NbdReplyType::ErrUnsup).await?;
                Default::default()
            }
        };

        Ok(ret)
    }

    async fn reply_data(
        &mut self,
        client_opt: &NbdClientOpt,
        reply_type: NbdReplyType,
        data: Option<&Vec<u8>>,
    ) -> BlockResult<()> {
        let bincode = bincode::DefaultOptions::new()
            .with_fixint_encoding()
            .with_big_endian();

        let data_length = match &data {
            Some(data) => data.len().try_into()?,
            None => 0,
        };
        let reply = NbdServerReply::new(client_opt.option, reply_type, data_length);
        let reply_buf = bincode.serialize(&reply)?;
        self.con.write_all(&reply_buf).await?;

        if let Some(data) = data {
            self.con.write_all(data).await?;
        }

        Ok(())
    }

    async fn reply(
        &mut self,
        client_opt: &NbdClientOpt,
        reply_type: NbdReplyType,
    ) -> BlockResult<()> {
        self.reply_data(client_opt, reply_type, None).await
    }
}

impl NodeData {
    pub fn new(export: Arc<Export>) -> Self {
        NodeData { export }
    }
}
