use crate::error::BlockResult;
use crate::helpers::{self, BlockFutureResult, MainThreadOnlyMarker};
use crate::{monitor, splittable_enum};
use serde::{Deserialize, Serialize};
use std::future::Future;
use std::task::{Context, Poll};

pub mod nbd;

pub struct Server {
    pub id: String,
    driver: Box<dyn ServerDriverData>,
    future: BlockFutureResult<'static, ()>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct ServerConfig {
    id: String,

    #[serde(flatten)]
    driver: ServerDriverConfig,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type", rename_all = "kebab-case")]
pub enum ServerDriverConfig {
    Nbd(nbd::ServerConfig),
}

/// Pollable nodes that are bound to servers announce this via creating this struct, which, on
/// drop, will automatically detach the node from the server
pub struct ServerNode {
    node_name: String,
    server: String,

    _main_thread: MainThreadOnlyMarker,
}

splittable_enum! {
    #[derive(Debug)]
    pub enum ServerNodeData {
        Nbd(nbd::NodeData),
    }
}

impl Server {
    pub async fn new(opts: ServerConfig) -> BlockResult<Server> {
        let (future, driver_data): (BlockFutureResult<'static, ()>, Box<dyn ServerDriverData>) =
            match opts.driver {
                ServerDriverConfig::Nbd(o) => nbd::create_server(o).await?,
            };

        Ok(Server {
            id: opts.id,
            driver: driver_data,
            future,
        })
    }

    pub fn poll(&mut self, cx: &mut Context<'_>) -> Poll<BlockResult<()>> {
        Future::poll(self.future.as_mut(), cx)
    }
}

pub trait ServerDriverData {
    /// Attempt to add the given pollable node to the server.  Return an error if the type does not
    /// match.
    fn add_node(&self, node_name: &str, node_data: ServerNodeData) -> BlockResult<()>;

    /// Remove the pollable node with the given name from the server; this is generally invoked
    /// some way or another when the node is dropped, i.e., the server need not take care to remove
    /// the node itself.
    fn remove_node(&self, node_name: &str) -> BlockResult<()>;

    /// Stop the server, i.e. stop all exported nodes, drop all connections, and have the server
    /// future return `Poll::Ready(_)`
    fn stop(&mut self);
}

impl ServerDriverData for Server {
    fn add_node(&self, node_name: &str, node_data: ServerNodeData) -> BlockResult<()> {
        self.driver.add_node(node_name, node_data)
    }

    fn remove_node(&self, node_name: &str) -> BlockResult<()> {
        self.driver.remove_node(node_name)
    }

    fn stop(&mut self) {
        self.driver.stop()
    }
}

impl From<nbd::ServerConfig> for ServerConfig {
    fn from(nbd: nbd::ServerConfig) -> Self {
        ServerConfig {
            id: String::from(helpers::nbd::DEFAULT_SERVER_NAME),
            driver: ServerDriverConfig::Nbd(nbd),
        }
    }
}

impl ServerNode {
    pub fn new<D: Into<ServerNodeData>>(
        server: &str,
        node_name: &str,
        node_data: D,
    ) -> BlockResult<Self> {
        let node_data: ServerNodeData = node_data.into();
        monitor::monitor().add_node_to_server(server, node_name, node_data)?;
        Ok(ServerNode {
            node_name: String::from(node_name),
            server: String::from(server),
            _main_thread: Default::default(),
        })
    }
}

impl Drop for ServerNode {
    fn drop(&mut self) {
        monitor::monitor()
            .remove_node_from_server(&self.server, &self.node_name)
            .unwrap();
    }
}
