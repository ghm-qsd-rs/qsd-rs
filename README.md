RSD
===

![RSD logo](logo.svg)

RSD (Rust Storage Daemon) is a yet very incomplete reimplementation of the QEMU
block layer, as provided by the qemu-storage-daemon (QSD), in Rust.  It allows
management of VM images, providing virtio-blk devices that can be connected to
QEMU instances via the vhost-user protocol.

Its interface is very similar to that of QSD: VM images are managed through a
block graph, with block nodes forming the basic building blocks.  Runtime
control is provided through an implementation of the QEMU Monitor Protocol
(QMP).

Notable Differences to QSD
--------------------------

RSD behaves in some ways differently from QSD:

* Every node requires an explicit node-name.  They are not auto-generated.
* Child nodes that are defined inline in `--blockdev` are not automatically
  deleted when their parent node is deleted.  (Issue #28)
* There is no “dotted key” syntax.  All parameter values on the command line
  must be specified as JSON objects.  (Issue #23)
* Nodes generally cannot be put into I/O threads.  Instead, nodes can be
  accessed from all threads simultaneously, and each export can use their own
  thread.  Therefore, thread configuration is only done in export nodes.

Running RSD
-----------

RSD will run until a `quit` command is issued through QMP, or once there is no
more activity, i.e.:
* No QMP monitor is connected
* There is no background activity on any block node, i.e. there are no active
  block jobs or exports
* There is no (NBD) server

Sending an interrupt signal to RSD (Ctrl-C) is functionally the same as issuing
the QMP `quit` command.

### Command-Line Options

Running RSD with the `--help` option will print all available command-line
options along with a description.  Besides `--help`, all other options create
internal objects, and each take a JSON object that provides the necessary
parameters.  Every such option corresponds to a QMP `*-add` or `*-start`
command, and using the option is indeed functionally equivalent to using the QMP
command (e.g.  using `--blockdev` will have RSD run the QMP `blockdev-add`
command internally).

Here is an example command line for exporting a qcow2 VM image (“vm.qcow2”)
using a virtio-blk device over vhost-user, using four queues, two of which
share an I/O thread:
```
$ rsd \
    --chardev '{"id": "chr0", "backend": {"type": "stdio", "data": {}}}' \
    --monitor '{"id": "mon0", "chardev": "chr0"}' \
    --blockdev '{"node-name": "node0", "driver": "file", "filename": "vm.qcow2"}' \
    --blockdev '{"node-name": "node1", "driver": "qcow2", "file": "node0"}' \
    --object '{"qom-type": "iothread", "id": "thr0"}' \
    --object '{"qom-type": "iothread", "id": "thr1"}' \
    --object '{"qom-type": "iothread", "id": "thr2"}' \
    --blockdev '{
        "node-name": "export",
        "driver": "vhost-user-blk-export",
        "exported": "node1",
        "addr": {
            "type": "unix",
            "path": "/tmp/rsd-vhost.sock"
        },
        "writable": true,
        "num-queues": 4,
        "iothreads": ["thr0", "thr1", "thr2", "thr2"]
    }'
```

QEMU can be connected to this RSD instance for example as follows:
```
$ qemu-system-x86_64 \
    -m 4g \
    --object memory-backend-file,id=mem,size=4G,mem-path=/dev/shm,share=on \
    --smp 8 \
    --numa node,memdev=mem \
    --accel kvm \
    --chardev socket,id=qsdsock0,path=/tmp/rsd-vhost.sock \
    --device vhost-user-blk-pci,queue-size=1024,chardev=qsdsock0,id=qsd,num-queues=4
```

QEMU Monitor Protocol
---------------------

See QEMU’s documentation for detailed information on how QMP works
(https://www.qemu.org/docs/master/interop/qmp-spec.html).

RSD supports the following QEMU commands, although some parameters may not be
supported yet:
* `qmp_capabilities`
* `quit`
* `blockdev-add`
* `blockdev-del`
* `blockdev-reopen`
* `block-export-add`
* `block-export-del`
* `nbd-server-start`
* `nbd-server-stop`
* `blockdev-backup`
* `blockdev-mirror`
* `block-job-cancel`, `job-cancel`
* `block-job-complete`, `job-complete`
* `block-job-dismiss`, `job-dismiss`
* `block-job-finalize`, `job-finalize`
* `block-job-pause`, `job-pause`
* `block-job-resume`, `job-resume`
* `query-block-jobs`, `query-jobs`
* `block-dirty-bitmap-add`
* `block-dirty-bitmap-clear`
* `block-dirty-bitmap-disable`
* `block-dirty-bitmap-enable`
* `block-dirty-bitmap-merge`
* `block-dirty-bitmap-remove`
* `chardev-add`
* `object-add`
* `object-del`

In addition, there are some RSD-specific commands:
* `server-start` (`id`, `type`, ...): More general version of `nbd-server-start`
* `server-stop` (`id`): More general version of `nbd-server-stop`
* `blockdev-pause` (`node-name`): Pauses background operations issued by a node,
  e.g. block job nodes or export nodes (`job-pause` is an alias for this)
* `blockdev-resume` (`node-name`): Resumes background operations issued by a
  node (`job-resume` is an alias for this)
* `blockdev-stop` (`node-name`, `mode`): Fully stops the background operation
  run on a node (`job-cancel`, `job-complete`, and `block-export-del` are
  aliases for this)
* `block-export-move` (`id`, `node-name`): Change an export’s exported node (can
  also be done using `blockdev-reopen`)
* `monitor-add` (`id`, `chardev`): Equivalent to the command line `--monitor`
  option
