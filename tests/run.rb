#!/usr/bin/ruby

require 'json'
require 'shellwords'
require 'tmpdir'

if ENV['VERBOSE']
    $verbose = true
else
    $verbose = false
end

$all_counter = 0
$pass_counter = 0
$fail_counter = 0
$skip_counter = 0

$failed_tests = []
$skipped_tests = []

def run_test(test_file, interpreter, test_info, to_run)
    args = test_info['args']
    if !args
        args = []
    end

    if args.empty?
        args = [[]]
    else
        args = args[0].product(*args[1..])
    end

    if to_run && to_run.length > 1
        args = [to_run[1..-1]]
    end

    if !$verbose
        runs = args.length
        progress_fmt = "%#{(Math.log10(args.length) + 1).to_i}i"
        progress_line = ''
        $stdout.write("#{test_file} (#{progress_fmt % 0}/#{runs}): #{progress_line}")
        $stdout.flush
    end

    args.each.with_index do |args, i|
        $all_counter += 1

        if $verbose
            puts("\033[1mRunning\033[0m: #{test_file} #{args.map { |arg| arg.shellescape } * ' '}")
        end

        stdout_r, stdout_w = $verbose ? [nil, nil] : IO.pipe()
        stderr_r, stderr_w = $verbose ? [nil, nil] : IO.pipe()

        sock_tmpdir = Dir.mktmpdir('rsd-sock')
        imgs_tmpdir = Dir.mktmpdir('rsd-imgs')
        ENV['SOCK_DIR'] = sock_tmpdir
        ENV['IMGS_DIR'] = imgs_tmpdir

        begin
            pid = fork
            if !pid
                STDOUT.reopen(stdout_w) if stdout_w
                STDERR.reopen(stderr_w) if stderr_w

                exec(interpreter, test_file, *args)
            end

            Process.waitpid(pid)
            result = $?
        rescue Interrupt => e # Catch Ctrl-C
            system("rm -rf #{sock_tmpdir.shellescape}")
            system("rm -rf #{imgs_tmpdir.shellescape}")

            if $fail_counter > 0
                exit 1
            else
                exit 0
            end
        end

        system("rm -rf #{sock_tmpdir.shellescape}")
        system("rm -rf #{imgs_tmpdir.shellescape}")

        stdout_w.close if stdout_w
        stderr_w.close if stderr_w

        if result.success?
            $pass_counter += 1
        elsif result.exitstatus == 2
            $skip_counter += 1
        else
            $fail_counter += 1
        end

        if result.success?
            if $verbose
                puts("\033[32;1mSUCCESS\033[0m")
                puts
            else
                progress_line += '.'
            end
        elsif result.exitstatus == 2
            if $verbose
                puts("\033[1mSKIP\033[0m")
                puts
            else
                skip_msg = stdout_r.read.split($/)
                while skip_msg.shift.strip != '---'
                end
                $skipped_tests << {
                    test: test_file,
                    args: args,
                    skip_msg: skip_msg * $/,
                }
                progress_line += "\033[1ms\033[0m"
            end
        else
            if $verbose
                puts("\033[31;1mFAILURE\033[0m")
                puts
            else
                $failed_tests << {
                    test: test_file,
                    args: args,
                    stdout: stdout_r.read_nonblock(65536),
                    stderr: stderr_r.read_nonblock(65536),
                }
                progress_line += "\033[31;1mF\033[0m"
            end
        end
        if !$verbose
            $stdout.write("\r#{test_file} (#{progress_fmt % (i + 1)}/#{runs}): #{progress_line}")
            $stdout.flush
        end
    end

    if !$verbose
        puts
    end
end

not_to_run = []
to_run = []

skip_next = false
ARGV.each do |arg|
    if arg == '-s'
        if skip_next
            $stderr.puts('Cannot use -s after -s')
            exit 1
        end
        skip_next = true
        next
    end

    if skip_next
        not_to_run << arg
        skip_next = false
    else
        to_run << arg
    end
end

if to_run.empty?
    to_run = nil
end

test_dir = File.realpath(File.dirname(__FILE__))

ENV['RSD_RB'] = test_dir + '/tests/lib/rsd.rb'

if !ENV['RSD']
    ENV['RSD'] = File.realpath(test_dir + '/../target/debug/rsd')
elsif ENV['RSD'].include?('/')
    ENV['RSD'] = File.realpath(ENV['RSD'])
end

if !ENV['QEMU_IMG']
    ENV['QEMU_IMG'] = 'qemu-img'
end

Dir.chdir(test_dir + '/tests')

Dir.entries('.').reject { |e|
    e[0] == '.'
}.select { |e|
    File.file?(e)
}.each { |test|
    if (to_run && to_run[0] != test) || not_to_run.include?(test)
        next
    end

    lines = IO.readlines(test)
    shebang = lines.shift
    if !shebang.start_with?('#!')
        next
    end
    interpreter = shebang.delete_prefix('#!').strip

    test_info = nil
    while true
        line = lines.shift.strip
        if line[0] == '#'
            line = line[1..].strip
            if line.start_with?('test:')
                test_info = line.delete_prefix('test:')
                break
            end
        elsif !line.empty?
            break
        end
    end

    if !test_info
        next
    end

    while lines[0].strip[0] == '#'
        test_info += lines.shift.strip[1..]
    end

    test_info = JSON.parse(test_info)

    run_test(test, interpreter, test_info, to_run)
}

if $all_counter > 0
    puts()
    puts()
end

if $fail_counter == 0 && $skip_counter == 0
    puts("\033[32;1m#{$pass_counter}/#{$all_counter} PASSED\033[0m")
elsif $fail_counter == 0 && $skip_counter > 0
    puts("\033[32;1m#{$pass_counter}/#{$all_counter} PASSED\033[0m, \033[1m#{$skip_counter}/#{$all_counter} SKIPPED\033[0m")
elsif $fail_counter > 0 && $skip_counter == 0
    puts("\033[31;1m#{$fail_counter}/#{$all_counter} FAILED\033[0m, #{$pass_counter}/#{$all_counter} PASSED")
else
    puts("\033[31;1m#{$fail_counter}/#{$all_counter} FAILED\033[0m, #{$pass_counter}/#{$all_counter} PASSED, \033[1m#{$skip_counter}/#{$all_counter} SKIPPED\033[0m")
end

if $skip_counter > 0 || $fail_counter > 0
    puts('---')
end

$skipped_tests.each { |test|
    puts("\033[1mSkipped\033[0m: #{test[:test]} #{test[:args].map { |arg| arg.shellescape } * ' '}")
    puts(test[:skip_msg])
    puts
}

$failed_tests.each { |test|
    puts("\033[31;1mFailed\033[0m: #{test[:test]} #{test[:args].map { |arg| arg.shellescape } * ' '}")
    puts("\033[1mstdout\033[0m:")
    puts(test[:stdout])
    puts
    puts("\033[1mstderr\033[0m:")
    puts(test[:stderr])
    puts
}

if $fail_counter > 0
    exit 1
else
    exit 0
end
