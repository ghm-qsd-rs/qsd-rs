#!/usr/bin/ruby

# Test adding a copy node, and simultaneously an NBD node on top; but the NBD
# configuration is faulty, so the copy node must be dropped again.

# test: {
#     "args": [
#     ]
# }

require ENV['RSD_RB']

rsd = RSD.new(ENV['RSD'])

# NBD node with a non-existing NBD server, that should fail
nbd_node = {
    node_name: 'nbd',
    driver: 'nbd-export',
    server: 'nbd-server',
    name: 'some-export',
    writable: true,
}
copy_node = {
    node_name: 'copy',
    driver: 'copy',
    job_id: 'copy',
    mode: 'lazy',
    auto_finalize: false,
}
source_node = {
    node_name: 'source',
    driver: 'null',
    no_op: true,
    size: 1024 * 1024 * 1024,
}
target_node = {
    node_name: 'target',
    driver: 'null',
    no_op: true,
    size: 1024 * 1024 * 1024,
}

# Expect the blockdev-add to fail gracefully
begin
    rsd.qmp.blockdev_add({
        **nbd_node,
        exported: {
            **copy_node,
            active: true,
            file: source_node,
            target: target_node,
        },
    })
rescue QMPError => e
    if e.object['error']['desc'] != 'Server "nbd-server" not found'
        $stderr.puts('Unexpected error message from blockdev-add')
        exit 1
    end
end

while rsd.qmp.event_wait('JOB_STATUS_CHANGE')['data']['status'] != 'null'
end

rsd.qmp.quit
rsd.wait
