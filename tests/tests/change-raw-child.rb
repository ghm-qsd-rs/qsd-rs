#!/usr/bin/ruby

# Create an export on top of a raw node, on top of a protocol node, then run a
# writer on the raw node for some time[1] before changing the raw node's `file`
# child.  Check that both files have been modified.
# [1] First, we run a benchmark to see how long writing the whole file takes,
#     then we switch the node at half that time.

# test: {
#     "args": [
#         ["bench", "nbd", "vhost-user-blk"],
#         ["blkio", "threads"]
#     ]
# }

require ENV['RSD_RB']
require 'shellwords'

$nbd_url = nil

def add_export(rsd, type, id, node, mb_count)
    case type
    when 'bench'
        rsd.qmp.block_export_add({
            id: id,
            type: 'bench',
            node_name: node,
            request_type: 'write',
            total: mb_count * 1024 * 1024 / 4096,
        })
    when 'nbd'
        rsd.qmp.server_start({
            type: 'nbd',
            id: 'nbd-server',
            addr: {
                type: 'unix',
                path: ENV['SOCK_DIR'] + '/nbd.sock',
            },
        })

        rsd.qmp.block_export_add({
            id: id,
            type: 'nbd',
            server: 'nbd-server',
            node_name: node,
            writable: true,
        })

        if !fork
            exec(ENV['QEMU_IMG'], 'bench', '-f', 'nbd', '-w',
                 '-c', (mb_count * 1024 * 1024 / 4096).to_s,
                 "nbd+unix:///#{node}?socket=#{ENV['SOCK_DIR']}/nbd.sock")
        end
    when 'vhost-user-blk'
        error = `#{ENV['QEMU_IMG'].shellescape} bench --image-opts driver=virtio-blk-vhost-user 2>&1`
        if error.include?('Unknown driver')
            puts '---'
            puts 'qemu-img lacks virtio-blk-vhost-user support, try pointing $QEMU_IMG to a build that has it'
            exit 2
        end

        rsd.qmp.block_export_add({
            id: id,
            type: 'vhost-user-blk',
            node_name: node,
            writable: true,
            addr: {
                type: 'unix',
                path: ENV['SOCK_DIR'] + '/vhost-user-blk.sock',
            },
        })

        if !fork
            exec(ENV['QEMU_IMG'], 'bench', '--image-opts', '-w',
                 '-c', (mb_count * 1024 * 1024 / 4096).to_s,
                 "driver=virtio-blk-vhost-user,path=#{ENV['SOCK_DIR']}/vhost-user-blk.sock,cache.direct=on")
        end
    else
        $stderr.puts("Bad export type '#{export_type}'")
        exit 1
    end
end

def wait_export(rsd, type, id)
    case type
    when 'bench'
        rsd.qmp.event_wait('BLOCK_EXPORT_DELETED')
    when 'nbd'
        Process.wait

        rsd.qmp.server_stop({ id: 'nbd-server' })

        export_deleted = false
        server_deleted = false
        while !(export_deleted && server_deleted)
            case rsd.qmp.event_wait()['event']
            when 'BLOCK_EXPORT_DELETED'
                export_deleted = true
            when 'SERVER_DELETED'
                server_deleted = true
            end
        end
    when 'vhost-user-blk'
        Process.wait
        rsd.qmp.block_export_del({ id: id })
        rsd.qmp.event_wait('BLOCK_EXPORT_DELETED')
    else
        $stderr.puts("Bad export type '#{export_type}'")
        exit 1
    end
end

export_type = ARGV[0]
protocol_driver = ARGV[1]

rsd = RSD.new(ENV['RSD'])

template = "#{ENV['IMGS_DIR']}/template.img"
test1 = "#{ENV['IMGS_DIR']}/test1.img"
test2 = "#{ENV['IMGS_DIR']}/test2.img"

system("truncate -s 256M #{template.shellescape}")
system("qemu-io -f raw -c #{'write -P 0xff 0 256M'.shellescape} #{template.shellescape}")

system("cp #{template.shellescape} #{test1.shellescape}")

rsd.qmp.blockdev_add({
    node_name: 'fmt',
    driver: 'raw',
    file: {
        node_name: 'test1',
        driver: 'file',
        aio: protocol_driver,
        filename: test1,
    },
})

add_export(rsd, export_type, 'exp0', 'fmt', 256)
start = Time.now
wait_export(rsd, export_type, 'exp0')
stop = Time.now

full_duration = stop - start

rsd.qmp.blockdev_del({
    node_name: 'fmt',
})
rsd.qmp.blockdev_del({
    node_name: 'test1',
})

system("cp #{template.shellescape} #{test1.shellescape}")
system("cp #{template.shellescape} #{test2.shellescape}")

rsd.qmp.blockdev_add({
    node_name: 'fmt',
    driver: 'raw',
    file: {
        node_name: 'test1',
        driver: 'file',
        aio: protocol_driver,
        filename: test1,
    },
})

rsd.qmp.blockdev_add({
    node_name: 'test2',
    driver: 'file',
    aio: protocol_driver,
    filename: test2,
})

add_export(rsd, export_type, 'exp0', 'fmt', 256)

sleep(full_duration * 0.5)

rsd.qmp.blockdev_reopen({
    options: [
        {
            node_name: 'fmt',
            driver: 'raw',
            file: 'test2',
        },
    ],
})

wait_export(rsd, export_type, 'exp0')

result = `#{ENV['QEMU_IMG'].shellescape} compare -f raw -F raw #{template.shellescape} #{test1.shellescape}`
if !result.start_with?('Content mismatch')
    $stderr.puts("test1.img was not modified")
    exit 1
end
offset = Integer(result.sub(/.*offset ([0-9]*).*/, '\1').strip)
if offset != 0
    $stderr.puts("Expected test1.img to be modified from offset 0")
    exit 1
end

result = `#{ENV['QEMU_IMG'].shellescape} compare -f raw -F raw #{template.shellescape} #{test2.shellescape}`
if !result.start_with?('Content mismatch')
    $stderr.puts("test2.img was not modified")
    exit 1
end
offset = Integer(result.sub(/.*offset ([0-9]*).*/, '\1').strip)
if offset == 0
    $stderr.puts("Expected test2.img to not be modified from offset 0")
    exit 1
end

rsd.qmp.quit
rsd.wait
