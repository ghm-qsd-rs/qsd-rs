#!/usr/bin/ruby

# Run CBW copy job with benchmark export on top.  Check the result matches the original.

# test: {
#     "args": [
#     ]
# }

require ENV['RSD_RB']

def random_ops(r, count, node_size)
    count.times.map { |_|
        ofs = (r.rand * node_size).to_i
        len = (r.rand * 1024 * 1024).to_i
        if ofs + len > node_size
            len = node_size - ofs
        end
        [ofs, len]
    }
end

rsd = RSD.new(ENV['RSD'])

template = "#{ENV['IMGS_DIR']}/template.img"
test = "#{ENV['IMGS_DIR']}/test.img"
target = "#{ENV['IMGS_DIR']}/target.img"

system("dd if=/dev/urandom of=#{template.shellescape} bs=64k count=#{1024 * 1024 * 1024 / (64 * 1024)}")
system("cp #{template.shellescape} #{test.shellescape}")
system("truncate -s #{1024 * 1024 * 1024} #{target.shellescape}")

bench_node = {
    node_name: 'bench',
    driver: 'bench-export',
    request_type: 'write',
    random: true,
}
copy_node = {
    node_name: 'copy',
    driver: 'copy',
    job_id: 'copy',
    mode: 'copy-before-write',
    auto_finalize: false,
    active: true,
}
source_node = {
    node_name: 'source',
    driver: 'file',
    filename: test,
}
target_node = {
    node_name: 'target',
    driver: 'file',
    filename: target,
}
rsd.qmp.blockdev_add({
    **bench_node,
    exported: {
        **copy_node,
        file: source_node,
        target: target_node,
    }
})

while rsd.qmp.event_wait('JOB_STATUS_CHANGE')['data']['status'] != 'pending'
end
rsd.qmp.job_finalize(id: 'copy')
while rsd.qmp.event_wait('NODE_FADED')['data']['node-name'] != 'copy'
end
while rsd.qmp.event_wait('JOB_STATUS_CHANGE')['data']['status'] != 'null'
end
rsd.qmp.quit
rsd.wait

puts("Comparing template.img with target.img:")
if !system("qemu-img compare #{target.shellescape} #{template.shellescape}")
    exit 1
end
