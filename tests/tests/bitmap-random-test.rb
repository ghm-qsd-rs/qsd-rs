#!/usr/bin/ruby

# Test random operations on dirty bitmaps.
# Create three bitmaps, disable one of them, run a couple of operation, enable
# that bitmap, run more operations, disable another bitmap, and run more
# operations.  Verify that the bitmaps that were disabled at some point still
# show the same dirty areas as the one that was constantly enabled (taking
# differing granularities into account).  Make one bitmap have finer
# granularity, check that its dirty areas did not change.  Verify all bitmaps
# against the list of operations (manually reconstructing bitmaps from them).
# (The argument is such just so we can run the test multiple times.)

# test: {
#     "args": [
#         ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]
#     ]
# }

require ENV['RSD_RB']

def random_ops(r, count, node_size)
    count.times.map { |_|
        ofs = (r.rand * node_size).to_i
        len = (r.rand * 1024 * 1024).to_i
        if ofs + len > node_size
            len = node_size - ofs
        end
        [ofs, len]
    }
end

def run_qemu_io(ops, nbd_sock, exp_name)
    nbd_url = "nbd+unix:///#{exp_name}?socket=#{nbd_sock}"
    cmd_file = "#{ENV['SOCK_DIR']}/qemu-io-cmds"
    out_file = "#{ENV['SOCK_DIR']}/qemu-io-out"
    IO.write(cmd_file, ops.map { |op|
        "write #{op[0]} #{op[1]}"
    } * $/)
    if !system("qemu-io -f nbd #{nbd_url.shellescape} &>#{out_file.shellescape} <#{cmd_file.shellescape}")
        system("cat #{out_file.shellescape} >&2")
        exit 1
    end
end

def check_bitmap(name, areas, ops, gran, node_size)
    last_bit = (node_size + gran - 1) / gran
    bit_count = last_bit + 1
    compare = [0] * ((bit_count + 31) / 32)
    ops.each do |op|
        if op[1] == 0
            next
        end
        first = op[0] / gran
        last = (op[0] + op[1] - 1) / gran
        (first..last).each do |i|
            compare[i / 32] |= 1 << (i % 32)
        end
    end

    state = false
    compare_areas = []
    start = nil
    i = 0
    while i < bit_count
        val = compare[i / 32]
        if (!state && val == 0) || (state && val == 0xffffffff)
            i += 32
            next
        end

        next_big_i = i + 32
        next_big_i = next_big_i > bit_count ? bit_count : next_big_i
        while i < next_big_i
            bit = (val & 1) != 0
            val >>= 1
            if state != bit
                if !state
                    start = i
                else
                    compare_areas << [start * gran, (i - start) * gran]
                    start = nil
                end
                state = bit
            end
            i += 1
        end
    end
    if state
        compare_areas << [start * gran, node_size - start * gran]
    end

    if areas == compare_areas
        puts("Comparison for #{name}: GOOD")
    else
        $stderr.puts("Comparison for #{name}: BAD")
        puts("Surplus reported:")
        p (areas - compare_areas)
        puts("Surplus calculated:")
        p (compare_areas - areas)
        exit 1
    end
end

seed = ARGV[1]
if seed
    seed = Integer(seed)
else
    seed = Random.new_seed
end
puts("=== Seed: #{seed} ===")
r = Random.new(seed)

rsd = RSD.new(ENV['RSD'])

nbd_sock = "#{ENV['SOCK_DIR']}/nbd.sock"
rsd.qmp.server_start({
    id: 'nbd-server',
    type: 'nbd',
    addr: {
        type: 'unix',
        path: nbd_sock,
    },
})

#size = (r.rand * 1024 * 1024 * 1024 * 1024).to_i
size = 1024 * 1024 * 1024 * 1024
rsd.qmp.blockdev_add({
    node_name: 'node0',
    driver: 'null',
    size: size,
})

rsd.qmp.block_export_add({
    id: 'exp0',
    type: 'nbd',
    node_name: 'node0',
    server: 'nbd-server',
    writable: true,
})

full_gran = 1 << (r.rand() * 16 + 9).to_i
rsd.qmp.block_dirty_bitmap_add({
    node: 'node0',
    name: 'bitmap-full',
    granularity: full_gran,
})

start_gran = 1 << (r.rand() * 16 + 9).to_i
rsd.qmp.block_dirty_bitmap_add({
    node: 'node0',
    name: 'bitmap-start',
    granularity: start_gran,
})

end_gran = 1 << (r.rand() * 16 + 9).to_i
rsd.qmp.block_dirty_bitmap_add({
    node: 'node0',
    name: 'bitmap-end',
    granularity: end_gran,
    disabled: true,
})

start_ops = random_ops(r, (r.rand * 1024).to_i, size)
mid_ops = random_ops(r, (r.rand * 1024).to_i, size)
end_ops = random_ops(r, (r.rand * 1024).to_i, size)

run_qemu_io(start_ops, nbd_sock, 'node0')
rsd.qmp.block_dirty_bitmap_enable({
    node: 'node0',
    name: 'bitmap-end',
})
run_qemu_io(mid_ops, nbd_sock, 'node0')
rsd.qmp.block_dirty_bitmap_disable({
    node: 'node0',
    name: 'bitmap-start',
})
run_qemu_io(end_ops, nbd_sock, 'node0')

rsd.qmp.block_export_del({ id: 'exp0' })

start_areas = rsd.qmp.x_dbg_dump_bitmap({
    node: 'node0',
    name: 'bitmap-start',
})
check_bitmap('bitmap-start', start_areas, start_ops + mid_ops, start_gran, size)

end_areas = rsd.qmp.x_dbg_dump_bitmap({
    node: 'node0',
    name: 'bitmap-end',
})
check_bitmap('bitmap-end', end_areas, mid_ops + end_ops, end_gran, size)


biggest_gran = start_gran > end_gran ? start_gran : end_gran
biggest_gran = biggest_gran > full_gran ? biggest_gran : full_gran

rsd.qmp.block_dirty_bitmap_add({
    node: 'node0',
    name: 'bitmap-merged',
    granularity: biggest_gran,
})
to_merge = ['bitmap-start', 'bitmap-end']
if r.rand >= 0.5
    to_merge += ['bitmap-merged']
end

rsd.qmp.block_dirty_bitmap_merge({
    node: 'node0',
    target: 'bitmap-merged',
    bitmaps: to_merge,
})

merged_areas = rsd.qmp.x_dbg_dump_bitmap({
    node: 'node0',
    name: 'bitmap-merged',
})
check_bitmap('bitmap-merged', merged_areas, start_ops + mid_ops + end_ops, biggest_gran, size)

finer_gran = 1 << (r.rand * 16 + 9)
if finer_gran > biggest_gran
    finer_gran = biggest_gran >> 1
end
rsd.qmp.block_dirty_bitmap_add({
    node: 'node0',
    name: 'bitmap-finer',
    granularity: finer_gran,
})
rsd.qmp.block_dirty_bitmap_merge({
    node: 'node0',
    target: 'bitmap-finer',
    bitmaps: ['bitmap-merged'],
})

finer_areas = rsd.qmp.x_dbg_dump_bitmap({
    node: 'node0',
    name: 'bitmap-finer',
})
if finer_areas != merged_areas
    $stderr.puts("making bitmap-merged finer resulted in wrong data")
    exit 1
end

if full_gran < biggest_gran
    full_areas = rsd.qmp.x_dbg_dump_bitmap({
        node: 'node0',
        name: 'bitmap-full',
    })
    check_bitmap('bitmap-full', full_areas, start_ops + mid_ops + end_ops, full_gran, size)

    rsd.qmp.block_dirty_bitmap_add({
        node: 'node0',
        name: 'bitmap-full-coarse',
        granularity: biggest_gran,
    })

    rsd.qmp.block_dirty_bitmap_merge({
        node: 'node0',
        target: 'bitmap-full-coarse',
        bitmaps: ['bitmap-full'],
    })

    full_areas = rsd.qmp.x_dbg_dump_bitmap({
        node: 'node0',
        name: 'bitmap-full-coarse',
    })
    check_bitmap('bitmap-full-coarse', full_areas, start_ops + mid_ops + end_ops, biggest_gran, size)
else
    full_areas = rsd.qmp.x_dbg_dump_bitmap({
        node: 'node0',
        name: 'bitmap-full',
    })
    check_bitmap('bitmap-full', full_areas, start_ops + mid_ops + end_ops, full_gran, size)
end

if merged_areas != full_areas
    $stderr.puts("Merging start + end together should have yielded the full bitmap, but did not")
    exit 1
end

['bitmap-start', 'bitmap-end', 'bitmap-full', 'bitmap-merged', 'bitmap-finer'].each do |bitmap|
    rsd.qmp.x_dbg_dump_bitmap({
        node: 'node0',
        name: bitmap,
        clear: true,
    })

    post_clear_iter = rsd.qmp.x_dbg_dump_bitmap({
        node: 'node0',
        name: bitmap,
    })
    if !post_clear_iter.empty?
        $stderr.puts("Clearing iterator should have cleared bitmap #{bitmap}, but didn't")
        exit 1
    end
end

rsd.qmp.blockdev_del({ node_name: 'node0' })

rsd.qmp.quit
rsd.wait
