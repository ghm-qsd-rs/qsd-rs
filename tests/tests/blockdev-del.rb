#!/usr/bin/ruby

# Adds an export on top of a null node, in a named thread, attempts to delete
# the node (which should fail), deletes the export, and then attempts to delete
# the node once more, which should succeed.
# (Also, test some invalid blockdev-del calls.)

# test: {
#     "args": [
#         ["bench", "nbd", "vhost-user-blk"]
#     ]
# }

require ENV['RSD_RB']

export_type = ARGV[0]

rsd = RSD.new(ENV['RSD'])

rsd.qmp.object_add({
    id: 'iothr0',
    qom_type: 'iothread',
})

rsd.qmp.blockdev_add({
    node_name: 'node0',
    driver: 'null',
    size: 1 * 1024 * 1024,
})

case export_type
when 'bench'
    rsd.qmp.block_export_add({
        id: 'exp0',
        type: 'bench',
        node_name: 'node0',
        request_type: 'read',
        iothread: 'iothr0',
        total: 1 * 1024 * 1024 * 1024, # huge count, will be deleted before the end anyway
    })
when 'nbd'
    rsd.qmp.nbd_server_start({
        addr: {
            type: 'unix',
            path: ENV['SOCK_DIR'] + '/nbd.sock',
        },
    })

    rsd.qmp.block_export_add({
        id: 'exp0',
        type: 'nbd',
        node_name: 'node0',
    })
when 'vhost-user-blk'
    rsd.qmp.block_export_add({
        id: 'exp0',
        type: 'vhost-user-blk',
        node_name: 'node0',
        iothreads: ['iothr0'],
        addr: {
            type: 'unix',
            path: ENV['SOCK_DIR'] + '/vhost-user-blk.sock',
        },
    })
else
    $stderr.puts("Bad export type '#{export_type}'")
    exit 1
end

begin
    rsd.qmp.blockdev_del({
        node_name: 'node0',
    })
    $stderr.puts('blockdev-del should have failed')
    exit 1
rescue QMPError => e
    if e.object['error']['desc'] != 'Node "node0" is in use'
        $stderr.puts('Unexpected error message from blockdev-del')
        exit 1
    end
end

sleep 0.2

begin
    rsd.qmp.blockdev_del({
        node_name: 'exp0',
    })
    $stderr.puts('blockdev-del should have failed')
    exit 1
rescue QMPError => e
    if e.object['error']['desc'] != 'Node with name "exp0" not found'
        $stderr.puts('Unexpected error message from blockdev-del')
        exit 1
    end
end

rsd.qmp.block_export_del({
    id: 'exp0',
    mode: 'hard',
})

rsd.qmp.event_wait('BLOCK_EXPORT_DELETED')

# Also try deleting and re-adding the I/O thread
# (Regression check for commit dc8458353a10913894a349cd5c6d4f6e02fb454e)
rsd.qmp.object_del({
    id: 'iothr0',
})

rsd.qmp.object_add({
    id: 'iothr0',
    qom_type: 'iothread',
})

begin
    rsd.qmp.blockdev_del({
        id: 'node0',
    })
    $stderr.puts('blockdev-del should have failed')
    exit 1
rescue QMPError => e
    if !e.object['error']['desc'].include?('unknown field `id`')
        $stderr.puts('Unexpected error message from blockdev-del')
        exit 1
    end
end


rsd.qmp.blockdev_del({
    node_name: 'node0',
})

rsd.qmp.quit
rsd.wait
