#!/usr/bin/ruby

# rsd: Management of rsd instances
# Copyright (C) 2017, 2022 Hanna Reitz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'json'
require 'shellwords'
require 'socket'
require (File.realpath(File.dirname(__FILE__)) + '/qmp.rb')


$qemu_input = ''


class RSD
    $instance_counter = 0

    def initialize(*command_line)
        @this_id = "#{Process.pid}-#{$instance_counter}"
        $instance_counter += 1

        @qmp_socket_fname = ENV['SOCK_DIR'] + '/rsd.rb-qmp-' + @this_id

        @qmp_socket = UNIXServer.new(@qmp_socket_fname)

        c_stdin, @stdin = IO.pipe()
        @stdout, c_stdout = IO.pipe()

        command_line.map! do |arg|
            if arg.kind_of?(Hash)
                JSON.unparse(qmp_replace_underscores(arg))
            else
                arg
            end
        end

        @child = Process.fork()
        if !@child
            chardev = {
                id: 'char0',
                backend: {
                    type: 'socket',
                    data: {
                        addr: {
                            type: 'unix',
                            path: @qmp_socket_fname,
                        },
                    },
                },
            }

            monitor = {
                id: 'mon0',
                chardev: chardev[:id],
            }

            add_args = ['--chardev', JSON.unparse(chardev),
                        '--monitor', JSON.unparse(monitor)]

            STDIN.reopen(c_stdin)
            STDOUT.reopen(c_stdout)

            Process.exec(*command_line, *add_args)
            exit 1
        end

        @qmp = nil
    end

    def qmp
        if !@qmp
            @qmp_con = @qmp_socket.accept()
            @qmp = QMP.new(@qmp_con)
        end

        @qmp
    end

    def stdin
        @stdin
    end

    def stdout
        @stdout
    end

    def cleanup()
        @stdout.close if @stdout

        @child = nil
        begin
            File.delete(@qmp_socket_fname)
        rescue
        end
    end

    # If wait is not set, the caller has to call it (or .cleanup)
    # manually.
    def kill(signal='KILL', wait=true)
        Process.kill(signal, @child) if @child
        self.wait() if wait
    end

    def wait()
        Process.wait(@child) if @child
        self.cleanup()
    end

    def pid
        @child
    end
end
