#!/usr/bin/ruby

# See what happens when 'quit' is issued while rsd is already quitting.
# (Should just be a plain error.)

# test: {
# }

require ENV['RSD_RB']


rsd = RSD.new(ENV['RSD'])

img = "#{ENV['IMGS_DIR']}/test.img"
system("truncate -s 256M #{img.shellescape}")

# file-async needs to join the worker threads, so this is a good node driver to
# use when you want 'quit' to take a bit of time
rsd.qmp.blockdev_add({
    node_name: 'prot',
    driver: 'file',
    aio: 'threads',
    filename: img,
})

rsd.qmp.block_export_add({
    id: 'exp0',
    type: 'bench',
    node_name: 'prot',
    request_type: 'read',
})


# Maybe this provokes a double quit, maybe it does not.  Also, the order in
# which this is processed by rsd is not fixed, so if the QMP quit comes in
# before the ^C, no error will be printed, even if the ^C notices the double
# quit.
# Therefore, we cannot rely on seeing an error here.
# What we do not want to see is a sudden EOF on the QMP channel, which would
# indicate that the process crashed (because of a failed assertion).  That is
# raised as a non-QMPError assertion, and so will fail this test.
begin
    rsd.kill('INT', wait=false)
    rsd.qmp.quit
    # pass
rescue QMPError
    # pass
end

rsd.wait
